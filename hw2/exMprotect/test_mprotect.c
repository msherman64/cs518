#include <stdio.h> //printf
#include <stdlib.h> //for posix_memalign
#include <unistd.h> //for sysconf
#include <signal.h> //for handler and signals
#include <sys/mman.h> //for mprotect, prot_...

#define numpages 10

char *memory = NULL;
int pagesize;

void * get_page_boundary( void *cur_addr, void *memstart){
    int pageoffset = -1;
    char *pageboundary = NULL;
    
    int offset = (char *)cur_addr - (char *)memstart;
    if((offset > 0) && (offset < (numpages*pagesize))){
       pageoffset = offset % pagesize; //distance from prev page boundary
       pageboundary = (char *)cur_addr - pageoffset;
    }

    return (void *)pageboundary;

}

static void handler(int sig, siginfo_t *si, void *unused)
{
    printf("handler: Got SIGSEGV at address: 0x%lx\n",(long) si->si_addr);
    printf("handler: Implements the handler only\n");
    //compute page boundary
   
    void * dummy = get_page_boundary(si->si_addr, memory); 
    printf("handler: dummy value is %p\n", dummy);

    int ret = mprotect(dummy, pagesize, PROT_READ | PROT_WRITE);
    printf("handler: mprotect return value %d\n", ret);
}




int main(){
    pagesize = sysconf(_SC_PAGESIZE);

    struct sigaction sa;
    sa.sa_flags = SA_SIGINFO;
    sigemptyset(&sa.sa_mask);
    sa.sa_sigaction = handler;
    sigaction(SIGSEGV, &sa, NULL);


    printf("main starting \n");


    int result = posix_memalign((void *)&memory, pagesize, numpages * pagesize);


    printf("address of region is %p\n", memory);
    printf("size of region is %lu\n", sizeof(*memory));

    mprotect(memory, numpages*pagesize, PROT_NONE);

    printf("main: memstart is %p\n", memory);
    memory[27]=5;

//    for(int i=0; i<(numpages*pagesize); i++){
//        memory[i]=0;
//       printf("setting value %d address %p to 0\n", i, &memory[i]);
//    }










}
