Design
======

Memory structure
----------------

Terminology
^^^^^^^^^^^

* Block: a requested allocation
* MetaBlock: in-memory metadata at the front of each block
* Page: Smallest contiguous memory reservation

Structure
^^^^^^^^^
* In Part A Blocks are allocated with a page
* In Part B, multiple pages are supported
  A design choice is whether to allow blocks to span pages.
  In userspace, we cannot translate requested addresses. Therefore:
  If blocks can span pages, page allocations must be contiguous
  If blocks cannot span pages, then page allocations must be of fixed index, 
  but are not necessarily contiguous.

.. image:: images/MallocDig.png

* A page table is needed to store which thread has which page. 
  This will be placed at the head of the allmem structure.
  The allmem struct is to be 8 MB, or 8 * 2^20. The page size is 4096 Bytes,
  giving us 2048 pages maximum. The page table is just an array of pointers to threads.

Functions
^^^^^^^^^

* myallocate: myallocate will check to see if the current thread has a page
  if so, it will check for a metablock at the start of that page.
  For each metablock, it will compute the free area between that block and the next
  and iterate to the next one until a large enough gap is found.
  if ->next is null, it should compute the space left until the end of the page.
  if for any reason it cannot allocate, myallocate returns a null pointer.
* mydeallocate: mydeallocate will scan through the block, until next = the 
  address in question.  It will then point current to next->next, skipping 
  the freed block.
* getpage: for a given tid, returns the page that is owned by that page, or a 
  new page. This behavoir will need to change when a process can own more 
  than one page. 
* initpagetable: zeros the page table at the begining of memory and sets 
  a pointer to the first useable page
* getfreeblock: takes a pointer to a meta block which is the first meta block
  in the page to be used. Scans the existing for a open spot of appropriate 
  size and then returns a pointer to that allocatable region. May need to 
  patch up metablocks.

TODO
^^^^

* Accurately calculate the page table size (number of allocateable pages) to 
  account for the page table memory usage. There is some minmax of number of 
  pages in the page table vs total number of allocated pages. The max index
  of the page table should not "go off the edge" of our memory allocation.
* Think more about the page table strategy. We could either store a bit string
  is 1 at the numeric position indicies for which the page own.  


Text from Google doc
^^^^^^^^^^^^^^^^^^^^

.. image:: images/SwappingProcedure.png

Swap in
On thread leave run state
Call mprotect on each page currently loaded and owned by thread
On enter run state
All pages in memory will be protected
On access, segfault will fire
If owned by thread, unprotect
If not owned by thread, swap in correct page, and unprotect
If no relevant swap available, segfault
Swap Out

Invariants:
Whenever a new thread is swapped in, its first page is swapped to the “front” of assignable memory.
The previous threads memory is swapped “back” to some other region of memory and all pages (except the first one) are memprotected
When the thread tries to retrieve a new page that is protected, it is swapped in in the signal handler
The signal handler copies the existing page to the “back” of memory and stores a pointer to the next free/evictable page.
The meta information at the front is a bit string that tells you what pages the current thread has active and a pointer to the next evictable/free page to use in case a swap needs to happen.
When a swap occurs, if the pointed to page is not marked as live (how this is done is not yet clear), it is overwritten. If it is marked as live, it gets copied to swap.
The list of pages owned by a thread is an array of pointers (potentially equal to the number of pages). This order of this array is the order they “should appear” when contiguously mapped. The pointers are to their current location in memory. E.g. if the second page in the contiguous allocation of a thread need to be swapped in. We go to this array and get the pointer at position 2, memcpy that page to position 2 of the running memory (after saving what was there in a appropriate place). 



New Description of memory process
We maintain an array at the head of memory, called page table
It contains the thread ID of every resident page
The index represents physical memory
In each thread’s my_pthread_t
We maintain a list(array?) of pointers to the thread’s pages, called thread page list
These pointers point to the CURRENT location of the page, active or stored
The index of this array represents the thread’s virtual memory
On access, if a segfault is triggered
Look up desired page in current thread’s thread page list
if page is in correct place, unprotect, return
If page is not in correct place, trigger swap
If page is not mapped, or is out of bounds, exit with error.
The thread page list MUST be accessible to the memory manager. Otherwise, we can hit deadlocks
Swap (in memory)
Takes argument of page index #
In page table, checks thread ID of current owner of index #
Use free page pointer
If no free page, return error (trigger swap to disk)
Or evict page here.
Save old page
Copies current occupant of index # into found free page
Updates owning thread’s thread page list with new location
Update page table with new owner, etc.
Move new page to location
Looks up index # in current thread’s thread page list
Copies to desired #
update’s current thread’s thread page list
Update page table with new owner, etc.
Update free page pointer
 Find first free page in memory, iterating from end to front
Or pick a page to evict (just “marking” actual eviction will happen later”)
Get new page
If only one free page, returns NULL / no.
One free page needed for swapping to continue
Calls swap on next contiguous page in order

