.. hw2 documentation master file, created by
   sphinx-quickstart on Fri Nov  4 22:08:41 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

HW2 - Remebering Memory
=======================

Authors:
 * Mike Sherman
 * Shridatt Sugrim

Built on:
 * Singleton.cs.rutgers.edu

Contents:

.. toctree::
   :maxdepth: 2

   parta
   partb
   partc
