Part C - Swapping to Disk
=========================

Design
------

In Part C, we expand our memory space by enabling swap to disk. Each thread is 
still limited to 8MB of addressable memory, but we increase the area available
for storage of pages.

When memory fills up, evict() will be called to choose a page to write to disk.
On access, if a threads' page is on disk, it will be swapped into the correct
location. This may trigger additional swaps to disk to make room.

Assumptions
-----------
We assume a fixed size swap file, equal to 16MB. An index into this file is 
maintained on each copy in or out.

New structures
--------------

* inverted_swap_index

    * we maintain a second inverted page table in memory. This one refers to the
      offset of pages in the swap file, and lists owner / unused for each.
    * it is used to find available pages in the swap file, and to look up the
      owning thread.

* array of thread page stucture named tps

    * Now stores offset, and flag. 
    * Offset is in units of pages, and can refer 
      either to memory or the swap file.
    * Flag contains one of the values 0, 1, or 2. 0 for not allocated. 
      1 for in memory. 2 for in swap file.

* segv_lock and memory_lock

    * segv_lock is a flag set by the segfault_handler, to prevent re-entry
      on error conditions. The sigalrm handler checks it as well, and returns
      to the current thread if in the middle of the segfault handler's 
      execution, so as not to interupt the operation.
    * memory_lock is a similar flag, but for myallocate and mydeallocate.
      This must be a separate lock, as the segfault handler is triggered by
      mprotected regions, while in myallocate.


New functions
-------------

The primary new primitives added are 

* copy_to_disk(int offset)

    * calls find_available_disk_page()
    * find_available_disk_page will return -1 if the swap file is full.

* copy_to_mem(int offset)

    * calls find_available_page()
    * find_available_page will call evict() if memory is full.

* Each of them copies from an offset, into an available page. 
* evict()

    * chooses which page to write to disk from memory
    * it returns the offset of the newly available page, if any 

* find_available_disk_page

    * searches inverted swap table for free page in swap file, from lowest index.

Modifed functions
-----------------
* swap_pages is modified as follows.

    * It is required that the copy out procedure comes before the copy in 
      procedure. This ensures that copy in does not overwrite used memory. This is
      enforced by the ordering of operations in swap_pages().
    * the criteria to decide whether we need to make a copy out is unchanged
      but the call chain required to do the copy, can now call evict() to copy to
      disk if there are no free pages in memory
    * the criteria to decide whether a copy in is necessary has changed. It now 
      the storage location to see whether the desired page was ever allocated, is 
      currently in memory, or is currently in the swap file. If the required page
      is in the swap file, the page is copied into the correct memory location.

* Separate copy in and copy out into function, from swap_pages(). This

    * copy_out_mem(offset): copies out of page(offset) in memory, to available
      page in memory. Available page is found by find_available_page.
    * copy_in_mem(offset): copies accessed page from stored location in memory, 
      referenced by tps[offset] in cur_thread; to page # offset in memory
    * copy_out_disk(offset): copies page # offset in memory to available spot 
      in swap file. Available spot is found by get_available_disk_page
    * copy_in_disk(offset): copies accessed page from stored location in swap 
      file, referenced by tps[offset] in cur_thread; to page # offset in memory

* find_available_page()

    * If the page table is full, find_available page calls evict() to make a page
      available. It returns NULL if evict fails; 

* initpagetable()

    * In addition to page_table, now initializes swap table as well
    * Creates swap file of appropriate size on disk in same directory as code.

* my_pthread: setup() and pthread_create()

    * Now initialize offset and storage location in tps for all my_pthread_t 
      created, including main's.

* myallocate, mydeallocate, segfault_handler

    * We now set a lock flag on entrance to each of these, if not already set.
      The flag is cleared just prior to successful return.


Fixed bugs
----------
* concurrency issues when segfault handler interrupted by scheduler 
  segfault handler sets flag on entrance, and clears on before returning
  issue arises when scheduler fires in the middle, and the next thread enters
  the segfault handler.
  the handler's check for the flag causes the handler to immediately return. 
  the thread then tries to access the address again, resulting in a busy-wait
  loop. It ends when the first thread's context is resumed, completes the 
  operation, and unsets the flag.
  Operates logically correctly, but not efficiently. alarm handler must give
  segv handler precedence.

    * resolved with locks

* last two pages shown as owned by main
  Find_free_page is skipping the last two pages, showing both as owned by main
  in test cases where main should have only one page.

    * off by one error in find_available_page

Missing Features
----------------
* Better eviction strategy

    * See section below on eviction strategy.

* Better timing coordination between SIGARLM handler and SIGSEGV handler

    * When SIGALRM interupts a memory operation, we currently give one extra 
      time quantum to finish. This could lead to starvation on heavy memory 
      access.
    * Instead, we should defer the SIGALRM only until the end point of the 
      memory handling function (myallocate, mydeallocate, or segfault_handler). 
    * This would be accomplished by setting a flag in the SIGALRM handler when
      we determine that we have interrupted a memory operation. At the end of 
      the memory operation, this flag will be checked, and SIGALRM raised if it
      is set.
    * This prevents the case where SIGARLM fires one per quanta, but each time 
      in the middle of a memory operation for the current thread, resulting in
      the current thread never being descheduled.

Eviction Stratgey
-----------------

The current eviction stratgey is to simply choose the highest index page and 
remove it. This was done for ease of imlementation under a time constraint.
A superior method that we will implement if time allows is to evicit the 
several pages after the largest allocated index that the current thread is 
running. If we evict these page, then the next allocation that the running 
thread preforms will require fewer swap operations a in response to a 
single allocation. 

.. figure:: images/EvictBad.png

   In this eviction strategy we simply evict off the end. This fairely close
   to a random stratgey as there is no particular order in which the threads
   pages will be inserted into the back of the list.

.. figure:: images/EvictGood.png

   In this eviction strategy we evict after the highest page index the current
   page has allocated. This should make future allocations a bit smoother as
   there are some contigous free pages behind the current allocations. Since
   we allocate memory in contigous blocks this will result in fewer swaps for 
   the next batch of allocations.

This stragey is biased toward faster allocation as opposed to access. This
strategey would have to be tuned to the expected common case. One observation
that remains true however is that if we do evict more than one page, we should
do so in contigous pages sets. If we were not allow for random eviction of 
a fixed number of pages, future allocations might be penalized. 


Debugging
---------
Normal debugging tools fail to work in this type of program. Valgrind fails, as
it will not continue on segfaults, and also redefines free and malloc.
We were able to test programs to completion in GDB via the following command
to automatically continue on segfaults.::

    gdb --batch --ex 'handle SIGSEGV nostop' --ex r --ex c --ex bt  ./test_swap

Merged Files
------------

As per submission instrctions we have merged all the files into a single 
library. The relavant files are:

* my_pthread_mem.c
* my_pthread_mem.h

These files contain all the #defines for malloc, free, and the exisiting 
thread library calls. To get around the issue of our library using its own
defintion of free and malloc, we used calloc for all our internal allocations.
We remapped free() to real_free() and using an input flag to indicate 
whether we need to pass the call through to the real free function or use
the one defined in our lib. 
