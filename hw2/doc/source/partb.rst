Part B - Contigous multipage allocation with mprotect
=====================================================

Design
-------

In part B we now take a large leap forward in how the memory allocator works. 
We need to make several assumptions about how memory (and the address space)
is presented to client code. We relax the single page requirement and 
handle the case where multiple pages are required to service a allcoation
request. To achieve this capability we swap pages around on access. 

Assumptions
-----------

Our assumtions are as follows:

 * Each thread has a memory space that start at the address of the first
   page. 
 * User code calls the remapped free function. our Library uses the 
   real_free function which resolves to the original glibc free call.
 * Our internal control structures (my_pthread_t, etc... ) do not use 
   our malloc, and are not stored in the 8MB allocatable memory rage. These
   structures live in "untouchable" system memory, which is not visible to 
   any user process.
 * Every memory allocation is proceeded with a metablock that is used to 
   identify it and determine it's extents. 
 * Every thread has a page created for it with a meta block that represent
   the start of thier addressable memory space. This is true even for "main".
 * In every threads control block (my_pthread_t) there is an array of 
   pointers (named page_array) that lists what pages this thread has 
   allocated and where they currently live in the memory space. There is one 
   array entry per possible pages.
 * The array entries are a posotional representations of the threads contigous
   memory allocation. That is page_array[0] points to the page that the 
   thread bleives is it's first page. page_array[1] is threads second page. 
   Because these mappings are updated when pages are swapped, they only have
   to be in the correct posotion when then thread tries to access them. 
 * Each time a thread is swapped, the entire allocatealbe page set is 
   mprotected to trigger a call into our segv signal handler. The segv handelr
   will consult the threads page_array to determine what page to swap into the
   posotion the thread was trying to access. 

.. figure:: images/TCB.png

   Thread control Block

New or Modifed functions
------------------------

These functions were added/modified to support the new features of part b.

segfault_handler
^^^^^^^^^^^^^^^^

The segfault handler's main purpose is to patch up memory when a "page fault"
occurs. In our case segment faults and page faults trigger the same signal, 
we have to consult our internal structures to determine if the segment fault
was a legitmate error, or page fault that needs to be managed by our memory
allocation scheme. To do this segfault handler evaulates several cases. The
information used to decide these cases the the value of si->si_addr which
is passed in to our segfault handler by the OS. The cases are:

* The address is out of bounds
  Result: exit with error
* The address refers to a page that the current thread never allocated:
  Result: exit with error
* The address refers to a page that is in the correct location, and is owned 
  by the current thread:
  Result: unprotect the page
* The address refers to a page that is not owned by the current thread and 
  the current thread previouly allocated that page:
  Result: call the swap_pages() function to replace the current page, with 
  the page thread expect to be there and unprotect it.

swap_pages
^^^^^^^^^^

Much of the logic for our memory manager lives in this function. The swap 
function takes an argument which is the index of the page to be swapped out. 
It will determine where to swap it to via a call to find_free_page(). If no
free page is found this is an out of memory error and we should terminate
(this will later be replaced with eviction). If we find a page, we copy the
current page to the free page, this is the copy out. We then index into the 
current threads page table (stored in the my_thread_t). The page table stores 
a pointer to a differnt page in memory that hold the datas that was written 
by the thread at that page index. The page that is pointed to is copied into 
posotion, this is the copy in procedure. After this all the book keeping is 
updated and the page protections are put in place.  The following digram 
illustrates an exchange. You can see the creation of a new my_pthread_t and 
the update of the threads page table and the inverted page table. 

.. figure:: images/SwappingProcedure.png

   The swapping process.


find_available_page
^^^^^^^^^^^^^^^^^^^

This function starts at the back of the inverted page table and cycles forward
until it finds a page that is unclaimed (value of thread_id == -1). It returns
the offset of this page. In the future it may need to trigger an eviction.

init_thread_page
^^^^^^^^^^^^^^^^

This function creates the first page for a thread. It is assumed that all 
threads have at least one page allocated to them, and that this page has a
metablock (possibly empty) at the start of it. Every time a my_pthread_t is 
created, this function is called to create a page and adjust the ownership in
the inverted page table. The retun value is the address of a page that will
serve as the newly create threads page_array[0]. 

my_allocate
^^^^^^^^^^^

This function is merely a wrapper around find_avalaible block. 

find_available_block
^^^^^^^^^^^^^^^^^^^^

This function is the key implementaiton of allocate. Give a request size 
(req_size), it starts at the first metablock (which is assumed to exist) and 
travese the metablock list evaluating if there is enough free space to fit 
the req_size + sizeof(metablock). If it finds one in between, it create
the metablock and returns a pointer of the block that is to be used by the
thread. If it cannot find a free space in the middle that is big enough,
it goes to the end of the existing allocations (potentially at the end
of all allocated pages). There may be a metablock there if the memory at the
end was freed, however this meta block will have an NULL next pointer. It then
computes how much space is left in the page and how many new pages (if any) 
are needed to service the request. If it can allocate that many pages, via a 
call to allocat_pages(), it then creates the metablock (or resuses the 
existing one) and returns a pointer to the block. If enough space cannot be
allocated, it returns NULL.

allocate_pages
^^^^^^^^^^^^^^

Allocate pages takes 2 arguments, a page boundary to start from and a size of
allocation. It determines how many pages are needed, and then does a bounds
check to make sure that the total number of allocated pages is less than
NUMPAGES. If this check passes, it indexes into the current threads page
table and generates pointers for the requred ammount of pages. These pointers
initally point to existing memory. Since these pages have not been accessed
by the current thread, they are still mprotected. When the calling thread 
recieves a pointer to the allocated memory some of the pages it was allocated
may be protected. When they are accessed, the segafault handler will trigger
the swapping procedure, but since these are newly created pages, there 
will be a copy out, but no copy in. 


New or Modified structures
--------------------------

We had to modify some of the existing memory strctures in the TCB to suport 
this new feature.

my_pthread_t
^^^^^^^^^^^^

The major chnage here was the inclusion of a page tabe (named page_array). 
It's posotional index matches that of the pages in memory. The way the indicies
are to be interpreted is that, each posotion represents the meta information
for that particular page. In this simple case it's simply a pointer to the
contents of that page. For example, if posotion zero points page zero
then this thread is either active or just recently swapped out. When this 
thread is inactive, it's pages maybe swapped out. If this happens the 
pointer will be updated to point to the page that the data was moved to.

Common header
-------------

Since may of the contants are now shared between the two libs we moved several 
of the common defines and externs to a common header appropriately named 
common.h. Things like #define NUMPAGES. There is commomon.c, as this is purely
for defintions and name spaces. 


Conditional define for free
---------------------------

We had to hack in a "condotional define" for free to allow our library 
code to use the actual call to free. This was done with the following code::

    if(from_lib == LIBREQ){
        //dirty dirty hacks
        #undef free
        free(address);
        #define free(x) mydeallocate(x, __FILE__, __LINE__, THREADREQ)
        return;
    }

It's sloppy and we're not sure if there is a better way to do this.
