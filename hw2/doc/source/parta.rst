Part A - Single page allocation
===============================

In this section of the homework, the memory allocator allocates only a single 
page to service threads memory requests. We have a few simple functions 
that service the request for memory. They are:

 * myallocate
 * mydeallocate
 * getpage
 * getfreeblock
 * initpagetable

Design
------

This section of the homework was a proof of concept. The
8MB was split into PAGESIZE chunks which was dictated by ::

    #define PAGESIZE sysconf( _SC_PAGE_SIZE)

This would later be required for using aligned memory but in this case
we simply used a globally allocated char array of size 8MB. We partioned
the char array into a inital section that was used to store a inverted
page table, which maps page indicies to threads. The remaning portion 
of memory was used for the pages. Each thread was allowed to have a single
page and we kept track of which thread owned which page by a simple integer
index which was the contents of the inverted page table.

initpagetable
-------------

The initpagetable function prepared the 8MB char array. It computed the
start of the pages and initalized the inverted page table. We set all the 
thread ids (used to indicate owner) to -1. This indicated that a page was 
unclaimed.

getpage
-------

This function would search the inverted page table for a thread id. If the
supplied thread id was found it would return a pointer to the start of the 
page that the thread id mapped to. If the thread id was not found a new page
would be assigned to the thread (if there were any available). When 
a new page was allocated the getpage function would initiliaze the first
metablock in that page. 

getfreeblock
------------

Get free block was the workhose of this library. It was expected that there 
was a metablock at the start of the threads page. (This was enforced by
get page). The metablocks are a linked list. Each entry store meta information
about the block that follows it, including allocated size. The getfree block
function would travese the linked list and asses the avialbility of free
blocks that were between metablocks, or between the last metablock and the
end of the page. If get free block found an block big enough to hold the 
requested data + a meta block (if one needed to be created), the getfreeblock
function would return the address of the free block, or NULL if no such block 
wasfound (See diagram below). 

.. figure:: images/GetFreeBlock.png

   The acquisiton of a free block of memory.

myallocate
----------

my allocate is simply a wrapper. It checks for initalization and that the 
request size is less than 1 page. It the calls getfreeblock, and returns the
value that getfreeblock produces (which may be NULL).

mydeallocate
------------

The operation of my deallocate is also very simple. For the given address, 
it computes the address of the meta block preceding it. Once it had this 
address, it searches throught the metablock list to find the meta block that
precedes the one just calcuated. It then updates the ponters for the block 
preceeded, or sets the pointer to NULL and zeros the the size if the preceding
metablock is at the start of the page.
