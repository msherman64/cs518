char memblock[8*(2^20)];

void * get_ptr(int offset){
    return (void *) &memblock + offset;
}
