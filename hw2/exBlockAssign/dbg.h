#ifndef _MY_PTHREAD_H_
#define _MY_PTHREAD_H_

#include <sys/queue.h>      //TAILQ_*
#include <ucontext.h>       // ucontext_t

/* debug macros*/

ucontext_t error_context;           /* the error context context */

//should this be write instead of printf?
#ifndef NDEBUG
#define debug(M, ...)
#else
#define debug(M, ...) printf("DEBUG %s:%d: " M "\n",\
        __FILE__, __LINE__, ##__VA_ARGS__)
#endif

#define warn(M, ...) printf("WARN %s:%d: " M "\n",\
        __FILE__, __LINE__, ##__VA_ARGS__)

#define info(M, ...) printf("INFO %s:%d: " M "\n",\
        __FILE__, __LINE__, ##__VA_ARGS__)

//A is what we want to assert is true.
#define check(A, M, ...) if(!(A)) {\
    debug(M, ##__VA_ARGS__); setcontext(&error_context); }

//If pointer is null, then !pointer is True
#define check_mem(A) check((A), "Pointer was null")

//Constructor for creating error context
static void con() __attribute__((constructor)); 
void con(void);
void error(void);

#define STACKSIZE 8192    //default stack size
#endif
