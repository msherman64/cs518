#include <stdio.h>
#include "blockAlloc.h"
#include "dbg.h"

int main (int argc, char * argv[]){
    info("Starting");
    int *p = NULL;
    p = get_ptr(0);
    check_mem(p);
    debug("main: p %d:%p",0,p);
    p = get_ptr(5);
    check_mem(p);
    debug("main: p %d:%p",5,p);
    p = get_ptr(-1);
    check_mem(p);
    debug("main: p %d:%p",-1,p);
    p = NULL;
    check_mem(p);
    debug("main: p %d:%p",NULL,p);
    info("Ending");
}
