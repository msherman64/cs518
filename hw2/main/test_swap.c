// Shridatt Sugrim and Michael Sherman
// Tested on singleton.cs.rutgers.edu
#include <time.h> // for time
#include <stdio.h>
#include <stdlib.h> //rand(), srand()
#include "my_pthread_mem.h" //for thread stuff

void * short_func (void * func_arg)
{
    int arg = *(int *)func_arg; // set TID to arg
    info("short_func: in thread %d", arg);
    char *test_var = NULL;
    int i = 0;
    char *first = malloc(sizeof(char));
    for( i = 0; i < 2000; i++){ //set to 4000 to cause out of virt mem error
        //TODO fails on size 4080
        test_var = malloc(4080 * sizeof(char));
        test_var = "foo\0";
        //info("short_func: test_var address is %p, value %d, loop %d"
        //                , test_var, *test_var, i);
    //    free(test_var);
    }
    waitFor(1);
    debug("commence poking");
    char tempval;
    for( i = 0; i < PAGESIZE * NUMPAGES; i++){
        tempval = *first; 
    }
    debug("finished poking");
    return NULL;
}

int main (int argc, char * argv[]){
    info("TEST: Starting");

    int arg1 = 1;
    int arg2 = 2;

    //char *temp = malloc(9000 * sizeof(char));
    //temp = "foo";

    srand(time(NULL));

    pthread_t thread1;
    pthread_create(&thread1, NULL, short_func, (void *)&arg1);
    info("THREAD: Made thread %d", thread1.thread_id);

    pthread_t thread2;
    pthread_create(&thread2,NULL,short_func, (void *)&arg2);
    info("THREAD: Made thread %d", thread2.thread_id);



    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);
    //info("end of main, stored %s", temp );
    info("end of main");
    return 0;
}
