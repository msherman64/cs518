// Shridatt Sugrim and Michael Sherman
// Tested on singleton.cs.rutgers.edu
#include <time.h> // for time
#include <stdio.h>
#include <stdlib.h> //rand(), srand()
#include "my_pthread_mem.h" //for thread stuff

#define NUMTHREADS 100
#define NUMLOOPS 10000
#define NUMFREES 50
int *t1_allocs[NUMTHREADS][NUMLOOPS];

int availables[NUMFREES];

void * short_func (void * func_arg)
{
    int arg = *(int *)func_arg; // set TID to arg
    debug("short_func: thread %d", arg+1);
    int i = 0;

    //allocate
    for(i = 0; i < NUMLOOPS; i++){
        t1_allocs[arg][i] = malloc( 1 * sizeof(int)); 
        *t1_allocs[arg][i] = arg;
    }
    waitFor(1);
    
    int dummy = 0;
    for(i = 0; i < NUMFREES; i++){
        //free
        dummy = availables[i];
        free(t1_allocs[arg][dummy]);
    }
    waitFor(1);
    for(i = 0; i < NUMFREES; i++){
        //reallocate
        dummy = availables[i];
        t1_allocs[arg][dummy] = malloc( 1 * sizeof(int)); 
        *t1_allocs[arg][dummy] = arg+1;
    }
    waitFor(1);
    //read
    debug("reading values for thread %d", arg);
    for(i = 0; i < NUMLOOPS; i++){
        printf("%d ", *t1_allocs[arg][i]);
        if((*t1_allocs[arg][i] != arg) && (*t1_allocs[arg][i] != arg + 1)){
            info("Thread %d corrupted memory value %d", arg, *t1_allocs[arg][i]);

        }

    }
    return NULL;
}

int main (int argc, char * argv[]){
    info("TEST: Starting");

    srand(time(NULL));
    int args[NUMTHREADS];
    pthread_t threads[NUMTHREADS];
    int i = 0;

    for(i = 0; i < NUMTHREADS; i++){
        args[i] = i;
    }

    for(i = 0; i < NUMFREES; i++){
        availables[i] = rand() % NUMLOOPS;
    }

    for(i = 0; i < NUMTHREADS; i++){
        pthread_create(&threads[i], NULL, short_func, (void *)&args[i]);
        debug("THREAD: Made thread %d", threads[i].thread_id);
    }
    for(i = 0; i < NUMTHREADS; i++){
        pthread_join(threads[i], NULL);
    }

    info("end of main");

     
    return 0;
}
