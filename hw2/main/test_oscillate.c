// Shridatt Sugrim and Michael Sherman
// Tested on singleton.cs.rutgers.edu
#include <stdio.h>
#include "my_pthread_mem.h"

int main (int argc, char * argv[]){
    int *foo[100];
    int *boo = NULL;
    int *coo = NULL;
    int i = 0;

    info("Starting from 0");
    for(i = 0; i < 100; i++){
        debug("loop count is %d", i);
        foo[i] = malloc( 100 * sizeof(char)); 
        debug("main: foo address is %p", foo[i]);
        free(foo[i]);
    }

    info("Starting from 1");
    boo = malloc( 100 * sizeof(char)); 
    for(i = 0; i < 100; i++){
        debug("loop count is %d", i);
        foo[i] = malloc( 100 * sizeof(char)); 
        debug("main: foo address is %p", foo[i]);
        free(foo[i]);
    }
    info("Starting from 0");
    coo = malloc( 100 * sizeof(char)); 
    for(i = 0; i < 100; i++){
        debug("loop count is %d", i);
        foo[i] = malloc( 100 * sizeof(char)); 
        debug("main: foo address is %p", foo[i]);
        free(foo[i]);
    }





}
