// Shridatt Sugrim and Michael Sherman
// Tested on singleton.cs.rutgers.edu
#include <stdio.h>
#include "my_pthread_mem.h"

int main (int argc, char * argv[]){
    info("Starting");
    int *foo[100];
    int i = 0;
    for(i = 0; i < 100; i++){
        info("loop count is %d", i);
        foo[i] = malloc( 100 * sizeof(char)); 
        info("main: foo address is %p", foo[i]);
    }
    for(i = 0; i < 100; i++){
        info("loop count is %d", i);
        //this should exit 1 if we've run out of space
        //and try to free a null pointer
        free(foo[i]);
        info("main: foo address is %p", foo[i]);
    }

}
