// Shridatt Sugrim and Michael Sherman
// Tested on singleton.cs.rutgers.edu
//common includes
#include <sys/time.h>              // setitimer
#include <time.h>                  //for time() function
#include <stdio.h>                 //printf
#include <stdlib.h>                //malloc, size_t
#include <signal.h>                //sigaction, sigset, signals, etc.
#include <errno.h>                 //error status for mprotect

//memory includes
#include <string.h>                //memset, memcpy
#include <sys/mman.h>              //for mprotect, prot_...

//thread includes
#include <sys/queue.h>             //TAILQ_*
#include <ucontext.h>              // *context()

//for debugging
//#include "valgrind.h"              //valgrind hooks for debugging

//our headers
#include "my_pthread_mem.h"            //Our library
//check, check_mem, debug()
//definitions for memory structs

// memory related globals
int memory_lock = 0;
int segv_lock = 0;

void *memblock = NULL;

int page_table_allocated = 0;
void *startpages = NULL; //end of page table
void *start_page_table = NULL; //start of table
void *start_swap_table = NULL; //start of swap table
FILE *fp = NULL;

//thread related globals
int cur_id = 0;                    // last thread id created
int cur_pri = 0;                   // Current priority que we are working on

head_t wait_head;                  // queue head
head_t ready_head[PRIORITYLEVELS]; // ready queue head
my_pthread_t *all_threads[MAXTHREADS] = {NULL}; // all threads

my_pthread_t *cur_thread = NULL;   //pointer to active thread
ucontext_t signal_context;          /* the interrupt context */
void *signal_stack;                 /* global interrupt stack */

ucontext_t error_context;           /* the error context context */

//int was_init = 0;                   //flag to tell if we've run setup or not
sigset_t signal_set;                // signal set for blocking sigalrm

// memory related code

void * myallocate(size_t size, char *filename, int linenum, int from_lib){
    /* Wrapper for memory allocation
     * allocates free block of appropriate size via find_available_block
     */
    if(memory_lock != 0){
        debug("myallocate: re-entering locked state, return early");
        return NULL;
    } else{
        memory_lock = 1;
        debug("myallocate: locking segv, value %d", memory_lock);
    }
    debug("myallocate: malloc called from %s %d %d", filename, linenum, from_lib);
    //set from_lib != THREADREQ, to bypass our library
    if(from_lib != THREADREQ){
        return NULL; //TODO find out how to use malloc
    }
    void *temp = find_available_block(size);

    memory_lock = 0;
 
    return temp;
}

void mydeallocate(void *address, char *filename, int linenum, int from_lib){
    /* removes reference to the metablock for address, fixes up pointers
     * First case, freeing first metablock
     *  set size to 0
     * Second case, freeing nonfirst metablock
     *  set pointer of previous block to skip
     */
    if(memory_lock != 0){
        debug("mydeallocate: re-entering locked state, return early");
        return;
    } else{
        memory_lock = 1;
        debug("mydeallocate: locking segv, value %d", memory_lock);
    }

    if(from_lib == LIBREQ){
         //dirty dirty hacks
         #undef free
         warn("passing through call to free");
         free(address);
         #define free(x) mydeallocate(x, __FILE__, __LINE__, THREADREQ)
         memory_lock = 0;
         return;
     }

    debug("free called from %s %d %d", filename, linenum, from_lib);
    check_mem(address);

    metablock *pagestart = get_page_addr(0); //all threads start at 0
    debug("pagestart address is %p", pagestart);

    metablock *freeblock = ((metablock *)address) - 1;
    if(freeblock == pagestart){
        //case where freeblock is at the front
        debug("freeing first block in page %p", freeblock);
        freeblock->size = 0;
        memory_lock = 0;
        return;
    }

    //case where freeblock is not at the front

    metablock *loopvar = pagestart;
    check_mem(loopvar);

    for( ; loopvar->next != NULL; loopvar = loopvar->next){
       if(loopvar->next == freeblock){
          //freeblock is in middle or end of list
          debug("patching up pointer for block %p", loopvar);
          loopvar->next = freeblock->next;
          memory_lock = 0;
          return;
       } 
    }
    debug("didn't find block at address");
    memory_lock = 0;
    return;
}

void * find_available_block( size_t req_size){
    /* iterates on next until free block of sufficient size found
     * returns address of found block, or NULL if none available
     */


    metablock *loopvar = startpages;
    check_mem(loopvar);
    size_t freesize = 0;
    char *endaddr = NULL;
    char *endmem = (char *)startpages + PAGESIZE * NUMPAGES;
    //initialize in case we don't enter the loop
    char *blockstart = ((char *)(loopvar + 1)) + loopvar->size;

    //if large enough space, find last metablock before it
    for( ; loopvar->next != NULL; loopvar = loopvar->next){ 
        blockstart = ((char *)(loopvar + 1)) + loopvar->size;
        endaddr = (char *)loopvar->next;

        if(blockstart < endaddr){
            freesize = endaddr - blockstart;
        } else {
            freesize = 0;
        }

        if((req_size + sizeof(metablock)) <= freesize){
            //break if gap is large enough
            debug("find_available_block: broke out of loop in middle");
            break;
        }
    }


    if(loopvar->next != NULL){
        //found block of correct size, make new metablock
        metablock *newblock = (metablock *)blockstart;
        newblock->size = req_size;
        newblock->next = (metablock *)endaddr; //point to next metablock or NULL
        loopvar->next = newblock;
        debug("find_available_block: new block created at %p, in page %d",
                newblock, get_page_offset(newblock));
        return (void *)(newblock + 1);
    } else {
        //after search, if next == null, at end of list
        //need to update these manually, as they are updated at the start of 
        //each loop
        blockstart = ((void *)(loopvar + 1)) + loopvar->size;
        endaddr = NULL;

        int allocate_retval = -1;

        freesize = endmem - blockstart;
        //note: checks for enough room for requested allocation + a metablock
        //even if new metablock not needed. this is overly conservative.
        if((req_size + sizeof(metablock)) > freesize){
            return NULL; //case1: could not find free space
        } 
       

        //int page_offset = get_page_offset(blockstart);
        char *end_of_page = (char *)get_page_boundary(blockstart) + PAGESIZE;
        size_t space_in_page = 0;
        //extra check, make sure we allocated the page, of no, need to allocate    
        if(cur_thread->tps[get_page_offset(end_of_page)].stored != NOALLOC){
            space_in_page = end_of_page - blockstart;
        }

        size_t new_space_needed = 0; //space needed that must be in new pages
        if(req_size > space_in_page){ 
            //clamp new_space needed to positive or zero, avoids overflow
            new_space_needed = req_size - space_in_page;
        }

        //check to see if we can re-use empty metablock
        if(loopvar->size == 0){
            debug("find_available_block: reuse meta: space needed in new page is %zu", 
                    new_space_needed);    
            loopvar->size = req_size;

            //check to see if new pages needed
            if(new_space_needed > 0){ 
                allocate_retval = allocate_pages(new_space_needed, end_of_page);
                if(allocate_retval == -1){
                    return NULL;
                }
            }

            //Meta block areadly exists, no need to offest to make room for it
            return (void *)blockstart; 

        } else {
            //need to make a new metablock 
            //check to see if new pages needed
            if((req_size + sizeof(metablock)) > space_in_page){
                new_space_needed = 
                    (req_size + sizeof(metablock)) - space_in_page;
            }
            debug("find_available_block: new meta: space needed in new page is %zu", 
                    new_space_needed);    

            if(new_space_needed > 0){     
                allocate_retval = allocate_pages(new_space_needed, end_of_page);
                if(allocate_retval == -1){
                    return NULL;
                }
            }
            
            // if at end of list, endaddr will == NULL
            // size != 0 
            metablock *newblock = (metablock *)blockstart;
            newblock->size = req_size;
            newblock->next = (metablock *)endaddr; //point to next metablock or NULL
            loopvar->next = newblock;
            debug("find_available_block: new block created at %p, in page %d",
                    newblock, get_page_offset(newblock));
            return (void *)(newblock + 1);

        }

    } 
    // I should never get here
    check(0, "find_available_block: should never get here");
}

int allocate_pages(size_t size_needed, char *page_boundary){
    debug("allocate_pages: size %zu, location %p", size_needed, page_boundary); 
    check_mem(page_boundary);

    int offset = get_page_offset(page_boundary);
    debug("allocate_pages: offset %d", offset); 
    int pages_needed = (size_needed / PAGESIZE);
    if(size_needed % PAGESIZE){
        pages_needed +=1;
    }
    debug("allocate_pages: pages needed %d", pages_needed); 

    if(offset + pages_needed > NUMPAGES){
        return -1;
    }

    int i = 0;
    for(i = offset; i < (offset + pages_needed); i++){
        debug("allocate_pages: setting offset %d", i); 
        cur_thread->tps[i].offset = i;
        cur_thread->tps[i].stored = INMEM;
        //hint that this thread is allowed to use this location in memory now
        //after this point, this thread may have a pointer or some data to this
        //page in memory
    }
    debug("allocate_pages: allocating %d pages", pages_needed);

    return 0;

}

void initpagetable(){

    check(posix_memalign(&memblock, PAGESIZE, MEMSIZE) == 0,
            "initpagetable: failed to allocate memaligned block");

    start_page_table = memblock;
    debug("initpagetable: memblock size %ld", sizeof(*memblock));
    debug("initpagetable: numpages count %d", NUMPAGES);
    debug("initpagetable: page size is %ld", PAGESIZE); 
    debug("initpagetable: page table entry size is %ld", sizeof(page_table_entry));

    int offset_page_table = (NUMPAGES * sizeof(page_table_entry)) / PAGESIZE;
    if ((NUMPAGES * sizeof(page_table_entry)) % PAGESIZE != 0){
        offset_page_table += 1; //add 1 if remainder
    }
    debug("initpagetable: page table is %d pages long", offset_page_table);

    start_swap_table = (char *)memblock + (offset_page_table * PAGESIZE);

    //set pointer for start of swap table
    int offset_swap_table = (NUMSWAP * sizeof(int)) / PAGESIZE;
    if ((NUMSWAP * sizeof(int)) % PAGESIZE != 0){
        offset_swap_table += 1; //add 1 if remainder
    }

    startpages = start_swap_table + (offset_swap_table * PAGESIZE);


    //protect all of memory, to trigger handler when needed
    check(mprotect(startpages, NUMPAGES * PAGESIZE, PROT_NONE) == 0,
            "initpagetable: mprotect of all pages failed");
    
    debug("initpagetable: memblock start %p", memblock);
    debug("initpagetable: memblock end %p", (char *)memblock + MEMSIZE);
    page_table_entry *p = (page_table_entry *)start_page_table;
    int i = 0; 
    //initialize each page table entry
    for(i = 0; i < NUMPAGES; i++){
    //   debug("page table entry pointer %p, count %d", p+i,i);
       (p+i)->thread_id = -1; 
       (p+i)->virt_page_id = -1; 
    }

    //initialize swap_table in memory
    int *swap_pointer = start_swap_table;
    for(i = 0; i < NUMSWAP; i++){
       *(swap_pointer + i) = -1;
    }

    //create swap file

    int swap_file_size = (NUMSWAP * PAGESIZE) - 1;
    //opens swapfile, creates if does not exist, truncates otherwise
    fp = fopen("swapfile", "w+"); 
    fseek(fp, swap_file_size , SEEK_SET);
    fputc(0, fp);
    //fclose(fp);


    //set signal handler
    struct sigaction sa;
    sa.sa_flags = SA_SIGINFO;
    sigemptyset(&sa.sa_mask);
    sa.sa_sigaction = segfault_handler;
    check(sigaction(SIGSEGV, &sa, NULL) == 0, 
            "initpagetable: failed to register segfault handler");

}


void segfault_handler(int sig, siginfo_t *si, void *unused)
{
    /* this hander fires when a thread accesses a memprotected region of memory
     * there are 4 cases to handle, depending on the address si->si_addr:
     *  1 The address is out of bounds:
     *      exit with error
     *  2 The address refers to a page that the current thread never allocated:
     *      exit with error
     *  3 The address refers to a page that is in the correct location, 
     *  and is owned by the current thread:
     *      unprotect the page
     *  4 The address refers to a page that is not owned by the current thread
     *  and the current thread previouly allocated that page:
     *      trigger swap() on the computed offset
     */

    if(segv_lock != 0){
        debug("segfault_handler: re-entering locked state, return early");
        return;
    } else{
        segv_lock = 1;
        debug("segfault_handler: locking segv, value %d", segv_lock);
    }
   
    debug("segfault_handler: Got SIGSEGV at address: 0x%lx",(long) si->si_addr);
    
    //compute page boundary
    void * boundary = get_page_boundary(si->si_addr); 
    debug("segfault_handler: boundary value is %p", boundary);
    //gets page index
    int offset = get_page_offset(si->si_addr);
    //exit if case 1
    check((offset != -1) && (boundary != NULL),
            "segfault_handler: access of invalid page");

    //look up requested page in current thread's array
    void * virt_req_page = get_page_addr(cur_thread->tps[offset].offset);
    //exit if case 2
    check(cur_thread->tps[offset].stored != NOALLOC, 
            "segfault_handler: current thread never allocated this page");
    check(virt_req_page !=NULL, 
            "segfault_handler: allocated but pointer is null");

    //look up requested page in page table, ensure ownership is correct
    page_table_entry *p = get_page_table_entry(offset);

    int ret = -1; //initialize return value
    //check to see if entry in virtal table points to 
    //correct physical location
    if((virt_req_page == boundary) &&
            (p->thread_id == cur_thread->thread_id)){
        //case 3:
        ret = mprotect(virt_req_page, PAGESIZE, 
                PROT_READ | PROT_WRITE | PROT_EXEC);
        debug("segfault_handler: nothing to swap, unprotect and return value %d", 
                ret);
    } else {

        //case 4:
        // entry is not in the correct place, need to move it
        // swap copies memory from stored offset to needed offset
        debug("segfault_handler: swap called, some copy must occur");
        ret = swap_pages(offset);
        debug("segfault_handler: swap called, returned value %d", ret);
    } 
        segv_lock = 0;
        debug("segfault_handler: calls returned, unlocking segv, %d", segv_lock);
}

void * get_page_boundary(void *cur_addr){
    // calculates address of start of current page
    int page_offset = get_page_offset(cur_addr);
    if(page_offset == -1){
        debug("get_page_boundary: invalid offset");
        return NULL;
    }
    return get_page_addr(page_offset);
}

int get_page_offset(void *cur_addr){
    //computes offset in units of pages from the addresss of start of pages 
    //takes advantage of integer division truncation towards zero
    check_mem(cur_addr);
    int pageoffset = -1;
    int mem_offset = (char *)cur_addr - (char *)startpages;
    //debug("get_page_offset: mem_offset is %d", mem_offset);

    if((mem_offset >= 0) && (mem_offset < (NUMPAGES * PAGESIZE))){
       pageoffset = mem_offset / PAGESIZE; //distance in pages from pagestart
       //debug("get_page_offset: pageoffset is %d", pageoffset);
    }

    return pageoffset; //if valid, returns offset as int. Othersise returns -1
}

page_table_entry * get_page_table_entry(int offset){
    //gets page table entry by counting in from start of memory by offset
    return ((page_table_entry *)memblock) + offset;
}

void * get_page_addr(int offset){
    //gets page address by counting in offset * PAGESIZE bytes from startpages
    return (void *)(((char *)startpages) + (offset * PAGESIZE));
}

int swap_pages(int offset){
    /* swap pages looks up offset in both physical and virtual memory
     * live refers to the address of the offset in physical memory
     * stored refers to the address at the offset in the thread page array
     * free refers to the address of the free page found
     *
     * it operates in 3 stages
     * 
     * Find free page
     * Copy from live to free
     * copy from stored to live
     * mark stored as free
     */

    page_table_entry *live_ent = get_page_table_entry(offset);

    if(live_ent->thread_id != -1){ //if true, page is unused, don't copy out
        if(copy_out_mem(offset) == -1){
            return -1;
        }
    }
    //**copy in**
    //storage location of offset for cur_thread
    int cur_page_location = cur_thread->tps[offset].stored; 
    if(cur_page_location == INDISK){
        //debug("swap_pages: page is on disk");
        int ret = copy_in_disk(offset);
        check(ret != -1, "swap_pages: failed to copy from disk");

    } else if(cur_page_location == INMEM){ //page is in memory
        copy_in_mem(offset);
    } else if(cur_page_location == NOALLOC){
        //no copy in necessary
        debug("swap_pages: page %d was never allocated by thread %d", 
                offset, cur_thread->thread_id);
    } else {
       check(0, "swap_pages: invalid storage type, should not get here");
    }

    //fix up page table entry and theread page array for live
    //fix up thread page array for virt_req_page
    cur_thread->tps[offset].offset = offset;
    live_ent->thread_id = cur_thread->thread_id;
    live_ent->virt_page_id = offset;
        
    void *live_page = get_page_addr(offset);
    mprotect(live_page, PAGESIZE, PROT_READ | PROT_WRITE | PROT_EXEC );  
    return 0; //success
}

void * find_available_page(){
    /* scans through page table looking for page marked free
     * free pages have thread_id == -1
     * starts at back and scans to front
     */
    
    page_table_entry *p = get_page_table_entry(0);
    int offset = NUMPAGES-1; //starts at 0

    for( ; offset >= 0; offset--){
         //debug("find_available_page: checking thread id %d at offset %d",
         //   (p + offset)->thread_id, offset);
        //TODO: showing main owning last two pages, is this correct
        

        if((p + offset)->thread_id == -1){
            break;
        }
    }

    if(offset >=  0){
        debug("find_available_page: returning page offset %d", offset);
        return get_page_addr(offset);
    }
    else{
        debug("find_available_page: couldn't find free page, offset = %d", offset);
        //TODO: if no free page, call evict(), return address of
        //newly free page
        int retval = evict();
        debug("find_available_page: evict returned page %d for use", retval);
        return get_page_addr(retval); 
    }

}

int init_thread_page(int thread_id){
    /* initializes a new page for a new thread. sets up initial metablock.
     * sets ownership in inverted page table
     * returns void * pointer to new page
     */

    metablock *temp = (metablock *)find_available_page();
    if(temp == NULL){
        //return NULL if can't find free page
        return -1;
    }
    mprotect((void *)temp, PAGESIZE, PROT_WRITE);
    temp->next = NULL;
    temp->size = 0;
    mprotect((void *)temp, PAGESIZE, PROT_NONE);

    int offset = get_page_offset(temp);
    page_table_entry *p = get_page_table_entry(offset);

    p->thread_id = thread_id;
    p->virt_page_id = 0;


    return offset;

}

int evict(){
    /* chooses page in memory to swap to disk. 
     * Returns offset of memory page made available
     * calls copy_out_disk
     */

    //TODO, for testing, eviction strategy is to evict highest index page
    int evict_offset = NUMPAGES-1;

    int ret = copy_out_disk(evict_offset);
    if(ret == -1){
        debug("evict: swap file full, could not copy page %d", evict_offset);
        return -1;
    }
    debug("evict: swap file not full, copied page %d to disk", evict_offset);

    return evict_offset;
} 

int * get_swap_table_entry(int offset){
    //gets swap table entry by counting in from start of swap table by offset
    return (int *)start_swap_table + offset;
}

int find_available_disk_page(){
    /* returns offset of available page in swapfile
     */
 
    /* scans through swap page table looking for page marked free
     * free pages have value == -1
     * starts at front and scans to back
     */
    
    int *swap_table_entry = get_swap_table_entry(0);
    int offset = 0;

    for( ; offset <= NUMSWAP; offset++){
        if(*(swap_table_entry + offset) == -1){
            break;
        }
    }

    if(offset <= NUMSWAP){
        debug("find_available_disk_page: returning page offset %d", offset);
        return offset;
    }
    else{
        debug("find_available_disk_page: couldn't find available "
              "page, offset = %d", offset);
        return -1;
    }  

}

int copy_out_mem(int mem_offset){
    /* copy_out_mem(offset): copies out of page(offset) in memory, to available
       page in memory. Available page is found by find_available_page.
    */

    //address of free page
    void *available_page = find_available_page();
    if(available_page == NULL){
        return -1; //return failure if out of free pages
                   //will only return null if find_available_page could not
    }

    page_table_entry *live_ent = get_page_table_entry(mem_offset);
    void *live_page = get_page_addr(mem_offset);
    
    debug("copy_out_mem: page %d owned by %d, copying out", 
            mem_offset, live_ent->thread_id);
   
    //unprotect pages to be operated on 
    check(mprotect(available_page, PAGESIZE, PROT_WRITE) == 0,
            "copy_out_mem: mprotect failed");  
    check(mprotect(live_page, PAGESIZE, PROT_READ) == 0,  
            "copy_out_mem: mprotect failed");

    //actual copy
    memcpy(available_page, live_page, PAGESIZE);

    //reprotect once copy done
    check(mprotect(available_page, PAGESIZE, PROT_NONE) == 0,
            "copy_out_mem: mprotect failed");  
    check(mprotect(live_page, PAGESIZE, PROT_NONE) == 0,  
            "copy_out_mem: mprotect failed");  

    //mark available_page as owned in pagetable
    int available_offset = get_page_offset(available_page);
    page_table_entry *available_ent = get_page_table_entry(available_offset);
    available_ent->thread_id = live_ent->thread_id;
    available_ent->virt_page_id = live_ent->virt_page_id;

    //get pointer to thread that owned the free page
    my_pthread_t *available_thread = all_threads[available_ent->thread_id];
    //update address of virtual page in thread's page array
    available_thread->tps[available_ent->virt_page_id].offset = 
        available_offset;

    return 0;

}

void copy_in_mem(int mem_offset){
    /* copy_in_mem(mem_offset): copies accessed page from stored location in memory, 
       referenced by tps[mem_offset] in cur_thread; to page # mem_offset in memory
    */
    
    //get current address of page [mem_offset] in cur_thread's array
    void *virt_req_page = get_page_addr(cur_thread->tps[mem_offset].offset); //from
    void *live_page = get_page_addr(mem_offset);                         //to

    debug("copy_in_mem: virt_req_page address is %p", virt_req_page);

    //if null, page was not allocated, no need to copy something in
    if(
        (virt_req_page != NULL) &&         // Never allocated
        (virt_req_page != live_page)){     // already in correct place

        debug("copy_in_mem: stored page %d != live_page %d, copying in", 
                get_page_offset(virt_req_page), mem_offset);


        check(mprotect(live_page, PAGESIZE, PROT_WRITE) == 0,
                "copy_in_mem: mprotect failed");  
        check(mprotect(virt_req_page, PAGESIZE, PROT_READ) == 0,  
                "copy_in_mem: mprotect failed");  

        memcpy(live_page, virt_req_page, PAGESIZE);

        check(mprotect(live_page, PAGESIZE, PROT_NONE) == 0,
                "copy_in_mem: mprotect failed");  
        check(mprotect(virt_req_page, PAGESIZE, PROT_NONE) == 0,  
                "copy_in_mem: mprotect failed");  

        //mark old page as free
        //update live as used
        int stored_offset = get_page_offset(virt_req_page);
        page_table_entry *stored_ent = get_page_table_entry(stored_offset);
        stored_ent->thread_id = -1;
        stored_ent->virt_page_id = -1;
    }

}

int copy_out_disk(int mem_offset){
    /* copy_out_disk(offset): copies page # offset in memory to available spot 
       in swap file. Available spot is found by get_available_disk_page
     */

    char *page_copied = get_page_addr(mem_offset);

    int swap_file_offset = find_available_disk_page();
    debug("copy_out_disk: copying page %d from memory to swap file offset %d",
            mem_offset, swap_file_offset);
    if(swap_file_offset == -1){
        debug("copy_out_disk: swap file full");
        return -1;
    } 

    //seeks from beginning
    fseek(fp, swap_file_offset * PAGESIZE, SEEK_SET);

    check(mprotect((void *)page_copied, PAGESIZE, PROT_READ) == 0,  
            "copy_out_disk: mprotect failed");  
    //TODO check return value or use loop
    int bytes_copied = fwrite((void *)(page_copied), 1, PAGESIZE, fp);
    check(bytes_copied == PAGESIZE, 
            "copy_out_disk: failed, wrote %d bytes instead of %ld"
            , bytes_copied, PAGESIZE);

/*    //fwrite in loop to handle partial writes
    while(bytes_copied < PAGESIZE){
        //pointer offset by prev value of bytes_copied, used on next loop
        bytes_copied += fwrite((void *)(page_copied + bytes_copied), 
                PAGESIZE - bytes_copied, 1, fp); 
    }
*/    
    check(mprotect((void *)page_copied, PAGESIZE, PROT_NONE) == 0,  
            "copy_out_disk: mprotect failed");  

    //fix metadata

    //get page 
    page_table_entry *p = get_page_table_entry(mem_offset);
    int page_owner = p->thread_id; 
    int virt_page_index = p->virt_page_id; 
    all_threads[page_owner]->tps[virt_page_index].offset = swap_file_offset;
    all_threads[page_owner]->tps[virt_page_index].stored = INDISK;
    
    int *swap_table_entry = get_swap_table_entry(swap_file_offset);
    *swap_table_entry = page_owner;
    p->thread_id = -1;
    p->virt_page_id = -1;

    debug("copy_out_disk: exit successful");
    return 0;
} 

int copy_in_disk(int offset){
    /* copy_in_disk(offset): copies accessed page from stored location in swap 
       file, referenced by tps[offset] in cur_thread; to page # offset in 
       memory
     */


    void *mem_page = get_page_addr(offset);
    int swap_file_offset = cur_thread->tps[offset].offset;
    int *swap_table_entry = get_swap_table_entry(swap_file_offset);
    //check that page on disk was stored by current thread
    if(*swap_table_entry != cur_thread->thread_id){
        debug("copy_in_disk: cur_thread does not own page in swapfile");
        return -1;
    }
    //seeks from beginning
    fseek(fp, swap_file_offset * PAGESIZE, SEEK_SET);
    
    check(mprotect(mem_page, PAGESIZE, PROT_WRITE) == 0,
            "copy_in_disk: mprotect failed");  

    int bytes_read = fread(mem_page, 1, PAGESIZE, fp);
    check(bytes_read == PAGESIZE, "copy_in_disk: failed");
    
    check(mprotect(mem_page, PAGESIZE, PROT_NONE) == 0,
            "copy_in_disk: mprotect failed");  

    //fix cur_thread's page table 
    cur_thread->tps[offset].offset = offset;
    cur_thread->tps[offset].stored = INMEM;
   
    //fix swap table entry to show unused 
    *swap_table_entry = -1;

    //fix inverted page table
    page_table_entry *p = get_page_table_entry(offset);
    p->thread_id = cur_thread->thread_id;
    p->virt_page_id = offset;

    return 0;
}

//thread related code
/* 
    setup signal handler, queue allocate memory for the current thread pointer
*/
void setup() {
    con();
    initpagetable();


    debug("setup: start function");

    struct sigaction sa; 
    //setting fuction pointer callback
    sa.sa_handler = handle_sigalrm; 

    //restart interrupted system calls (deffer till after protected regions)
    sa.sa_flags = SA_RESTART; 

    //block all signals during handler except sigkill and sigstop
    sigfillset(&sa.sa_mask);

    //register the mask and call back
    check(sigaction(SIGVTALRM, &sa, NULL) == 0, "Sigaction set failed");

    //Allocate the first stack
    cur_thread = calloc(sizeof(my_pthread_t),1);
    check_mem(cur_thread);
    debug("setup: malloced cur_thread %p",cur_thread);
    cur_thread->thread_id=0;                       //This is mains my_pthread_t
    cur_thread->priority=1;                  //Every ones initial prioirty is 1
    cur_thread->thread_id=0;
    signal_stack = calloc(STACKSIZE,1);
    check_mem(signal_stack);
    debug("setup: malloced signal_stack %p",signal_stack);


    //initialize page of memory for main
    int i = 0;
    for(i = 0; i < NUMPAGES; i++){
        cur_thread->tps[i].offset = -1;
        cur_thread->tps[i].stored = 0;
    }  
    int first_page_offset = init_thread_page(cur_thread->thread_id);
    check(first_page_offset != -1, "setup failed to allocate page"); 
    cur_thread->tps[0].offset = first_page_offset;
    cur_thread->tps[0].stored = 1;


    // Initilaize all the queues
    TAILQ_INIT(&wait_head);
        // Can't check return value on these for some reason (macro badness)

    check(cur_id == 0,"Setup called call after threads were made?");
    all_threads[cur_id] = cur_thread; 
        // cur_id should be zero when setup is called

    for(i = 0; i < PRIORITYLEVELS; i++){
        TAILQ_INIT(&ready_head[i]); 
        debug("setup: initialized pq %d",i);
    }
    check(sigfillset(&signal_set) == 0,
            "setup: failed to empty sig_set"); 
    debug("setup: end function");
}

/*
    This schedules an alarm to fire ms milliseconds form the termnation
    of this function. It's used to schedule a single alarm in the futre.
    We do not want a "regular fireing clock" instead we want a off set
    that gives us back control ms seconds after we hand it off
*/

void start_timer (int ms)
{
    check (ms < MAXQUANTA, "start_timer: quanta too large");

    //sets up timer struct values. We only need the usec field 
    struct itimerval timer;

    // set timer for ms milliseconds
    timer.it_value.tv_sec=0;
    timer.it_value.tv_usec = ms*1000;       //must be nonzero for timer to fire
    timer.it_interval.tv_sec = 0;
    timer.it_interval.tv_usec = 0;

    //this starts the timer
    check (setitimer(ITIMER_VIRTUAL, &timer, NULL) == 0, "timer set failed"); 
}

/*
    creates a thread. Allocates it's stack memory and sets all of it's 
    my_pthread_t paramters to starting defaults. This function retuns
    after create so the uc_link context should not be returned to.

    thead is added to the run que, and also tracked in the all_thread que.
    returns nonzero on failure.
*/

int my_pthread_create( my_pthread_t *pt, my_pthread_attr_t *attr, void *(*function)(void*), void *arg)
{
    debug("my_pthread_create: start function");

    //Check for null pointers in the mandatory args
    check_mem(pt);
    check_mem(function);

    /*if(!was_init){                          //Call setup if it was never called
        setup();
        was_init=1;
        start_timer(SCHED_QUANTA);
    }
    */

    cur_id++;
    if (cur_id > MAXTHREADS){
        debug("make_pthread: can't make more threads");
        return -1;
    }

    // setting defaults in pt struct
    debug("my_pthread_create: begin Making thread %d",cur_id);
    all_threads[cur_id] = pt; //Index into the all threads array with thread id
    pt->thread_id = cur_id;          // make cur_id and thread_counter the same

    pt->priority=1;
    pt->finished=0;
    pt->ticks=0;
    pt->waiting_for=NULL;
    pt->joined_by=NULL;
    pt->func_arg=arg;

    int i = 0;
    for(i = 0; i < NUMPAGES; i++){
        pt->tps[i].offset = -1;
        pt->tps[i].stored = 0;
    }  
    int first_page_offset = init_thread_page(pt->thread_id);
    check(first_page_offset != -1, "setup failed to allocate page"); 
    pt->tps[0].offset = first_page_offset;
    pt->tps[0].stored = 1;

    //Create a context for the new thread
    void *stack=calloc(STACKSIZE,1);
    check_mem(stack);
    debug("my_pthread_create: malloced %p",stack);

    ucontext_t *uc = &pt->context;
    getcontext(uc);
    uc->uc_stack.ss_sp = stack;
    uc->uc_stack.ss_size = STACKSIZE;
    uc->uc_stack.ss_flags = 0;
    sigemptyset(&uc->uc_sigmask);
//    int ret = VALGRIND_STACK_REGISTER(stack, stack + STACKSIZE);
//    check(ret == 0, "my_pthread_create: VALGRIND_STACK_REGISTER Failed");
    makecontext(uc, (void (*)(void))run_thread, 1, function);


    //schedule with highest prioirty
    ready_insert(pt, 1);

    debug("my_pthread_create: finished, made thread id=%d",pt->thread_id);
    debug("my_pthread_create: end function");
    return 0;
}

/*
    All yield does is call SIGALARM, the sighandler / scheduler will then take
    over and schedule what ever is next in the queue
*/
void my_pthread_yield()
{
    raise(SIGVTALRM);
}

/*
    Exit saves the value that will be pathched into join and then sets the
    finsihed flag for this thread pointer
*/
void my_pthread_exit(void *value_ptr)
{
    debug("my_pthread_exit: start function for thread %d",
            cur_thread->thread_id);
    cur_thread->retval = value_ptr;                               //May be null
    cur_thread->finished=1;

    if(cur_thread->joined_by != NULL){
       // block_sigalrm(); //start protected region

        //make joined thread runnable
        my_pthread_t *np = cur_thread->joined_by;      //who was waiting for me
        np->waiting_for = NULL;                             //No longer waiting
        TAILQ_REMOVE(&wait_head, np, tailq_struct); 
        ready_insert(np, 1);

        debug("my_pthread_exit: thread %d moved to ready queue",
            np->thread_id);

        //unblock_sigalrm(); //end protected region
    }

 

    debug("my_pthread_exit: end function for thead  %d",
            cur_thread->thread_id);
    my_pthread_yield();
}

/*
    Join function set the waiting_for and joined_by hints to scheduer and
    then yeilds. Join returns 0 when the joined thread completes or a 
    negative number on error. If value_ptr is specified, will be used to 
    retrieve joined threads retval
*/

int my_pthread_join(my_pthread_t search_thread, void **value_ptr)
{
    my_pthread_t *np = all_threads[search_thread.thread_id];

    // Error conditions
    if(np == NULL){
        debug("my_pthread_join: not found in search");
        return ESRCH;
    } else if(np->joined_by != NULL){
        debug("my_pthread_join: thread already joined");
        return EINVAL;
    /*
     np->waiting_for may be null. The first case is to prevent joining some
     a thread that is wating on the current thread (deadlock). The second
     case is to prevent self joins (also deadlock).
    */
    } else if((np->waiting_for == cur_thread) ||
              (np == cur_thread)){
        debug("my_pthread_join: deadlock");
        return EDEADLK;
    } else if(np->finished == 1){
        debug("my_pthread_join: already finished");
        if (value_ptr != NULL){
            *value_ptr = np->retval;
        }
        return 0;
    }

    debug("my_pthread_join: found thread %d",np->thread_id);

    //block_sigalrm();  // begin protected region
    // set scheduler hints for joining and joined threads 
    np->joined_by=cur_thread;
    cur_thread->waiting_for=np;
    //unblock_sigalrm();  // end protected region

    debug("my_pthread_join: not already done, set pointers");
    my_pthread_yield();

    // set value_pointer's value to the address of np's return value pointer. 
    if (value_ptr != NULL){
        *value_ptr = np->retval;
    }

    return 0;
}


int my_pthread_mutex_init(my_pthread_mutex_t *mutex,
                          const pthread_mutexattr_t *mutexattr)
                                                       //attributes are ignored
{
    check_mem(mutex);
    mutex->lock_q = calloc(sizeof(m_head_t),1);        //malloc list head pointer
    check_mem(mutex->lock_q);
    debug("my_pthread_mutex_init: malloced list head %p",mutex->lock_q);
    m_head_t *lockp = mutex->lock_q;
    TAILQ_INIT(lockp);

    return 0;
}
/* this function should attempt to lock a mutex by placing itself on the 
   mutex's queue. if successful, it returns immediately otherwise, it blocks 
   until lock is obtained. The front of the list owns the lock, every one
   else on the list should be wating (on the wait queue).
 */
int my_pthread_mutex_lock(my_pthread_mutex_t *mutex)
{
    //debug and protection
    check_mem(mutex);
    //create list item, add reference to current thread, and insert 
    m_head_t *lockp = mutex->lock_q;       //get convenient handle to list head
    mutex_list_t *mtp = calloc(sizeof(mutex_list_t),1);
    check_mem(mtp);
    mtp->thread_p = cur_thread;               //point list object to cur_thread

    //actual tail insertion for FIFO ordering
    TAILQ_INSERT_TAIL(lockp, mtp, m_tailq_struct); 
    debug("my_pthead_mutex_lock: added thread %d to mutex queue\n",
          cur_thread->thread_id);

    //when we are not at the head of queue, we want to move to the wait queue
    check_mem(lockp->tqh_first);    //should  be not null, verify just inserted
    my_pthread_t *front = lockp->tqh_first->thread_p; 

    //we are the only item on the queue, and so are at the front
    if(front == cur_thread){ 
        debug("my_pthread_mutex_lock: thead %d got lock immediately",
              cur_thread->thread_id);
        return 0;                               //don't need to wait, succeeded
    } else {                          //case where we are waiting, not at front
       /*
        We set waiting for and allow the scheduler to move this thread to the
        wait queue
       */
        debug("my_pthread_mutex_lock: thead %d waiting for lock",
              cur_thread->thread_id);
        cur_thread->waiting_for=front; 
        my_pthread_yield();                              //give up control asap
        return 0;                            //return only when we get the lock
    }
}

/*
  Unlock mutex by popping current thread off of queue, if current thread is 
   in front free list item if removal successful
*/
int my_pthread_mutex_unlock(my_pthread_mutex_t *mutex)
{
    check_mem(mutex);
    //setup error cases
    m_head_t *lockp = mutex->lock_q;                 //pointer to head of queue
    check_mem(lockp);                                 //check mutex initialized

    //pointer to first item of queue
    mutex_list_t *front = lockp->tqh_first;

    //Front is null if list is empty, trying to unlock empty mutex
    if(front == NULL){
        debug("my_pthread_mutex_unlock: thead %d tried to unlock initialized"
              ", but empty mutex", cur_thread->thread_id);
        return 0;
    }
    if(front->thread_p != cur_thread){
        debug("my_pthread_mutex_unlock: thread %d can't unlock, not in front",
               cur_thread->thread_id);
        return -1;
    }

    //normal case where we have the lock
    TAILQ_REMOVE(lockp, front, m_tailq_struct); 
    debug("my_pthread_mutex_unlock: ran remove\n"); 
    real_free(front); //free list item

    //pop next item off of wait queue, so it can be scheduled
    front = lockp->tqh_first;            //old front is freed, we can now reuse
    if(front == NULL){
        return 0;                  //no more items, don't need to schedule next
    } else {
        //reschedule the head of the mutex queue
        front->thread_p->waiting_for = NULL;                //No longer waiting
        TAILQ_REMOVE(&wait_head, front->thread_p, tailq_struct); 
        ready_insert(front->thread_p, 1);

        debug("my_pthread_mutex_unlock: thread %d moved to ready queue",
            front->thread_p->thread_id);
        return 0;
    }
}

int my_pthread_mutex_destroy(my_pthread_mutex_t *mutex)
//mutex should be unlocked before destroy
{
    debug("my_pthread_mutex_destroy: start function for thread  %d",
        cur_thread->thread_id);
    check_mem(mutex);

    //check if mutex is locked.
    m_head_t *lockp = mutex->lock_q;
    check_mem(lockp);
    mutex_list_t *front = lockp->tqh_first;
    if(front != NULL){
        debug("my_pthread_mutex_destroy: cannot destroy, mutex not unlocked");
        return -1;
    }
    real_free(lockp);
    debug("my_pthread_mutex_destroy: end function for  %d",
        cur_thread->thread_id);
    return 0;
}

/*
    Wrapper for the running thread. passes the return value to
    my_pthread exit.
*/

void run_thread(void *(*function)(void *))
{
    void * dummy_retval = function(cur_thread->func_arg);
    my_pthread_exit(dummy_retval); 
}

/*
    This is a calendar que sheduler. Every process starts  
    my_pthread_t->prioirty 1 when created. They will be rescheduled at that 
    priority for MAX_PRIORITY_TICKS after which their prioity will be bumped 
    up (Higher means scheduled further into the future). When
    my_pthread_t-> prioirty reaches PRIORITYLEVELS-1, it gets reset to zero.
    This giving the old slow process more cycles for a while to see if we can 
    get it to complete.
*/

void scheduler(void)
{
    //TODO: Call memory init function on first scheduler run, or in con
    check_mem(cur_thread);        // If this happens we lost a context somwhere
    debug("Scheduler: thread %d completed %d ticks at priority %d",
            cur_thread->thread_id, cur_thread->ticks, cur_thread->priority);

    if(cur_thread->ticks >= MAX_PRI_TICKS){
        cur_thread->ticks = 0;                             //roll over priority
        cur_thread->priority++;
    }
    else {
        cur_thread->ticks++;                   // move the tick counter forward
    }
    if (cur_thread->priority >= PRIORITYLEVELS){
       debug("scheduler: priority of thread %d reset from %d",
            cur_thread->thread_id, cur_thread->priority);
       cur_thread->priority = 1;                      //Been stale for too long
    }

    if(cur_thread->waiting_for!=NULL){

        // this thread join another, put on wait queue
        TAILQ_INSERT_TAIL(&wait_head, cur_thread, tailq_struct);
        debug("scheduler: thread  %d moved to wait queue",cur_thread->thread_id);
    } else if(cur_thread->finished==0){

        //Thread not finished
        ready_insert(cur_thread, cur_thread->priority);
        debug("scheduler: thread  %d  rescheduled",cur_thread->thread_id);
    }

    my_pthread_t *dummy = cur_thread;
    cur_thread=get_next();

    if(dummy->finished == 1){
        //Free the stack we malloced in create, the rest belongs to the user
        ucontext_t uc = dummy->context; 
        real_free(uc.uc_stack.ss_sp);
    }

    debug("scheduler: startpages address is %p", startpages);
    check(mprotect(startpages, NUMPAGES * PAGESIZE, PROT_NONE) == 0,
            "scheduler: mprotect of all pages failed");
    debug("scheduler: memory protected for thread %d", cur_thread->thread_id);
    start_timer(SCHED_QUANTA);
    check(setcontext(&cur_thread->context) == 0,
        "scheduler: failed to switch contexts");
}

/* 
    The sigalrm handler is tasked with storing the current context,
    and preparing a new context for the scehduler, then swapping to it. 
*/
//void handle_sigalrm(int signal, siginfo_t *info, void *old_context) 
void handle_sigalrm(int signal) 
{
    if(memory_lock != 0){
        debug("handle_sigalrm: interrupted memory operation");
        start_timer(SCHED_QUANTA);
        return;
    }
    if(segv_lock != 0){
        debug("handle_sigalrm: interrupted segv operation");
        start_timer(SCHED_QUANTA);
        return;
    }
    debug("handle_sigalrm: start function for thread %d",
        cur_thread->thread_id);
    if(signal != SIGVTALRM){
        debug("handle_sigalarm: got wrong signal %d", signal);
        return;
    }
    start_timer(0);                                     //disables timer firing
    debug("Tick");

    /* Create new scheduler context */
    getcontext(&signal_context);

    //This is getting overwritten on each call
    signal_context.uc_stack.ss_sp = signal_stack; 
    signal_context.uc_stack.ss_size = STACKSIZE;
//    int ret = VALGRIND_STACK_REGISTER(signal_stack, signal_stack + STACKSIZE);
//    check(ret == 0, "handle_siglarm: VALGRIND_STACK_REGISTER Failed");
    signal_context.uc_stack.ss_flags = 0;
    sigemptyset(&signal_context.uc_sigmask);
    makecontext(&signal_context, scheduler, 0);

    /* save running thread, jump to scheduler */
    check(swapcontext(&cur_thread->context,&signal_context) == 0,
        "Stack overflow");
    debug("handle_sigalrm: end function for thread %d",
        cur_thread->thread_id);
}

/*
    my_pthread lib safe sleep that uses timers to sleep instead of sigalarm.
*/

void waitFor (unsigned int secs)
{
    debug("waitFor: started function");
    unsigned int retTime = time(0) + secs;                // Get finishing time
    while (time(0) < retTime){
    // Loop until it arrives.
    };
    debug("waitFor: ended function");
}

/*
  The call to sigprocmask sets the block for sig alarm process wide
  it does not matter what context this is called in.
*/

void block_sigalrm()
{
    check(sigprocmask(SIG_BLOCK, &signal_set, NULL) == 0,
            "block_sigalrm: failed to set signal block"); 
}

/*
    undoes the block
*/

void unblock_sigalrm()
{
    check(sigprocmask(SIG_UNBLOCK, &signal_set, NULL) == 0,
            "unblock_sigalrm: failed to unset signal block"); 
}


/*
    Insert into calendar que, priority can be in range [1,PRIORITYLEVELS]
*/
int ready_insert(my_pthread_t *ins, int pri)
{
    //arg checks
    check_mem(ins); 
    check( 0 < pri && PRIORITYLEVELS > pri,
        "ready_insert: Prority index out of range :  %d < %d < %d", 
        1, pri, PRIORITYLEVELS);

    //relative offset with out mod

    int ind = cur_pri + pri;
    if (ind >= PRIORITYLEVELS){
        ind = ind - PRIORITYLEVELS;
    }

    TAILQ_INSERT_TAIL(&ready_head[ind], ins, tailq_struct);
    debug("ready_insert: inserted thread %d into que %d", ins->thread_id, ind);
    return 0;
}

/* 
    Gets the next item from the calendar que. Moves cur_pri forward if needed
*/

my_pthread_t * get_next(void)
{
    //find next non-empty queue
    int around = 0;
    my_pthread_t *np = ready_head[cur_pri].tqh_first;
    while (np == NULL && around < PRIORITYLEVELS){
        cur_pri = cur_pri + 1;
        if (cur_pri >= PRIORITYLEVELS){                          //fast modulus
            cur_pri = PRIORITYLEVELS - cur_pri;
        }
        around ++;                             //Don't go around more than once
        np = ready_head[cur_pri].tqh_first;
    }
    check(np != NULL, "All ready queues empty");

    TAILQ_REMOVE(&ready_head[cur_pri], ready_head[cur_pri].tqh_first,
                  tailq_struct);
    //debug("get_next: exited at cur_pri %d",cur_pri);
    return np;
}

//debug related code
/*
   This constructor runs when the library is included. It ensures that the 
   error_context is populated before we ever try to jump to it.
*/

void con(void)
{
    getcontext(&error_context);
    error_context.uc_stack.ss_sp = calloc(STACKSIZE, 1);
    debug("con: allocated error stack %p", error_context.uc_stack.ss_sp);
    error_context.uc_stack.ss_size = STACKSIZE;
    error_context.uc_stack.ss_flags = 0;
    sigemptyset(&error_context.uc_sigmask);
    makecontext(&error_context, (void (*)(void))error, 0);
}

/*
   For now the error contexts just exits. It could potentially do some form of
   clean up
*/
void error(void)
{
    warn("Entered error context");
    exit(1);
}
