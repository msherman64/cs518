// Shridatt Sugrim and Michael Sherman
// Tested on singleton.cs.rutgers.edu
#include <stdio.h>
#include <stdlib.h> //rand()
#include "my_pthread_mem.h"

int main (int argc, char * argv[]){
    info("TEST: Starting");
    int *foo[100];
    int i = 0;
    int j = -1;

    for(i = 0; i < 100; i++){
        debug("TEST: loop count is %d", i);
        foo[i] = malloc( 10 * sizeof(char)); 
        debug("TEST: foo address is %p", foo[i]);
    }


    j = rand()%100;
    info("TEST: freeing index %d, addr:  %p", j, foo[j]);
    free(foo[j]);

    j = rand()%100;
    info("TEST: freeing index %d, addr:  %p", j, foo[j]);
    free(foo[j]);
    
    j = rand()%100;
    info("TEST: freeing index %d, addr:  %p", j, foo[j]);
    free(foo[j]);

    info("TEST: last addr:  %p", foo[i-1]);
    info("TEST: malloc'd addr %p", malloc( 10 * sizeof(char))); 
    info("TEST: malloc'd addr %p", malloc( 10 * sizeof(char))); 
    info("TEST: malloc'd addr %p", malloc( 10 * sizeof(char))); 


}
