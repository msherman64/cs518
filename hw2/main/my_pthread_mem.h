// Shridatt Sugrim and Michael Sherman
// Tested on singleton.cs.rutgers.edu
#ifndef _MY_PTHREAD_MEM_H_
#define _MY_PTHREAD_MEM_H_

#include <unistd.h> //sysconf
#include <sys/queue.h>      //TAILQ_*
#include <ucontext.h>       // ucontext_t
#include <stdio.h> //printf
#include <signal.h> //for handler and signals

//thread lib defines
#define STACKSIZE 8192    //default stack size
#define SCHED_QUANTA 50   //How long a thread is scheduled for
#define MAXQUANTA 1000    //Longest a thread can be scheduled for
#define MAXTHREADS 8192   // Limit on the number of threads you can schedule
#define PRIORITYLEVELS 10 //Number of queues in the calendar queueu
#define MAX_PRI_TICKS 5   // numer of ticks before we bump priority

//memory lib defines
#define THREADREQ 1 //flag to indicate call from user code
#define LIBREQ 2 //flag to indicate call from lib
#define PAGESIZE sysconf( _SC_PAGE_SIZE) //this should be 4096
#define MEMSIZE (8<<20) //8MB
//NUMPAGES should be memsize/pagesize - metadata
//arbitrarily setting to 2000 to prevent overallocation
//in our environment, MEMSIZE / PAGESIZE = 2048
//to track 2000 pages in a page table requires 4 pages after alignment. 
//each page table entry is 8 bytes
//to track 4096 swap pages we need 4 pages, each entry is 4 bytes
#define NUMPAGES 2000 
#define NUMSWAP 4096
#define INDISK 2
#define INMEM  1
#define NOALLOC 0

//thread lib externs
typedef struct
{
    int offset; //offset into either memory or swap file. Indicated by type
    int stored; //0 never allocated, 1 memory, 2 disk
} thread_page_struct;

typedef struct _my_pthread_t
{
    int thread_id;        //Numeric thread identifer (just counts up)
    int priority;         // level at whcich we are reinserted into ready_queue
    int ticks;            // counter for number of ticks at current priority
    int finished;         // Tells us when to no longer schedule this thread
    struct _my_pthread_t *joined_by;   //who is joining this thread
    struct _my_pthread_t *waiting_for; //thread this is wating for
    ucontext_t context;   //Uses for calls to get/set context

    //Part of the callers memory that we will store the retrun value in
    void *func_arg;
    void *retval;
    thread_page_struct tps[NUMPAGES];

    //Next and Prev Pointers for ready or waiting queue
    TAILQ_ENTRY(_my_pthread_t) tailq_struct; 
} my_pthread_t;          //Type of the elements in the TAILQ

my_pthread_t *all_threads[MAXTHREADS]; // all threads
my_pthread_t *cur_thread;   //forward declaration of pointer to active thread

//memory lib externs

void *startpages; // start of pages
void *start_swap_table; //start of swap table
void *start_page_table; //start of table
int memory_lock;
int segv_lock;

typedef struct _metablock {
    int size;
    struct _metablock *next;
} metablock;



/* debug macros*/

ucontext_t error_context;           /* the error context context */

//should this be write instead of printf?

#ifndef NDEBUG
#define debug(M, ...)
#else
#define debug(M, ...) fprintf(stderr, "DEBUG %s:%d: " M "\n",\
        __FILE__, __LINE__, ##__VA_ARGS__)
#endif

#define warn(M, ...) fprintf(stderr, "WARN %s:%d: " M "\n",\
        __FILE__, __LINE__, ##__VA_ARGS__)

#define info(M, ...) fprintf(stderr, "INFO %s:%d: " M "\n",\
        __FILE__, __LINE__, ##__VA_ARGS__)

//A is what we want to assert is true.
#define check(A, M, ...) if(!(A)) {\
    debug(M, ##__VA_ARGS__); setcontext(&error_context); }

// we seem to be having concurrency issues with jumping to error context
// exit immediately instead, to preserve asserts 
//#define check(A, M, ...) if(!(A)) {\
//    debug(M, ##__VA_ARGS__); exit(1); }

//If pointer is null, then !pointer is True
#define check_mem(A) check((A), "Pointer was null")

//Constructor for creating error context
//void con() __attribute__((constructor)); 
void con(void);
void error(void);

#define STACKSIZE 8192    //default stack size


/* Internal constants and structures */
//#define EDEADLK -2        // deadlock
//#define EINVAL -3         // thread is not joinable or already joined by another
//#define ESRCH -4          // could not find thread
//commented out values due to inclusion of ESRCH

#define pthread_t my_pthread_t
#define pthread_mutex_t my_pthread_mutex_t
#define pthread_yield my_pthread_yield
#define pthread_exit my_pthread_yield
#define pthread_join my_pthread_join
#define pthread_create my_pthread_create
#define pthread_mutex_init my_pthread_mutex_init
#define pthread_mutex_lock my_pthread_mutex_lock
#define pthread_mutex_unlock my_pthread_mutex_unlock
#define pthread_mutex_destroy my_pthread_mutex_destroy


void setup() __attribute__((constructor)); 
void setup();

typedef TAILQ_HEAD(tailhead, _my_pthread_t) head_t;    //Type of the TAILQ head

// A list entry for our mutex list. Contains a pointer to a thread
typedef struct _mutex_list_t
{
    my_pthread_t *thread_p; //pointer to thread locking mutex
    TAILQ_ENTRY(_mutex_list_t) m_tailq_struct; //List structure
} mutex_list_t; //type of element in the queue

//Head for a mutex list (as opposed to a pthread list)
typedef TAILQ_HEAD(m_tailhead, _mutex_list_t) m_head_t;

//this is passed to mutex functions by reference as arg.
typedef struct _my_pthread_mutex_t
{
    m_head_t *lock_q; //pointer to fifo queue of locking threads
} my_pthread_mutex_t; //type of element in the queue

/*
    We're never going to take one of these it's just here to prevent complier
    complaints 
*/
typedef struct {
} my_pthread_attr_t; 


/* exposed API */
void my_pthread_yield();
void my_pthread_exit(void *value_ptr);
int my_pthread_join(my_pthread_t thread, void **value_ptr);
int my_pthread_create( my_pthread_t *pt,
                       my_pthread_attr_t *attr,
                       void *(*function)(void*),
                       void * arg);

/* mutex API */

//attributes are ignored
int my_pthread_mutex_init(my_pthread_mutex_t *mutex,
                          const pthread_mutexattr_t *mutexattr);

int my_pthread_mutex_lock(my_pthread_mutex_t *mutex);
int my_pthread_mutex_unlock(my_pthread_mutex_t *mutex);

//mutex should be unlocked before destroy
int my_pthread_mutex_destroy(my_pthread_mutex_t *mutex);

/* Support functions*/
void init_timer(int ms);
void start_timer (int ms);
void run_thread(void *(*function)(void *));
void handle_sigalrm(int signal);

/* Convience functions*/
void waitFor (unsigned int secs);
void block_sigalrm();
void unblock_sigalrm();
int ready_insert(my_pthread_t *ins, int pri);
my_pthread_t * get_next(void);



#define malloc(x) myallocate(x, __FILE__, __LINE__, THREADREQ)
#define free(x) mydeallocate(x, __FILE__, __LINE__, THREADREQ)
#define real_free(x) mydeallocate(x, __FILE__, __LINE__, LIBREQ)


void * myallocate(size_t size, char *filename, int linenum, int from_lib);
void mydeallocate(void *address, char *filename, int linenum, int from_lib);

typedef struct {
    int thread_id; //ex owner is thread 3
    int virt_page_id; // ex thread 3's page index 2
} page_table_entry;


//void initpagetable() __attribute__((constructor)); 
void initpagetable();

//gets available block, of size_t size, may allocate pages as needed
void * find_available_block(size_t size);
//allocates enough pages for size_needed, starting at page_boundary
int allocate_pages(size_t size_needed, char *page_boundary); 

void * find_available_page(); //locating a page to swap into
int swap_pages(int offset); //swaps one page, into a available page
int init_thread_page(int thread_id); //initializes a available page for a thread

void segfault_handler(int sig, siginfo_t *si, void *unused);
void * get_page_boundary( void *cur_addr);
int get_page_offset( void *cur_addr);
page_table_entry * get_page_table_entry(int offset);
void * get_page_addr(int offset);

//swap related functions
int evict(); 
void copy_in_mem(int offset); 
int copy_out_mem(int offset); 
int copy_in_disk(int offset); 
int copy_out_disk(int offset);
int find_available_disk_page();
int * get_swap_table_entry(int offset);



#endif
