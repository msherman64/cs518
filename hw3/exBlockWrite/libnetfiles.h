#ifndef _LIBNETFILES_H_
#define _LIBNETFILES_H_
//header for libnetfiles library
//implementations of open, close, read, write, via network socket
//netopen, netclose, netread, netwrite
//note, file descriptors will be negative to avoid overlap with system ones

//#include <sys/types.h>
#include <netinet/in.h> //for sockaddr_in type

int netopen();
/* send opr_filename
 * receive opr_ret, act on return values
 */

int netclose();
/* send opr_fd
 * recevie opr_ret, act on return values
 */

int netread();
/* send opr_size, includes FD
 * while(total recv size < req_size)
 * read (sock_fd, metabuf, sizeof(opr_ret)
 * if, opr_ret includes num bytes following
 *    read (sock_fd, buffer, opr_ret.numbytes)
 * else, get error code from opr_ret, break;
 * }
 */

int netwrite();
/* send opr_size, includes FD
 * while (total written size < req_size)
 */

void init_sockaddr(
        struct sockaddr_in *name, 
        const char *hostname, 
        uint16_t port);

int connect_socket(struct sockaddr_in *servername, 
                const char* serverhost, uint16_t port);

#endif
