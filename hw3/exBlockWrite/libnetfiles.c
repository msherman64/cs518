#include <stdlib.h> //for exit
#include <stdio.h> //for fprintf
#include <netinet/in.h> //for sockaddr_in
#include <netdb.h> //for getyhostbyname
#include <sys/socket.h> //for AF_INET
#include <string.h> //for memcpy

#include "libnetfiles.h"
#include "dbg.h"
#include "common.h" //definitions for client and server



void
init_sockaddr (struct sockaddr_in *name,
               const char *hostname,
               uint16_t port)
{
    struct hostent *hostinfo;
    name->sin_family = AF_INET;
    name->sin_port = htons(port);
    hostinfo = gethostbyname(hostname);
    check(hostinfo != NULL, "Unknown host %s.\n", hostname);
    //copy address from hostent struct to sockaddr_in struct on the heap
    memcpy( &(name->sin_addr.s_addr), hostinfo->h_addr_list[0], hostinfo->h_length);
//  name->sin_addr.s_addr = *(struct in_addr *) hostinfo->h_addr;
    debug("socket addr %u", name->sin_addr.s_addr);
}


int connect_socket(struct sockaddr_in *servername, 
                const char* serverhost, uint16_t port)
{

    int sock = socket (AF_INET, SOCK_STREAM, 0);
    check((0 <= sock), "socket (client)");
    
    /* Connect to the server. */
    debug("make_socket: calling init_sockaddr, servername at %p", servername);
    init_sockaddr (servername, serverhost, port);
    check(0 <= connect(sock,(struct sockaddr *) servername, 
                sizeof (*servername)),
         "make_socket, connection failed" );

    return sock;
}

