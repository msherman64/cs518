#include <stdio.h>                 //printf
#include <stdlib.h>                //malloc
#include <signal.h>                //sigaction
//#include <sys/time.h>              // setitimer
//#include <sys/queue.h>             //TAILQ_*
//#include <string.h>                //memset
#include <ucontext.h>              // *context()
//#include <time.h>                  //for time() function
#include "dbg.h"            //Our library

/*
   This constructor runs when the library is included. It ensures that the 
   error_context is populated before we ever try to jump to it.
*/

void con(void)
{
    getcontext(&error_context);
    error_context.uc_stack.ss_sp = malloc(STACKSIZE);
    debug("con: allocated error stack %p", error_context.uc_stack.ss_sp);
    error_context.uc_stack.ss_size = STACKSIZE;
    error_context.uc_stack.ss_flags = 0;
    sigemptyset(&error_context.uc_sigmask);
    makecontext(&error_context, (void (*)(void))error, 0);
}

/*
   For now the error contexts just exits. It could potentially do some form of
   clean up
*/
void error(void)
{
    warn("Entered error context");
    exit(1);
}

