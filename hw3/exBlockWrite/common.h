#ifndef _COMMON_H_
#define _COMMON_H_

#include <sys/types.h> //for uint8_t

#define SERVERNAME "mediator.cs.rutgers.edu"
#define SERVERPORT 8765

//client command types
#define NET_OPEN  1
#define NET_CLOSE 2
#define NET_READ  3
#define NET_WRITE 4

//server response types
#define NET_OPEN_RET  11
#define NET_CLOSE_RET 12
#define NET_READ_RET  13
#define NET_WRITE_RET 14


//struct to signal which operation is requested
struct opr{
    uint8_t operation; //one of NETOPEN, NETCLOSE, NETREAD, NETWRITE
};

struct opr_ret{
    uint8_t operation;
    /* retval has multiple uses
     * for open, returns FD or -1 on error
     * for close, return 0, or -1 on error
     * for read, returns number of bytes following this message = num read
     * for write, returns number of bytes written
     */ 
    uint64_t retval;
    uint8_t errno;
};

struct opr_filename{
    uint8_t operation; //one of NETOPEN, NETCLOSE, NETREAD, NETWRITE
    char fname[512];
    //TODO flags
};

struct opr_size{
    uint8_t operation; //one of NETOPEN, NETCLOSE, NETREAD, NETWRITE
    uint16_t fd;
    size_t nbyte; //size of read or write
    //struct is followed by nbyte raw bytes
};

struct opr_fd{
    uint8_t operation; //one of NETOPEN, NETCLOSE, NETREAD, NETWRITE
    uint16_t fd;
};

#endif
