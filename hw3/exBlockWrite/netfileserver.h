#ifndef _NETFILESERVER_H_
#define _NETFILESERVER_H_

int make_socket (uint16_t port);

int accept_connection();

void * read_message(void * arg);


void server_net_read( ); //responds to read command from client
/* now know disk file descriptor, and num bytes requested
 * allocate buffer size BUFF_SIZE
 * while (sent < req_size) loop {
 * read_size = try to read max(BUFF_SIZE, req_size) from disk file descriptor
 * if successfull, send opr_ret, with size = read_size
 *      write(sock_fd, buff, read_size)
 * else
 *      send opr_ret, with size = -1, set errno
 *      break
 * end loop}
 *
 *
 */
void server_net_write( ); //responds to read command from client
/* know disk file descriptor, and num bytes requested to write
 * allocate buffer size BUFF_SIZE
 * while (written < req_size){
 *    read metadata block from sock_fd to meta buffer
 *    check for errors
 *    read size(determined from metadata) bytes from sock_fd to buffer
 *    write(file_fd, buffer, size from metadata)
 *    if successfull
 *      send opr_ret, size = bytes written
 *    else
 *      send opr_ret, size = -1, set errno
 *      }
 */ 
void server_net_open( ); //responds to read command from client
/* know file name (includes full path) 
 * attempt to open file
 * if successfull, send opr_ret, with FD
 * if fail, send opr_ret with -1, and errno
 */
void server_net_close( ); //responds to read command from client
/* know file descriptor
 * attempt to close fd
 * send opr_ret with retval, and errno if applicable
 */












#endif
