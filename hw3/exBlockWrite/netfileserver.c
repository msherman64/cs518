#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h> //for bzero
#include <unistd.h> //for read, write, close, etc.
#include <pthread.h> //threads for accepting connections

#include "netfileserver.h"
#include "dbg.h"
#include "common.h" //definitions for client and server


int
make_socket(uint16_t port)
{
    int status = 0;  
    int sock = 0;
    struct sockaddr_in name = { 0 };
  
    // Create the socket.
    sock = socket(AF_INET, SOCK_STREAM, 0);
    check(0 <= sock, "make_socket: failed to make socket");
 
    //set linger option for socket 
    struct linger linger = { 0 }; //null out struct
    linger.l_onoff = 1; //enable option
    linger.l_linger = 0; //set linger time to 0
    status = setsockopt(sock,
        SOL_SOCKET, SO_LINGER,  //socket api, linger option
        (const char *) &linger,
        sizeof(linger));
    check(0 <= status, "make_socket: failed to set options");
  
    /* Give the socket a name. */
    name.sin_family = AF_INET;
    name.sin_port = htons(port);
    name.sin_addr.s_addr = htonl(INADDR_ANY);
  
    //bind socket address
    status = bind(sock, (struct sockaddr *) &name, sizeof(name)); 
    check(0 <= status, "make_socket: failed to bind socket");

    //listen on socket
    status = listen(sock, 5);  //file descriptor, backlog.
    check(0 <= status, "make_socket: failed to listen on socket");

  
    return sock;
}
void * read_message(void *arg){

    int fd = *(int *)arg; 
    check(0 < fd, "read_message: invalid file descriptor");
    debug("read_message: starting new thread with fd %d", fd);

//    char buffer[2048];
//    bzero(buffer,sizeof(buffer));
    struct opr *foo = NULL;
    int i = 0;
    int n = 0;
    int retval = -1;
/*    while(n < sizeof(struct opr)){
        retval = read(fd, buffer + n , sizeof(struct opr) - n);
        check(0 <= retval, "read_message: error reading from socket");
        n += retval;
        debug("read_message: read %d bytes from socket", n);
    }
    foo = (struct opr *)&buffer;
*/
    int buff[256];
    while(1){
        retval = read(fd, buff, sizeof(buff));
        check(0 <= retval, "read_message: error reading from socket");
        n += retval;
        debug("read_message: read %d bytes from socket, loop %d", retval, i++);
        sleep(1);

    }
    
    //info("read_message: Here is the message: %s", foo->fname);
    //info("read_message: Here is the opr: %d", foo->operation);

    close(fd);

    return NULL;
}


void server_net_read(){








}

int main(){
    debug("main: starting");
    int listen_fd = make_socket(SERVERPORT);

    struct sockaddr_in cli_addr = { 0 };
    socklen_t clilen = sizeof(cli_addr);
    int newsockfd = -1;

    pthread_t *worker;

    while(1){
        newsockfd = accept(listen_fd, 
                    (struct sockaddr *) &cli_addr, 
                    &clilen);
        check(0 <= newsockfd, "main: Failed to accept on socket");
        worker = malloc(sizeof(pthread_t));
        pthread_create(worker, NULL, read_message, &newsockfd);
    }

    close(listen_fd); //will never reach here
    return 0;
}
