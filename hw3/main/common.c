#include <unistd.h> //for read, write, close, etc.
#include <string.h>
#include <stdio.h>
#include <stdint.h> //uints
#include <string.h> //for bzero

#include "dbg.h"
#include "common.h"

ssize_t reliable_read(int fd, void *buff, size_t size){
    ssize_t bytes_read = 0;
    ssize_t retval = 0;
    while(bytes_read < size){
        retval = read(fd, buff + bytes_read, size - bytes_read);
        if(0 == retval){ //end of file
            debug("relable_read: EOF returning early, fd %d bytes %zd",
                    fd, bytes_read);
            return bytes_read;
        }else if(0 > retval){ //error reading
            debug("relable_read: error reading file, returning early");
            return retval;
        }
        bytes_read += retval;
    }
    return bytes_read; //should equal size

};

ssize_t reliable_write(int fd, void *buff, size_t size){
    ssize_t bytes_write = 0;
    ssize_t retval = 0;
    while(bytes_write < size){
        retval = write(fd, buff + bytes_write, size - bytes_write);
        if(0 == retval){ //no space on device, unable to continue writing
            debug("relable_write: write finished early, fd %d bytes %zd",
                    fd, bytes_write);
            return bytes_write;
        } else if(0 > retval){ //error condition
            debug("reliable_write: error on write");
            return retval;
        }
        bytes_write += retval;
    }
    return bytes_write; //should equal size

};

int read_opr(int sock_fd, char *buff, size_t buffsize){

    bzero(buff, buffsize);
    struct opr *opr_unknown = NULL;
    ssize_t retval = -1;
    ssize_t bytes_read = 0;
    //read header
    retval = reliable_read(sock_fd, buff, sizeof(struct opr));
    check ((0 <= retval),"read_opr: unable to read sizeof(opr) from fd %d", 
            sock_fd);
    bytes_read += retval;
    opr_unknown = (struct opr *)buff;
    debug("read_opr: opr_unknown opr %d, length %d", 
            opr_unknown->operation, opr_unknown->length);
    int length = opr_unknown->length - sizeof(struct opr);
    //read payload
    retval = reliable_read(sock_fd, 
                        buff + sizeof(struct opr), length);
    check ((0 <= retval),"read_opr: unable to read length %d from fd %d", 
                        length, sock_fd);
    bytes_read += retval;
    //return type of message
    debug("read_opr: read %zd bytes", bytes_read);
    return opr_unknown->operation;

}

ssize_t 
send_response(int sock_fd, int operation, uint64_t value, uint8_t err_value){
    /* send a return message with value and errno */
    struct opr_ret return_message;
    bzero(&return_message, sizeof(return_message));
    return_message.length = sizeof(return_message);
    return_message.operation = operation;
    return_message.value = value; //The bytes the recieve side should expect
    return_message.error_value = err_value;

    //send response message
    ssize_t bytes_sent = 0;
    bytes_sent = reliable_write(sock_fd, &return_message, sizeof(return_message));
    debug("send_response: return message bytes sent: %zd", bytes_sent);
    return bytes_sent;
}
