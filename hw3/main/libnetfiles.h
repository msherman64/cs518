#ifndef _LIBNETFILES_H_
#define _LIBNETFILES_H_
//header for libnetfiles library
//implementations of open, close, read, write, via network socket
//netopen, netclose, netread, netwrite
//note, file descriptors will be negative to avoid overlap with system ones

//#include <sys/types.h>
#include <netinet/in.h> //for sockaddr_in type
#include "common.h"

struct client_fd{
    off_t offset;
    int numopen; //0 for no, 1 for yes
};

struct net_port_arg{
    struct opr_port req_info; //What you want the operation to do
    int port; //The port you want to call on
    void *buff; //The buffer you want to act on
};

/* for reference
    struct opr_port{
        int opr_type;
        int length;
        int req_off;
        int req_size;
        int disk_fd;
    };
*/

void * net_port_read(void * arg); //thread function for read on port
void * net_port_write(void * arg); //thread function for write on port

int netopen(const char *pathname, int flags);
/* Takes pathname and flags, sends command to server
 * flags can be one of: O_RDONLY, O_WRONLY, or O_RDWR
 * these can be bitwise ored with O_CREAT
 * if O_CREAT is specified, permissions are set to 777
 *
 * Messages:
 * send opr_filename
 * receive opr_ret, return opr_ret.retval
 */

int netclose(uint16_t fd);
/* send opr_fd
 * recevie opr_ret, act on return values
 */

ssize_t netread(int fildes, void *buf, size_t nbyte);
/* send opr_size, includes FD
 * while(total recv size < req_size)
 * read (sock_fd, metabuf, sizeof(opr_ret)
 * if, opr_ret includes num bytes following
 *    read (sock_fd, buffer, opr_ret.numbytes)
 * else, get error code from opr_ret, break;
 * }
 */

ssize_t netwrite(int fildes, void *buf, size_t nbyte);
/* send opr_size, includes FD
 * while (total written size < req_size)
 */

void init_sockaddr(
        struct sockaddr_in *name, 
        uint16_t port);

int connect_socket(struct sockaddr_in *servername, uint16_t port);

int get_sock_fd();
int netserverinit(char *hostname);

#endif
