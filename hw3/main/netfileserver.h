#ifndef _NETFILESERVER_H_
#define _NETFILESERVER_H_

#include "common.h"

typedef struct {
    char filename[512];
} server_disk_fd;

int safe_open(char * filename, int flags, mode_t mode);
int safe_close(int disk_fd);
int get_dup_fd(int old_fd);

int make_socket (uint16_t port);

int accept_connection();

void * read_message(void * arg);
/* read message is a dispatch function
 * it reads the control message from the sock_fd
 * and then calls the correct function to handle
 * the message based on the operation field of the
 * struct
 *
 * responsible for closing the sock_fd after dispatch is completed
 */


void server_net_read(struct opr_size *message, int sock_fd); 
/* acts on, to NET_READ control message
 * obtains disk_fd and requested bytes from message
 * reads min(requested bytes, BUFF_SIZE) from disk_fd to buffer
 * sends response mesage of type OPR_RET containing bytes read from disk
 * and errno, both from call to read
 * 
 * writes the number of bytes read from disk, from the buffer, into the socket
 * and then returns
 *
 * the receive side is expected to read the number of bytes read from disk,
 * out of the other side of the socket
 */


void server_net_write(struct opr_size *message, int sock_fd); 
/* acts on NET_WRITE control message
 * obtains disk_fd and requested bytes from message
 * net_bytes_read = reads min(requested bytes, BUFF_SIZE) from sock_fd to buffer
 * disk_bytes_written = write net_bytes_read bytes from buffer to disk_fd
 * send response message containing disk_bytes_written and errno
 *
 * it is assumed that the client has sent data of size requested bytes 
 * immediately after the NET_WRITE control message
 * if fewer than request bytes are written, the client will request
 * the remaining on its own, in a new connection
 */

void server_net_open(struct opr_fname *message, int sock_fd); 
//responds to open command from client
/* know file name (includes full path) 
 * attempt to open file
 * if successfull, send opr_ret, with FD
 * if fail, send opr_ret with -1, and errno
 */
void server_net_close(struct opr_fd *message, int sock_fd); 
//responds to close command from client
/* know file descriptor
 * attempt to close fd
 * send opr_ret with retval, and errno if applicable
 */


void server_net_port_list(struct opr_port_list *message, int sock_fd);










#endif
