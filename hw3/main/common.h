#ifndef _COMMON_H_
#define _COMMON_H_

//#include <sys/types.h> //for uint8_t
#include <stdint.h> //uints

#define MAXTHREADS 10
#define SERVERPORT 8765
#define PORTSTART 8500
#define PORTSEP 5

#define BUFFSIZE 2048
#define METABUFFSIZE 1024

//#define MAX_DISK_FD sysconf(_SC_OPEN_MAX) //doesn't work with global alloc
#define MAX_DISK_FD 1024

//client command types
#define NET_OPEN  1
#define NET_CLOSE 2
#define NET_READ  3
#define NET_WRITE 4
#define NET_PORT_READ  5
#define NET_PORT_WRITE  6
#define NET_PORT_LIST  7

//server response types
#define NET_OPEN_RET  11
#define NET_CLOSE_RET 12
#define NET_READ_RET  13
#define NET_WRITE_RET 14

#define NET_PORT_LIST_RET  17

//Failure types
#define ARG_FAILURE   -100
#define SEEK_FAILURE  -101
#define READ_FAILURE  -102
#define WRITE_FAILURE -103

//struct to signal which operation is requested
struct opr{
    uint8_t operation; //one of NETOPEN, NETCLOSE, NETREAD, NETWRITE
    uint16_t length; // sizeof(opr) == 24
}; 

struct opr_ret{
    uint8_t operation;
    uint16_t length; //sizeof(opr_ret) == 96
    /* retval has multiple uses
     * for open, returns FD or -1 on error
     * for close, return 0, or -1 on error
     * for read, returns number of bytes following this message = num read
     * for write, returns number of bytes written
     */ 
    uint64_t value;
    uint8_t error_value;
};

struct opr_port_list{
    uint8_t operation;
    uint16_t length;
    uint16_t ports[MAXTHREADS];
};

struct opr_port{
    uint8_t operation;
    uint16_t length;
    int req_off;
    int req_size;
    int disk_fd;
};

//Port status struct, theres should be one per Thread
struct port_stat{
    uint16_t port;
    int status;
};

struct opr_fname{
    uint8_t operation; //one of NETOPEN, NETCLOSE, NETREAD, NETWRITE
    uint16_t length; //sizeof(opr_fname) == 529
    uint8_t flag; //one of O_RDONLY, O_WRONLY, O_RDWR, bitwise or with create
    char fname[512];
    //TODO flags
}; 

struct opr_size{
    uint8_t operation; //one of NETOPEN, NETCLOSE, NETREAD, NETWRITE
    uint16_t length; //sizeof(opr_size) == 48
    uint16_t fd;
    ssize_t nbyte; //size of read or write
    off_t offset; //disk offset
    //struct is followed by nbyte raw bytes
};

struct opr_fd{
    uint8_t operation; //one of NETOPEN, NETCLOSE, NETREAD, NETWRITE
    uint16_t length; //sizeof(opr_fd) == 40
    uint16_t fd;
};

ssize_t reliable_read(int sock_fd, void *buff, size_t size);
ssize_t reliable_write(int sock_fd, void *buff, size_t size);
int read_opr(int sock_fd, char *buff, size_t buffsize);
ssize_t 
send_response(int sock_fd, int operation, uint64_t value, uint8_t err_value);

#endif
