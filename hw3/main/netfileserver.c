#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h> //for open, and open modes
#include <string.h> //for bzero
#include <unistd.h> //for read, write, close, etc.
#include <pthread.h> //threads for accepting connections
#include <errno.h> //for errno passing

#include <sys/stat.h> // for stat

#include "netfileserver.h"
#include "dbg.h"
#include "common.h" //definitions for client and server


server_disk_fd fd_array[MAX_DISK_FD]; //MAX_DISK_FD is num of possible disk FDs
struct port_stat ports[MAXTHREADS];  // global port status array

int
make_socket(uint16_t port)
{
    int status = 0;  
    int sock = 0;
    struct sockaddr_in name = { 0 };

    // Create the socket.
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if(0 > sock){
        debug("make_socket: failed to make socket for port %d",
                port);
        return -1;
    }

    //set linger option for socket 
    struct linger linger = { 0 }; //null out struct
    linger.l_onoff = 0; //enable option
    linger.l_linger = 0; //set linger time to 0
    status = setsockopt(sock,
        SOL_SOCKET, SO_LINGER,  //socket api, linger option
        (const char *) &linger,
        sizeof(linger));
    if(0 > status){
        debug("make_socket: failed to set sock_opt for port %d",
                port);
        return -1;
    }

    /* Give the socket a name. */
    name.sin_family = AF_INET;
    name.sin_port = htons(port);
    name.sin_addr.s_addr = htonl(INADDR_ANY);

    //bind socket address
    status = bind(sock, (struct sockaddr *) &name, sizeof(name)); 
    if(0 > status){
        debug("make_socket: failed to bind to port %d",
                port);
        return -1;
    }

    //listen on socket
    status = listen(sock, 5);  //file descriptor, backlog.
    if(0 > status){
        debug("make_socket: failed to listen on port %d",
                port);
        return -1;
    }

    return sock;
}

void * read_message(void *arg){
    /* Read opearion fromc socket and dispatch */
    //get and check arg
    check_mem(arg);
    int sock_fd = *(int *)arg; 
    check(0 < sock_fd, "read_message: invalid file descriptor");
    debug("read_message: starting new thread with sock_fd %d", sock_fd);

    //read the operation struct from the socket file descriptor
    char metabuff[METABUFFSIZE];
    int retval = -1;
    retval = read_opr(sock_fd, metabuff, sizeof(metabuff));

    if (retval == NET_OPEN){
        //operation was OPEN
        struct opr_fname *rec_fname = NULL;
        rec_fname = (struct opr_fname *)metabuff;

        debug("read_message: recieved filename %s", 
                rec_fname->fname);
        server_net_open(rec_fname, sock_fd);
    } else if (retval == NET_CLOSE){
        //operation was close
        struct opr_fd *rec_fd= NULL;
        rec_fd = (struct opr_fd *)metabuff;

        debug("read_message: recieved fd %d", 
                rec_fd->fd);
        server_net_close(rec_fd, sock_fd);

//We're going to handle reads differently
/*    } else if (retval == NET_READ){
        //operation was read
        struct opr_size *rec_size = NULL;
        rec_size = (struct opr_size *)metabuff;

        debug("read_message: recieved fd %d", 
                rec_size->fd);
        server_net_read(rec_size, sock_fd);
    } else if (retval == NET_WRITE){
        //operation was write
        struct opr_size *rec_size = NULL;
        rec_size = (struct opr_size *)metabuff;

        debug("read_message: recieved fd %d", 
                rec_size->fd);
        server_net_write(rec_size, sock_fd);
*/

    } else if (retval == NET_PORT_LIST){
        //operation was port list
        struct opr_port_list *rec_list = NULL;
        rec_list = (struct opr_port_list *)metabuff;

        debug("read_message: recieved port list message length %d",
                rec_list->length);
        server_net_port_list(rec_list, sock_fd);
    } else {
        warn("Unknown oper type %d", retval);
    }
    debug("Handled messge from sock_fd %d",sock_fd);
    close(sock_fd); //We close it on client side instead
    return NULL;
}

void server_net_read(struct opr_size *message, int sock_fd){
    /* receive read command message
     * get nbytes and disk_fd number from message
     * call read(disk_fd, temp_buff, min(buff_size, nbytes))
     * take return value of read, populate response mesage
     * send response message
     * write(sock_fd, temp_buff, read_size)
     * return;
     */

    off_t new_offset = -1; //stores new offset after executing lseek
    off_t prev_offset = -1; //stores unmodified FD offset to be reported
    ssize_t disk_bytes_read = -1;
    //get nbytes and disk_fd from message
    ssize_t nbytes = message->nbyte;
    int disk_fd = get_dup_fd(message->fd);
    off_t requested_offset = message->offset; 
    debug("server_net_read: attempting to read %zd bytes from with disk_fd %d",
            nbytes, disk_fd);
    //initialize buffer
    ssize_t meta_bytes_sent = -1;
    ssize_t bytes_sent = 0;
    char temp_buf[BUFFSIZE];

    //calculate bytes to read
    ssize_t bytes_to_read = -1;
    if(nbytes < BUFFSIZE){
        bytes_to_read = nbytes;
    } else{
        bytes_to_read = BUFFSIZE;
    }

    //Seek to requested position and read requested bytes
    //TODO put mutex lock here
    prev_offset = lseek(disk_fd, 0, SEEK_CUR);
    errno = 0;
    if(-1 != requested_offset){
        new_offset = lseek(disk_fd, requested_offset, SEEK_SET);
        debug("server_net_read: current FD moved to position %ld", new_offset);
    } else{
        debug("server_net_read: no offset requested for FD %d", disk_fd);
    }

    if(-1 != bytes_to_read){
        disk_bytes_read = read(disk_fd, temp_buf, bytes_to_read);
        debug("server_net_read: read %zd bytes from disk, errno %d %s",
                disk_bytes_read, errno, strerror(errno));
    } else {
        debug("server_net_read: no read requested for FD %d", disk_fd);
    }

    //TODO put mutex unlock here

    //create and send response message
    meta_bytes_sent = send_response(
            sock_fd,
            NET_READ_RET,
            disk_bytes_read,
            errno);

    bytes_sent = reliable_write(sock_fd, temp_buf, disk_bytes_read);
    debug("server_net_read: sent %zd bytes", bytes_sent);
    /*ensure consistency of file descriptor on network failure
     * logically this should work, but we are unable to trigger the failure
     * case, so this is untested. The working case (else) functions as expected.
     */


    //file descriptor will be closed by the calling function
}

void server_net_write(struct opr_size *message, int sock_fd){
    int disk_fd = message->fd;
    debug("server_net_write: entered function, with disk_fd %d, size %zu",
            disk_fd, message->nbyte);

    ssize_t meta_bytes_sent = -1;
    size_t target = 0;
    ssize_t net_bytes_read = 0;
    ssize_t disk_bytes_written = 0;
    char temp_buf[BUFFSIZE];

    if(message->nbyte < BUFFSIZE){
        target = message->nbyte;
    } else{
        target = BUFFSIZE;
    }
    debug("server_net_write: byte target is %zu bytes", target);
    //read from socket
    errno = 0;
    net_bytes_read = read(sock_fd, temp_buf, target);
    debug("server_net_write: read %zd bytes from socket, errno %d %s",
            net_bytes_read, errno, strerror(errno));
    //write to disk
    disk_bytes_written = reliable_write(disk_fd, temp_buf, net_bytes_read);
    debug("server_net_write: wrote %zd bytes to disk, errno %d %s",
            disk_bytes_written, errno, strerror(errno));

    //create and send response message
    meta_bytes_sent = send_response(
            sock_fd,
            NET_WRITE_RET,
            disk_bytes_written,
            errno);

} 

void server_net_close(struct opr_fd *message, int sock_fd){
    /* close file descriptor fd received from client */
    int close_status = -1;
    int disk_fd = message->fd;
    ssize_t meta_bytes_sent = -1;

    errno = 0;
    close_status = safe_close(disk_fd);
    debug("server_net_close: closed disk_fd %d, status %d, errno %d %s",
            disk_fd, close_status, errno, strerror(errno));

    meta_bytes_sent = send_response(
            sock_fd,
            NET_CLOSE_RET,
            close_status,
            errno);
}

int safe_open(char * filename, int flags, mode_t mode){
    //ONLY FOR DISK FD

    int disk_fd  = open(filename, flags, mode);
    if( (0 > disk_fd) || (MAX_DISK_FD < disk_fd) ){
        debug("safe_open: disk_fd %d out of bounds for struct", disk_fd);
        errno = EBADF; //invalid file descriptor
        return -1;
    } else {
        //set filename in struct
        strncpy(fd_array[disk_fd].filename, filename, 
                sizeof(fd_array[disk_fd].filename));
        debug("safe_open: set disk_fd %d filename to %s", 
                disk_fd, fd_array[disk_fd].filename);
        return disk_fd;
    }
}

int safe_close(int disk_fd){
    //ONLY FOR DISK FD

    if( (0 > disk_fd) || (MAX_DISK_FD < disk_fd) ){
        errno = EBADF; //invalid file descriptor
        return -1;
    } else {
        //set filename in struct
        debug("safe_open: closing disk_fd %d, filename was %s", 
                disk_fd, fd_array[disk_fd].filename);
        strcpy(fd_array[disk_fd].filename, "");
        return close(disk_fd);
    }
}

int get_dup_fd(int old_fd){
    //ONLY FOR DISK FD
    
    if( (0 > old_fd) || (MAX_DISK_FD < old_fd) ){
        errno = EBADF; //invalid file descriptor
        debug("get_dup_fd: file descriptor %d out of range", old_fd);
        return -1;
    } else if((strcmp(fd_array[old_fd].filename , "")) == 0){ //check if string empty
        errno = EBADF;
        debug("get_dup_fd: file descriptor %d not opened", old_fd);
        return -1;
    } else {
        char *filename = fd_array[old_fd].filename;
        /* mode is only used on create, not using create here */
        return safe_open(filename, O_RDWR, 0); 
    }
}

void server_net_open(struct opr_fname *message, int sock_fd){
    /* opens file on server with read, write, or rw flag.
     * if create also specified, creates with permission 777
     */
    debug("server_net_open: filname is %s", message->fname);
    //check for flag of O_CREAT
    int flag = message->flag;
    int open_status = -1;
    ssize_t meta_bytes_sent = -1;

    errno = 0;
    debug("server_net_open: flag is %d", flag);
    if(flag & O_CREAT){ //bitwise and should return 1 of O_CREAT in flag
        //create file 
        open_status = safe_open(message->fname, message->flag, 00777);
        //TODO creating with wrong permissions, umask issue?
        debug("server_net_open: created file %s", message->fname);
    } else{
        //not creating
        open_status = safe_open(message->fname, message->flag, 00777);
    }
    debug("server_net_open: opened file, got fd %d, errno %d", 
            open_status, errno);

    meta_bytes_sent = send_response(
            sock_fd,
            NET_OPEN_RET,
            open_status,
            errno);
}
void server_net_port_list(struct opr_port_list *message, int sock_fd){
    /* Recieves a port list request from clinet, checks the port stauts
       and returns a list for free ports.
    */

    ssize_t meta_bytes_sent = -1;
    if(NET_PORT_LIST != message->operation){
        meta_bytes_sent = send_response(sock_fd, ARG_FAILURE, -1, -2);
        debug("server_net_port_list: Got a bad oper Type %d",
            message->operation);
        return;
    }

    int i = 0;
    for(i = 0; i < MAXTHREADS; i++){
        if(0 == ports[i].status) { //avaialbe port
            message->ports[i] = ports[i].port;
            debug("server_net_port_list: port %d : %d was available",
                  i, ports[i].port);
        } else {
            message->ports[i] = 0; //not available
            debug("server_net_port_list: port %d : %d was NOT available",
                  i, ports[i].port);
        }
    }
    message->operation = NET_PORT_LIST_RET;
    meta_bytes_sent = reliable_write(sock_fd, message, message->length);
    debug("server_net_port_list: Returned message with operation %d of size %zd",
            message->operation, meta_bytes_sent);
}

//Read Write thread
void * read_write_thread(void *arg){
    /* This thread gets created on startup and handles read requests (one
        at a time) for ever. It's arg is an INT that is the port number
        it will bind to. It retuns nothing. 
    */

    //Entry invariant
    check_mem(arg);

    //socket setupvars
    struct sockaddr_in cli_addr = { 0 };
    socklen_t clilen = sizeof(cli_addr);

    int my_port_index  = *(int *)arg; // worker thread arugment
    struct port_stat *my_port_stat = &ports[my_port_index]; //my port status
    int sock_fd = 0; // Holds the current sock_fd
    int disk_fd = 0; // Holds the current disk_fd
    int opr_type = 0; // Type read from socket
    char metabuff[METABUFFSIZE]; //Where the meta oper goes
    char databuff[BUFFSIZE];//Where the data to respond will go
    ssize_t read_bytes = 0; //Bytes read from an fd (disk or sock)
    ssize_t write_bytes = 0;//Bytes written to fd (disk or sock)
    size_t opr_limit = BUFFSIZE; //min(req_size, BUFFSIZE)
    off_t new_offset; //Where the lseek ends up
    struct opr_port *message = NULL; //Should point to buff when we read

    //Bind to port number my_arg.port

    int i = 0;
    int listen_fd = -1;
    int used_port = 0;
    for(i = 0; i < PORTSEP; i++){
        used_port = my_port_stat->port + i;
        listen_fd = make_socket(used_port);
        if(listen_fd > 0){
            debug("read_write_thread: thread %d bound to port %d",
                my_port_index, used_port);
            my_port_stat->port = used_port;
            break;
        }
    }
    check( 0 < listen_fd, "Failed to create socket for port %d",
       my_port_stat->port);
    debug("read_write_thread: Create listen_socket %d",listen_fd);


    while (1){
        //Ready for connection
        my_port_stat->status = 0; // status 0 inidicates ready for connection
        sock_fd = accept(listen_fd,  //Block until connection
                        (struct sockaddr *) &cli_addr, 
                        &clilen);
        my_port_stat->status = 1; // status 1 inidicates working
        if(0 >= sock_fd){
            warn("read_write_thread: Failed to accept on socket");
            continue; //Go back to waiting
        }
        debug("read_write_thread: accepted socket fd %d", sock_fd);


        /*
           Act on socket request
        */

        //get operation and check type
        message = NULL; // Just to be neat I null this before this call to prevent previous loop iterations from carrying over
        opr_type = read_opr(sock_fd, metabuff, sizeof(metabuff));
        debug("read_write_thread: returned from read_opr, type is %d", 
                opr_type);
        if((NET_PORT_READ != opr_type) && (NET_PORT_WRITE != opr_type)){
            //This port only accept read/write requests 
            warn("read_write_thread: got wrong opr_type");
            send_response(sock_fd, ARG_FAILURE, -1, EINVAL);
            close(sock_fd);
            continue; // Skip this iteration (saves on allocations) and block accept again
        }

        debug("read_write_thread: handling opr_type %d, on port %d", 
                opr_type, my_port_stat->port);
        /*
          Operations common to both read and write
        */

        //Format message
        message = (struct opr_port *) metabuff;
        debug("read_write_thread: recevied offset %d, size %d, disk_fd %d",
                message->req_off, message->req_size, message->disk_fd);

        //seek fd to correct position
        errno = 0;
        disk_fd = get_dup_fd(message->disk_fd);
        if(-1 == disk_fd){
            send_response(sock_fd, ARG_FAILURE, -1, errno);
            close(sock_fd);
            continue; // Skip this iteration and block accept again
        }
        new_offset = lseek(disk_fd, message->req_off, SEEK_SET);
        if( 0 > new_offset){ //Seek Failure
            send_response(sock_fd, SEEK_FAILURE, -1, errno);
            close(sock_fd);
            safe_close(disk_fd);
            warn("read_write_thread: Failed to seek to %d", message->req_off);
            continue;
        }
        debug("read_write_thread: seeked fd %d to %ld", disk_fd, new_offset);
        opr_limit = BUFFSIZE;
        if (message->req_size < BUFFSIZE){
            opr_limit = message->req_size;
        }
        debug("read_write_thread: MARKER working on at most %lu bytes",opr_limit);

        /*
            Work on read or write
        */

        bzero(databuff,BUFFSIZE);
        if(NET_PORT_READ == opr_type){ // READ CASE
            debug("read_write_thread: got read message %d", opr_type);
            //read disk
            read_bytes = read(disk_fd, databuff, opr_limit);
            if( 0 > read_bytes){ //Read Failure
                send_response(sock_fd, READ_FAILURE, -1, errno);
                close(sock_fd);
                safe_close(disk_fd);
                debug("read_write_thread: Failed to read to %lu from disk",
                        opr_limit);
                continue;
            }
            debug("read_write_thread: MARKER fd %d, read disk %zd bytes", 
                    disk_fd, read_bytes);
            send_response(sock_fd, NET_READ_RET, read_bytes, errno);

            /*
            make one attempt on write, if fails the clinet will know and it's 
            the clients job (and ultimatley the users job) to seek again and 
            retry
            */

            //write socket
            write_bytes = write(sock_fd, databuff, read_bytes);
            debug("read_write_thread: MARKER sent %zd bytes", write_bytes);

        } else if (NET_PORT_WRITE == opr_type){ //WRITE CASE
            debug("read_write_thread: got write message %d", opr_type);
            //read socket
            read_bytes = reliable_read(sock_fd, databuff, opr_limit);
            if( 0 > read_bytes){ //Read Failure
                close(sock_fd); //Can't report any thing cuz socket read failed
                safe_close(disk_fd);
                debug("read_write_thread: Failed to read %lu from socket", 
                    opr_limit);
                continue;
            }
            debug("read_write_thread: read from socket %zd bytes", read_bytes);

            /*
            This may be -1, but if it is we can't do any thing about it 
            besidies report it (which we're doing any way)
            */

            //write disk
            write_bytes = write(disk_fd, databuff, read_bytes);
            debug("read_write_thread:  wrote %zd bytes to disk", write_bytes);
            send_response(sock_fd, NET_WRITE_RET, write_bytes, errno);

        } else { //Should not get here
            send_response(sock_fd, ARG_FAILURE, -1, errno);
            debug("read_write_thread: got a bad arg %d",opr_type);
        }

        close(sock_fd); //Don't do this because the client will
        safe_close(disk_fd); //Done with this FD 
    }

    //Should never get here.
    close(listen_fd);
    return NULL; 
}

int main(){
    debug("main: starting");
    int listen_fd = make_socket(SERVERPORT);

    pthread_t *worker;
    //Initalize the port status array
    int i = 0;
    int tids[MAXTHREADS] = {0};
    for(i =0; i < MAXTHREADS; i++){
        tids[i]=i;
        ports[i].port = PORTSTART + (i * PORTSEP);
        ports[i].status = 1; // Will get set to zero right before the accept
        worker = malloc(sizeof(pthread_t));
        //these will not get freed, live for life of the program
        pthread_create(worker, NULL, read_write_thread, &tids[i]);
    }



    struct sockaddr_in cli_addr = { 0 };
    socklen_t clilen = sizeof(cli_addr);
    int newsockfd = -1;


    while(1){
        newsockfd = accept(listen_fd, 
                    (struct sockaddr *) &cli_addr, 
                    &clilen);
        //check(0 <= newsockfd, "main: Failed to accept on socket");
        worker = malloc(sizeof(pthread_t));
        pthread_create(worker, NULL, read_message, &newsockfd);
        //TODO need to find out how to free these
    }

    close(listen_fd); //will never reach here
    return 0;
}
