#include <sys/types.h> //for uint16_t, etc.
#include <stdio.h> //for fprintf
#include <netinet/in.h> //for sockaddr_in
#include <stdlib.h> //malloc
#include <unistd.h> // for sleep
#include <string.h> //strlen
#include <fcntl.h> //for open types
#include <errno.h> //for errno, strerror
#include <netdb.h> //h_errno
#include <pthread.h>

#include "libnetfiles.h"
#include "dbg.h"
#include "common.h"




int main(){
    int connection = netserverinit("mediator.cs.rutgers.edu");
    debug("main: connection status %d %d %s", 
            connection, h_errno, strerror(h_errno));
    int fd = 0;
    int fd2 = 0;
    int retval = 0;
    ssize_t writeval = 0;
    ssize_t readval = 1;
    char buf[3500 * BUFFSIZE];
    bzero(buf, sizeof(buf));

    fd = netopen("./filesrv/6M.txt", O_RDWR | O_CREAT);
    fd2 = netopen("./filesrv/copy_6M.txt", O_RDWR | O_CREAT);

    while(readval > 0){
        debug("in read loop");
        readval = netread(fd, buf, sizeof(buf));
        writeval = netwrite(fd2, buf, readval);
    }

    retval = netclose(fd);
    retval = netclose(fd2);

    
    
    debug("main: exiting");
}
