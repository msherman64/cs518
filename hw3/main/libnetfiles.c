#include <stdlib.h> //for exit
#include <stdio.h> //for fprintf
#include <netinet/in.h> //for sockaddr_in
#include <netdb.h> //for getyhostbyname
#include <sys/socket.h> //for AF_INET
#include <string.h> //for memcpy
#include <errno.h> //for errno, strerrno
#include <unistd.h> //for read
#include <pthread.h> //for pthread create, join

#include "libnetfiles.h"
#include "dbg.h"
#include "common.h" //definitions for client and server

//indicate if server found
struct hostent *hostinfo;

struct client_fd all_fd[MAX_DISK_FD];

void init_sockaddr(struct sockaddr_in *name, uint16_t port)
{
    name->sin_family = AF_INET;
    name->sin_port = htons(port);
    check(hostinfo != NULL, "Unknown host");
    //copy address from hostent struct to sockaddr_in struct on the heap
    memcpy( &(name->sin_addr.s_addr), hostinfo->h_addr_list[0], hostinfo->h_length);
    debug("socket addr %u", name->sin_addr.s_addr);
}


int connect_socket(struct sockaddr_in *servername, uint16_t port)
{

    int sock = socket (AF_INET, SOCK_STREAM, 0);
    check((0 <= sock), "socket (client)");
    /* Connect to the server. */
    debug("make_socket: calling init_sockaddr, servername at %p", servername);
    init_sockaddr(servername, port);
    check(0 <= connect(sock,(struct sockaddr *) servername, 
                sizeof (*servername)),
         "make_socket, connection failed" );

    return sock;
}

int netserverinit(char * hostname){
    /* ensure hostname exists
     * return -1 on error, 0 on success
     * set h_errno to HOST_NOT_FOUND on error
     */
    hostinfo = gethostbyname(hostname);
    if(hostinfo == NULL){
        h_errno = HOST_NOT_FOUND;
        return -1;
    } else {
        return 0;
    }
}

int get_sock_fd(int port){
    //check for init
    if(hostinfo == NULL){
        h_errno = HOST_NOT_FOUND;
        return -1;
    }
    //open connection
    struct sockaddr_in *foo = malloc(sizeof(struct sockaddr_in));
    int sock_fd = -1;
    sock_fd = connect_socket(foo, port);
    return sock_fd;
}

int netopen(const char *fname, int flag){
    /* connect socket
     * send opr_filename
     * wait for opr_ret
     * close socket
     */
    int sock_fd = get_sock_fd(SERVERPORT);
    if(-1 == sock_fd){
        return -1;
    }

    //prepare control message
    struct opr_fname message;
    bzero(&message, sizeof(message)); //zero struct
    strcpy(message.fname, fname); //set file name
    message.operation = NET_OPEN; //set operation type
    message.length = sizeof(message); //set message length
    message.flag = flag;
    debug("netopen: flag is %d", flag);


    //send control message
    int retval = 0;
    retval = reliable_write(sock_fd, &message, sizeof(message));
    debug("netopen: write bytes %d", retval); //return val of write

    //receive response
    char metabuff[1024];

    retval = read_opr(sock_fd, metabuff, sizeof(metabuff));
    if (retval == NET_OPEN_RET){
        struct opr_ret *opr_return = NULL;
        opr_return = (struct opr_ret *)metabuff;
        errno = opr_return->error_value;
        debug("netopen: received errno %d %s", errno, 
                strerror(errno));
        close(sock_fd);
        int serv_fd = opr_return->value;
        //check for already open, imply exclusive access
        if(all_fd[serv_fd].numopen > 0){
            errno = EACCES;
            return -1;
        } else { //case where was not open already
            all_fd[serv_fd].numopen = 1;
            return opr_return->value;
        }
    } else { //case where we receive wrong response
        close(sock_fd);
        return -1;
    }
}

int netclose(uint16_t file_fd){
    /* closes file descriptor of disk file on server
     * returns 0 on success, -1 on failure, and sets errno
     * connect socket
     * send opr_fd
     * wait for opr_ret
     * close socket
     */

    //open connection
    int sock_fd = get_sock_fd(SERVERPORT);
    if(-1 == sock_fd){
        return -1;
    }

    //prepare control message
    struct opr_fd message;
    bzero(&message, sizeof(message)); //zero struct
    message.length = sizeof(message); //set message length
    message.operation = NET_CLOSE; //set operation type
    message.fd = file_fd;
    debug("netclose: fd is %d", message.fd);


    //send control message
    int retval = 0;
    retval = reliable_write(sock_fd, &message, sizeof(message));
    debug("netclose: write bytes %d", retval); //return val of write

    //receive response
    char metabuff[1024];

    retval = read_opr(sock_fd, metabuff, sizeof(metabuff));
    if (retval == NET_CLOSE_RET){
        struct opr_ret *opr_return = NULL;
        opr_return = (struct opr_ret *)metabuff;
        errno = opr_return->error_value;
        debug("netopen: received errno %d %s", errno, 
                strerror(errno));
        close(sock_fd);
        if(opr_return->value == 0){ //case where successfully closed
            all_fd[file_fd].offset = 0;
            all_fd[file_fd].numopen = 0;
        }
        return opr_return->value;
    }
    close(sock_fd);
    return -1;
    //TODO close socket FD
}

ssize_t netread(int fildes, void *buf, size_t nbyte){
    /* reads nbytes from server fd files, into buffer buf
     * to do so, it will:
     * opens connection, and send control message requesting ports
     * receive control message, extract ports
     * send read requests of size BUFFSIZE on each port until done
     * return bytes read, or error
     */
    //ensure we don't overrun ssize_t (sic)
    size_t maxsize = -1; //setting to maximum value of size_t
    if(nbyte > (maxsize/2)){
        debug("netread: request %zu too large", nbyte);
        errno = EINVAL;
        return -1;
    }

    //compute number of blocks
    int num_blocks = (nbyte / BUFFSIZE);
    if(nbyte % BUFFSIZE){
        num_blocks += 1;
    }
    off_t start_offset = all_fd[fildes].offset; //get starting offset from FD
    debug("netread: TOKEN requesting %d blocks of size %d, for read of size %zu offset %zu",
            num_blocks, BUFFSIZE, nbyte, start_offset);

    //open connection for control message
    int sock_fd = get_sock_fd(SERVERPORT);
    if(-1 == sock_fd){
        //TODO set errno?
        return -1;
    }

    //prepare control message, to request list of valid ports
    struct opr_port_list message;
    bzero(&message, sizeof(message)); //zero struct
    message.operation = NET_PORT_LIST; //set operation type
    message.length = sizeof(message); //set message length
    //prepare port request array
    int i = 0; //if num_blocks >= maxthreads, will set all to 1
    for(i = 0; i < MAXTHREADS; i++){
        message.ports[i] = 0;
    }

    //send control message
    ssize_t meta_bytes_sent = 0;
    meta_bytes_sent = reliable_write(sock_fd, &message, sizeof(message));
    debug("netread: requested port list from server, wrote %zd bytes", 
            meta_bytes_sent);

    //receive response
    char metabuff[METABUFFSIZE];
    //ssize_t target = 0;
    int opr_type = read_opr(sock_fd, metabuff, sizeof(metabuff));
    struct opr_port_list *opr_return = NULL;
    if (opr_type == NET_PORT_LIST_RET){
        opr_return = (struct opr_port_list *)metabuff;
    } else {
        //TODO handle opr_ret, use errno
        debug("netread: got back incorrect reponse type %d", opr_type);
        close(sock_fd);
        return -1;
    }

    debug("netread: requested server to read %zu bytes from fd %d offset %ld",
            nbyte, fildes, start_offset);


    /* get list of ports to use */
    uint16_t ports_to_use[MAXTHREADS] = {0};
    int num_available_ports = 0;
    for(i = 0; i < MAXTHREADS; i++){
        if (opr_return->ports[i] >= PORTSTART){
            ports_to_use[num_available_ports] = opr_return->ports[i];
            debug("netread: available port is %d, message port %d", 
                    ports_to_use[num_available_ports], opr_return->ports[i]);
            num_available_ports += 1;
        }
    }

    /* spawn at most number of blocks, threads */
    if(num_blocks < num_available_ports){
        num_available_ports = num_blocks;
    }

    /* spawn one thread for each requested block */

    ssize_t bytes_read = 0;
    ssize_t port_bytes_read;
    int j = 0;
    //loop over all blocks
    for(i = 0; i < num_blocks; i += num_available_ports){
        //loop over paralel ports
        debug("netread: entered outer loop, blocks %d to %d", 
                i, i + num_available_ports);
        
        pthread_t readthread[num_available_ports];
        struct net_port_arg read_port_arg[num_available_ports];

        /* if fewer remaining loops than ports, only do needed loops */
        int inner_loop_cap = num_blocks - i; //remaining loops
        if(inner_loop_cap > num_available_ports){ 
            inner_loop_cap = num_available_ports;
        }


        for(j = 0; j < inner_loop_cap; j++){ 
            debug("netread: entered inner loop, block %d", i+j);
            //prepare input struct
            read_port_arg[j].req_info.req_off = start_offset + ((i+j) * BUFFSIZE); //offset is block position
            read_port_arg[j].req_info.req_size = BUFFSIZE;    //requested size is block size
            read_port_arg[j].req_info.disk_fd = fildes;
            read_port_arg[j].port = ports_to_use[(i+j) % num_available_ports]; 
            read_port_arg[j].buff = buf + ((i+j) * BUFFSIZE); //offset into buffer as well
            
            debug("netread: requesting block %d, offset %d, on port %d",
                i+j, read_port_arg[j].req_info.req_off, read_port_arg[j].port);
            //launch thread to read
            pthread_create(&readthread[j], NULL, net_port_read, (void *)&read_port_arg[j]);
        }

        //join and read results
        for(j = 0; j < inner_loop_cap; j++){ 
            ssize_t *result_ptr = NULL;
            pthread_join(readthread[j], (void **)&result_ptr);
            //get result 
            port_bytes_read = *result_ptr;
            if(port_bytes_read == -1){
                debug("netread: read block %d from port %d failed, errno %d %s",
                        i+j, read_port_arg[j].port, errno, strerror(errno));
                return -1;
            } else {
                if(port_bytes_read == 0){
                    debug("netread: 0 bytes read, EOF");
                    //TODO exit early on EOF
                }
                bytes_read += port_bytes_read;
                //TODO check port_bytes_read < BUFFSIZE
                debug("netread: read %zd bytes for block %d", port_bytes_read, i+j);
            }
            free(result_ptr);
        }
    }

    all_fd[fildes].offset = start_offset + bytes_read;
    debug("netread: received %zd bytes, errno %d, %s",
            bytes_read, errno, strerror(errno));
    close(sock_fd);
    return bytes_read;
}

void * net_port_read(void * arg){

    check_mem(arg);
    //this is my return value
    ssize_t *bytes_processed = malloc(sizeof(ssize_t));
    *bytes_processed = 0;

    //this is all the info I need, fd, offset, req_size, port #, buf ptr
    struct net_port_arg *my_arg = (struct net_port_arg *)arg;
    check_mem(my_arg->buff);

    //create the socket (or fail if not);
    int sock_fd = get_sock_fd(my_arg->port); // Connect to server on this port
    if(0 > sock_fd){
        warn("net_port_read: connect to port %d failed",my_arg->port);
        *bytes_processed = -1;
        return bytes_processed;
    }
    debug("net_port_read: got sock_fd %d for port %d",
        sock_fd, my_arg->port);

    //send the request
    struct opr_port read_req = my_arg->req_info;
    debug("net_port_read: requesting disk fd %d, offset %d, size %d",
            read_req.disk_fd, read_req.req_off, read_req.req_size);

    read_req.operation = NET_PORT_READ; //Tell server read request
    read_req.length = sizeof(read_req); //For read_opr
    ssize_t meta_bytes_sent = reliable_write(
                sock_fd, &read_req, sizeof(read_req));
    if(0 > meta_bytes_sent){
        warn("net_port_read: failed to send message on sock_fd %d",sock_fd);
        *bytes_processed = -1;
        return bytes_processed;
    }
    debug("net_port_read: Sent %ld meta_bytes", meta_bytes_sent);

    //get response opr_ret back
    char metabuff[METABUFFSIZE];
    int opr_type = read_opr(sock_fd, metabuff, METABUFFSIZE); //bzero in here
    if(opr_type != NET_READ_RET){
        warn("net_port_read: Recieved wrong respone type from server %d",
            opr_type);
        *bytes_processed = -1;
        return bytes_processed;
    }
    struct opr_ret *opr_received = (struct opr_ret *) metabuff;
    debug("net_port_read: received opr_ret with length %u, value %lu, error %u",
           opr_received->length, opr_received->value, opr_received->error_value); 

    //read the opr_received->value bytes back into my_arg->buff
    *bytes_processed = reliable_read(
                sock_fd, my_arg->buff, opr_received->value);
    debug("net_port_read: Sucessfully read %zd bytes", *bytes_processed);
    close(sock_fd); //close the socket, we're done
    return bytes_processed;
}

ssize_t netwrite(int fildes, void *buf, size_t nbyte){

    //ensure we don't overrun ssize_t (sic)
    size_t maxsize = -1; //setting to maximum value of size_t
    if(nbyte > (maxsize/2)){
        debug("netwrite: request %zu too large", nbyte);
        errno = EINVAL;
        return -1;
    }
    
    //compute number of blocks
    int num_blocks = (nbyte / BUFFSIZE);
    if(nbyte % BUFFSIZE){
        num_blocks += 1;
    }
    off_t start_offset = all_fd[fildes].offset; //get starting offset from FD
    debug("netwrite: writing %d blocks of size %d, for write of size %zu offset %zd",
            num_blocks, BUFFSIZE, nbyte, start_offset);

    //open connection for control message
    int sock_fd = get_sock_fd(SERVERPORT);
    if(-1 == sock_fd){
        return -1;
    }

    //prepare control message, to request list of valid ports
    struct opr_port_list message;
    bzero(&message, sizeof(message)); //zero struct
    message.operation = NET_PORT_LIST; //set operation type
    message.length = sizeof(message); //set message length
    //prepare port request array
    int i = 0; //if num_blocks >= maxthreads, will set all to 1
    for(i = 0; i < MAXTHREADS; i++){
        message.ports[i] = 0;
    }

    //send control message
    ssize_t meta_bytes_sent = 0;
    meta_bytes_sent = reliable_write(sock_fd, &message, sizeof(message));
    debug("netwrite: requested port list from server, wrote %zd bytes", 
            meta_bytes_sent);

    //receive response
    char metabuff[METABUFFSIZE];
    int opr_type = read_opr(sock_fd, metabuff, sizeof(metabuff));
    struct opr_port_list *opr_return = NULL;
    if (opr_type == NET_PORT_LIST_RET){
        opr_return = (struct opr_port_list *)metabuff;
    } else {
        //TODO handle opr_ret, use errno
        debug("netwrite: got back incorrect reponse type %d", opr_type);
        close(sock_fd);
        return -1;
    }

    /* get list of ports to use */
    uint16_t ports_to_use[MAXTHREADS] = {0};
    int num_available_ports = 0;
    for(i = 0; i < MAXTHREADS; i++){
        if (opr_return->ports[i] >= PORTSTART){
            ports_to_use[num_available_ports] = opr_return->ports[i];
            debug("netwrite: available port is %d, message port %d", 
                    ports_to_use[num_available_ports], opr_return->ports[i]);
            num_available_ports += 1;
        }
    }
    debug("netwrite: requested server to write %zu bytes from fd %d offset %ld",
            nbyte, fildes, all_fd[fildes].offset);

    /* spawn one thread for each requested block */
    ssize_t bytes_written = 0;
    ssize_t port_bytes_written;
    int j = 0;
    //loop over all blocks
    for(i = 0; i < num_blocks; i += num_available_ports){
        //loop over paralel ports
        
        pthread_t writethread[num_available_ports];
        struct net_port_arg write_port_arg[num_available_ports];
        
        /* if fewer remaining loops than ports, only do needed loops */
        int inner_loop_cap = num_blocks - i; //remaining loops
        if(inner_loop_cap > num_available_ports){ 
            inner_loop_cap = num_available_ports;
        }

        for(j = 0; j < inner_loop_cap; j++){ 
            //prepare input struct
            write_port_arg[j].req_info.req_off = start_offset + (i+j) * BUFFSIZE; //offset is block position
            write_port_arg[j].req_info.req_size = BUFFSIZE;    //requested size is block size
            write_port_arg[j].req_info.disk_fd = fildes;
            write_port_arg[j].port = ports_to_use[(i+j) % num_available_ports]; 
            write_port_arg[j].buff = buf + ((i+j) * BUFFSIZE); //offset into buffer as well
            
            debug("netwrite: writing block %d, offset %d, on port %d",
                i+j, write_port_arg[j].req_info.req_off, write_port_arg[j].port);
            //launch thread to write
            pthread_create(&writethread[j], NULL, net_port_write, (void *)&write_port_arg[j]);
        }

        //join and read results
        for(j = 0; j < inner_loop_cap; j++){ 
            ssize_t *result_ptr = NULL;
            pthread_join(writethread[j], (void **)&result_ptr);
            //get result 
            port_bytes_written = *result_ptr;
            if(port_bytes_written == -1){
                debug("netwrite: write block %d from port %d failed, errno %d %s",
                        i+j, write_port_arg[j].port, errno, strerror(errno));
                return -1;
            } else {
                if(port_bytes_written == 0){
                    debug("netwrite: 0 bytes write, EOF");
                    //TODO exit early on EOF
                }
                bytes_written += port_bytes_written;
                //TODO check port_bytes_written < BUFFSIZE
                debug("main: write %zd bytes for block %d", port_bytes_written, i+j);
            }
            free(result_ptr);
        }
    }

    all_fd[fildes].offset = start_offset + bytes_written;
    debug("netwrite: wrote %zd bytes, errno %d, %s",
            bytes_written, errno, strerror(errno));
    close(sock_fd);
    return bytes_written;
}

void * net_port_write(void * arg){

    check_mem(arg);
    //this is my return value
    ssize_t *bytes_processed = malloc(sizeof(ssize_t));
    *bytes_processed = 0;

    //this is all the info I need, fd, offset, req_size, port #, buf ptr
    struct net_port_arg *my_arg = (struct net_port_arg *)arg;
    check_mem(my_arg->buff);

    //create the socket (or fail if not);
    int sock_fd = get_sock_fd(my_arg->port); // Connect to server on this port
    if(0 > sock_fd){
        warn("net_port_write: connect to port %d failed",my_arg->port);
        *bytes_processed = -1;
        return bytes_processed;
    }
    debug("net_port_write: got sock_fd %d for port %d",
        sock_fd, my_arg->port);

    //send the request
    struct opr_port write_req = my_arg->req_info;
    debug("net_port_write: requesting disk fd %d, offset %d, size %d",
            write_req.disk_fd, write_req.req_off, write_req.req_size);

    write_req.operation = NET_PORT_WRITE; //Tell server write request
    write_req.length = sizeof(write_req); //For read_opr
    ssize_t meta_bytes_sent = reliable_write(
                sock_fd, &write_req, sizeof(write_req));
    if(0 > meta_bytes_sent){
        warn("net_port_write: failed to send message on sock_fd %d",sock_fd);
        *bytes_processed = -1;
        return bytes_processed;
    }
    debug("net_port_write: Sent %ld meta_bytes", meta_bytes_sent);


    //Send the respective bytes to the server
    ssize_t write_bytes = reliable_write(
                sock_fd, my_arg->buff, write_req.req_size);
    debug("net_port_write: Sucessfully wrote %zd bytes into sock_fd",
            write_bytes);
    /* There is a potential for the stream to get out of sync
       if we send more than BUFFSIZE. It's not a
       big deal because this is a one time use socket, so anything
       past BUFFSIZE will be discarded on the server side and then 
       this connection will close.
    */

    //get response opr_ret back
    char metabuff[METABUFFSIZE];
    int opr_type = read_opr(sock_fd, metabuff, METABUFFSIZE); //bzero in here
    if(opr_type != NET_WRITE_RET){
        warn("net_port_write: Recieved wrong respone type from server %d",
            opr_type);
        *bytes_processed = -1;
        return bytes_processed;
    }
    struct opr_ret *opr_received = (struct opr_ret *) metabuff;
    debug("net_port_write: received opr_ret with length %u, value %lu, error %u",
           opr_received->length, opr_received->value, opr_received->error_value); 
    *bytes_processed = opr_received->value;

    close(sock_fd); //close the socket, we're done
    return bytes_processed;
}
