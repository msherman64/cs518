/* A simple server in the internet domain using TCP
 *    The port number is passed as an argument */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

#include "common.h"

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

int main(int argc, char *argv[])
{
     int sockfd, newsockfd, portno, n, stat;
     socklen_t clilen;
     char buffer[256];
     struct sockaddr_in serv_addr, cli_addr;
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) 
        error("ERROR opening socket");
     //set linger option for socket·                                            
     struct linger linger = { 0 }; //null out struct                            
     linger.l_onoff = 1; //enable option                                        
     linger.l_linger = 0; //set linger time to 0                                
     stat = setsockopt(sockfd,                                                  
         SOL_SOCKET, SO_LINGER,  //socket api, linger option                    
         (const char *) &linger,                                                
         sizeof(linger));
     if (stat < 0) 
        error("ERROR setting opts");
     bzero((char *) &serv_addr, sizeof(serv_addr));
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(PORT);
     if (bind(sockfd, (struct sockaddr *) &serv_addr,
              sizeof(serv_addr)) < 0) 
              error("ERROR on binding");
     listen(sockfd,5);
     clilen = sizeof(cli_addr);
     newsockfd = accept(sockfd, 
                 (struct sockaddr *) &cli_addr, 
                 &clilen);
     if (newsockfd < 0) 
          error("ERROR on accept");
     bzero(buffer,256);

     n = read(newsockfd,buffer,255);
     if (n < 0) error("ERROR reading from socket");
     printf("Here is the message: %s\n",buffer);
     int i = 0;
     for(i =0; i < 100; i++){
	     n = write(newsockfd,"I got your message",18);
	     printf("Wrote %d bytes\n",n);
	     if (n < 0) error("ERROR writing to socket");
     }
     
     close(newsockfd);
     close(sockfd);
     return 0; 
}
