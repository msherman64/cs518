Mutiport server for large copies
================================

We've decided to do part B. The task is to multiplex connections for transfers
larger than 2k. We've heavily modified the logic, and created separate 
threads that do the actual work of read and write. A logical flow of the
read operation is diagrammed in figure :ref:`LargeNetRead`. The sequence diagram 
shows the message exchange of a Large net read. Solid arrows represent 
network traffic, dashed arrows represent function argument passing.


.. _LargeNetRead:

.. figure:: images/LargeNetReadFileSequence.png

   Large Net Read


The write operation follows a similar logic but the checks and order of 
operations is different. All calls to netread / netwrite follow this logic. 
Each thread only accepts a conection up to BUFFSIZE (2K bytes). If a read/write
call is smaller than 2K, it simply uses a single port (essentially returning 
us to the previous version). 

Usage
-----

Client
^^^^^^
The client side usage is the same, the user must call netopen to get a file
descriptor which corresponds to a disk file descriptor on the server side. This
file descriptor can be passed to subsequent calls to netread/netwrite. The
netread/netwrite call will make a best effort to retrieve the number of bytes
requested and then return the number of bytes successfully processed or -1 
on failure. The client is responsible for handle netread/netwrite failures and
closing the file descriptor (with netclose).

Server
^^^^^^

The server starts up on port SERVERPORT and enters an accept loop. Every time
a new connection comes, a thread is spawned to handle the resulting socket 
file descriptor. The main thread now handles requests for open, close, and 
available port list. 

When the server comes up, it spawn worker threads which listen on PORTSTART + 
thread id (TID). TID can range from 0 to MAXTHREADS (10 in our case). There is
a one to one correspondance between threads and ports. Each of the worker 
threads is assigned a port and services only that port. 

These threads do a blocking call to accept (they handle only one connection 
at a time), and then service the requests. Each connection can ask for upto 
BUFFSIZE bytes at a offset within the file which is pointed to by the 
supplied file descriptor.  When a request is received, the thread duplicates 
the file descriptor and then seeks the file descriptor to the requested 
position and makes a best effort attempt to send those bytes to the client.

In the normal case or in the event of an error, the server will send 
an opr_ret indicating any errornos and the number of bytes sucessfully written.

Server side
-----------

Read Message
^^^^^^^^^^^^

Read message has been updated to only dispatch for the operations open, close,
and get port list. The client side will handle read/writes as a two phase 
operation:

 #. Get available ports list
 #. Send read/write request to some of those ports

Each port can handle up to BUFFSIZE requests, but takes an offset for where
in the file it wants to the get the bytes from. Thus the client can assemble
any size read/write as a independent sequence of see/read operations.

Read Write thread
^^^^^^^^^^^^^^^^^

This function is the logic for each read/write thread. The prototype matches 
what the pthread lib expects. It takes an int as an argument which it 
uses to index into a global array of struct port_stat objects. It uses the 
port_stat to identify it's starting port number.  The thread will try to bind 
to this number but if it fails, the thread will bump the port number up by 1. 
It will do this upto PORTSEP number of times, after which point it will fail. 
When a port is successfully bound to, the thread updates port_stat.port. 

The port_stat also contains a status flag that is used to indicate if the
thread is currently servicing a request or ready for connection. This value 
is set to 0 immediately before the blocking call to accept. It's set to one
when accept unblocks. These values will be checked by the  
server_net_port_list when assembling the list to be report to the clients.

Once the thread receives a connection from the client, the thread reads 
the resulting socket file descriptor to get the required parameters from
the client. The client use provide a disk file descriptor, an offset and the
number of bytes to be returned. The thread will duplicate the file descriptor
and then seek the file descriptor to the requested position. If the 
duplication or seek fails, the server will return an error message.

If those operations succeed, the operation type is checked to determine what
kind of operation is requested. If it is a read, the 
min(BUFFSIZE, requested size) bytes is read in to a buffer. The amount of
bytes read from the disk may be less that the amount requested. Once the
read is done, the amount of bytes read is put into a response message. This is
sent to the client so the client knows how many bytes to expect from the 
socket. Once the message is sent the server copies the buffer into 
the socket.

If the case of a write, the server reads the socket for a message
that tells it how many bytes are expected to be in the write operation. It 
copies min(BUFFSIZE, requested size) into a buffer. If either of these socket
reads fails, the client simply closes the file descriptor (it was unable to
get a message and thus can't use the socket to respond). If the write succeeds, 
the client respond with a message that indicates the number of bytes written.

This thread has no return value and should only terminate with the server.

server_net_port_list
^^^^^^^^^^^^^^^^^^^^

This function is run when the read_message function gets a message with 
operation NET_PORT_LIST. This operation sends a opr_port_list  as
part of the message request and the message response. When this function
is invoked, it scans the port status array and writes the port number 
into the array if the status is available. This message is sent back 
to the client. The client will then use these ports to preform any read/write
operations it needs.

Safe Open
^^^^^^^^^
Safe open is a wrapper around open. It maintains struct for each open disk
file descriptor, containing the filename it maps to. This is used later, for
get_dup_fd.

Safe close
^^^^^^^^^^
Safe close is a wrapper around close. It maintains the same struct as 
safe_open, but clears the filename to indicate that it is unused.

dup_fd
^^^^^^
The built in function "dup()" returns a new fd number, but these refer to the 
same kernel data structure, sharing offset, mode, etc.

For this reason, we implement our own function, get_dup_fd.
It takes a disk file descriptor "old_fd", and looks it up in the global
server_disk_fd struct. If it has a filename attached, it calls 
safe_open(filename). This maintains the struct, and returns a file descriptor
new_fd, referring to the same file as old_fd. However, these two file 
descriptors can have separate offsets, allowing concurrent disk reads.


server_net_open
^^^^^^^^^^^^^^^
Server_net_open calls safe_open instead of open. This maintains our file 
descriptor structure.

server_net_close
^^^^^^^^^^^^^^^^
Server_net_close calls safe_close instead of close. This maintains our file 
descriptor structure.

server_net_read
^^^^^^^^^^^^^^^

Depreciated

server_net_write
^^^^^^^^^^^^^^^^

Depreciated

Client Side
------------

netopen
^^^^^^^
We are now tracking the offset of a given disk_fd, on the client side.
Netopen now checks a struct array "all_fd[disk_fd]". If the all_fd entry has
a numopen greater than 0, netopen returns -1. This prevents multiple access to
the same file descriptor.

netclose
^^^^^^^
We are now tracking the offset of a given disk_fd, on the client side.
Netclose now checks a struct array "all_fd[disk_fd]", and sets the offset and 
number of open accesses to 0, for the disk_fd passed to it.

netread
^^^^^^^
Netread no longer requests a read directly. Instead, it requests a list of
valid ports from the server. It then, in increments of 2kB, spawns threads
to read from each port returned, in a round robin fashion.

Each thread exectues net_port_read, and writes directly into the buffer that
was passed into netread. Each thread returns the amount of bytes successfully
written.

It then updates the all_fd[disk_fd] entry with the number of bytes successfully
read.


netwrite
^^^^^^^^
Netwrite no longer requests a write directly. Instead, it requests a list of
valid ports from the server. It then, in increments of 2kB, spawns threads
to write to each port returned, in a round robin fashion.

Each thread exectues net_port_write, and reads directly from the buffer that
was passed into netwrite. Each thread returns the amount of bytes successfully
written.

It then updates the all_fd[disk_fd] entry with the number of bytes successfully
writen.


net port read
^^^^^^^^^^^^^

The net_port_read function is the client side analogue of the 
read_write_thread. It is meant to be spawned as a thread and takes
an struct as an arguement that includes the target port number, 
the disk file descriptor, the requsted offset, the requested size, and a 
buffer to place the read bytes. 

After some basic data checks, this function calls the supplied port and
writes a NET_PORT_WRITE message into the resulting socket file descriptor. It
then reads the socket file descriptor to get the return message which
will indicate how many bytes were succesfully read from the supplied
disk file descriptor. It will try to read that many bytes from the socket 
file descriptor and returns the resulting value as a pointer to an int. 

net port write
^^^^^^^^^^^^^^

net_port_write is similar to net_port_read in almost all logic except for 
the order/direction of the read and write operation. It also expect to write
the data immediately after sending the NET_PORT_WRITE message. When 
the server sends back a confirmation message about the size of bytes
actually written to disk, this value will be reported to the calling
context.

New Structures
--------------

file descriptors
^^^^^^^^^^^^^^^^
The client and server now each maintain their own local struct for 
meta-information about disk file descriptors.

The client uses all_fd to track concurrent access to a given disk file
descriptor, as well as the current offset. This enables all messages sent to
the server to be idempotent, specifying a given offset directly, instead of
implcitly.

The server uses an array of type server_disk_fd, to associate a filename with
each disk_fd in use. This mapping is done in safe_open. This enables us to get
non-interfering file descriptors for paralel read/write operations.

opr_port_list
^^^^^^^^^^^^^
opr_port_list is a struct containing the operation type, message length, and an
array of uint16_t. This array has one entry for each server port thread,
defined in MAXTHREADS. The client prepares this struct, and fills the array 
with zeros. 

The server, upon receiving the opr_port_list, fills each array position with an
available listen port, before sending it back to the client. Any unused array
positions are set to 0.

Upon receipt, the client passes these port numbers to net_port_read or 
net_port_write, as needed.

opr_port
^^^^^^^^
opr_port is the type that sent to initiate a read or write from the 
read_write_thread. It contains the three major pieces of info required to
service that request::
 
    struct opr_port{
        uint8_t operation;
        uint16_t length;
        int req_off;
        int req_size;
        int disk_fd;
    };

Those are the offset, the size, and the number of the disk file descriptor 
to read or write to/from.

port_stat
^^^^^^^^^

Port stat is simply a method of tracking the status of the thread maintaining
a port number::

    struct port_stat{
        uint16_t port;
        int status;
    };

If the value of the status member is 1, the thread is currently servicing a
request. If the status is 0, the thread is ready to answer a call on the
indicated port.


net_port_arg
^^^^^^^^^^^^

The net_port_arg is a super structure that is passed into net_port_read or 
net_port_write. It contains two additonal pieces of information::

    struct net_port_arg{
        struct opr_port req_info; //What you want the operation to do
        int port; //The port you want to call on
        void *buff; //The buffer you want to act on
    };

specfically the port number to call on and the buffer to read/write to/from.
