Building the Remote FS
======================

Part 1 of this project was to remotely execute the low level, open,
close, read and write system calls on a server machine. The client side
would call net\* versions of these calls, and have the messages transported
to the server where they would be acted on. The server has to respond
with meta information about the success or failure of this operation.

For read and write, the bytes that are passed to the read/write function need
to be sent over the network to the server where they will be stored within
a file that resides on the disk. 


data structures / messages
--------------------------
We define a set of values and structures that are common to both server and 
client. 

First, we define the port that they server binds to, the server's read
buffer size, as follows.::

    #define SERVERPORT 8765
    #define BUFFSIZE 1024

The port number tell the client where to connect, while the buffer size allows 
us to avoid overrunning the serve buffer accidental.

We set a series of #defines to specify each of the function and message types.
These function types are used to disambiguate the messages being sent between 
the client and server. They are as follows ::

    //client command types
    #define NET_OPEN  1
    #define NET_CLOSE 2
    #define NET_READ  3
    #define NET_WRITE 4

    //server response types
    #define NET_OPEN_RET  11
    #define NET_CLOSE_RET 12
    #define NET_READ_RET  13
    #define NET_WRITE_RET 14

To pass meta information back and fourth between the server and the client, 
we defined a protocol / message structure that housed the relevant data 
elements for each operation. These were as follows. 

Struct opr is a type and length header. The length member is  used to 
determine how many additional bytes to read from the socket and the type is 
used to determine what type to cast it to::

    //struct to signal which operation is requested
    struct opr{
        uint8_t operation; //one of NETOPEN, NETCLOSE, NETREAD, NETWRITE
        uint16_t length; // sizeof(opr) == 24
    }; 

Struct opr_ret is used for return values from any of the syscall as they all
have the same return semantics.::

    struct opr_ret{
        uint8_t operation;
        uint16_t length; //sizeof(opr_ret) == 96
        /* retval has multiple uses
         * for open, returns FD or -1 on error
         * for close, return 0, or -1 on error
         * for read, returns number of bytes following this message = num read
         * for write, returns number of bytes written
         */ 
        uint64_t value;
        uint8_t error_value;
    }; 

Struct opr_fname is used to transmit the filename to be opened. It is assumed
that the full file (including path) name cannot be longer than 512 bytes.::

    struct opr_fname{
        uint8_t operation; //one of NETOPEN, NETCLOSE, NETREAD, NETWRITE
        uint16_t length; //sizeof(opr_fname) == 529
        uint8_t flag; //one of O_RDONLY, O_WRONLY, O_RDWR, bitwise or with create
        char fname[512];
        //TODO flags
    }; 

Struct opr_size is used in both read and write operations. It conveys a file 
descriptor which corresponds to the disk file descriptor that the server 
holds open. The size member is the number of bytes that the requested operation
want to read/write.::

    struct opr_size{
        uint8_t operation; //one of NETOPEN, NETCLOSE, NETREAD, NETWRITE
        uint16_t length; //sizeof(opr_size) == 48
        uint16_t fd;
        size_t nbyte; //size of read or write
        //struct is followed by nbyte raw bytes
    };

Struct opr_fd is used for the close operation. It has a single additional 
member which is the number of the fd to be closed::

    struct opr_fd{
        uint8_t operation; //one of NETOPEN, NETCLOSE, NETREAD, NETWRITE
        uint16_t length; //sizeof(opr_fd) == 40
        uint16_t fd;
    };

Client
------


The client side implelents the net\* functions. This client code is presented
as a libaray that can be included in test code. This test code can make use
of the net\* versions of the I/O system calls. 

Connection setup
^^^^^^^^^^^^^^^^
Connection to the server from the client proceeds in multiple stages.

netserverinit
+++++++++++++

User code must call this function with a valid hostname for the server. It 
uses gethostbyname() to check for validity, and populate a global hostinfo 
struct with the resulting address. The prototype is::

    int netserverinit(char * hostname);

It returns 0 on success, and -1 on failure, as well as populating h_errno with
HOST_NOT_FOUND. All other connection and net\* calls will not proceed without 
a valid host, instead returning -1 immediately.

get socket file descriptor
++++++++++++++++++++++++++

Each of the four net\* functions first calls::

    int get_sock_fd();

This checks that netserverinit has been called, and returns immediately if not.
It mallocs a sockaddr_in struct, and then calls connect_socket to populate it. 
This struct will store all the information needed for connect.

Get socket returns either a valid socket file descriptor, or -1, as returned by
connect socket. This means that each net\* call operates on a different socket
file descriptor, and will not receive each other's messages. This file 
descriptor is closed just before the net\* functions return.

connect socket
++++++++++++++

Connect socket has the prototype::

    int connect_socket(struct sockaddr_in *servername, uint16_t port);

This calls init_sockaddr to set up the sockaddr_in struct. It also creates the
socket integer. After initializing the socket, it attempts to connect to the
socket. If successfull, the socket file descriptor is returned.


initialize socket address
+++++++++++++++++++++++++
This function is called by connect_socket, to set up the struct. As the return
type of gethostbyname() is incompatible with  connect() or bind(), we must do
manual memcpys to populate the appropriate struct.
Specfically, it checks for the validity of the global hostinfo struct, and then
copies hostinfo->h_addr_list[0] to sockaddr_in->sin_addr.s_addr.
The field h_addr_list[0] is the first IP address listed for the hostname.
The field s_addr is what calls to connect or bind can use.

The function prototype is::
    init_sockaddr (struct sockaddr_in *name, uint16_t port);


open
^^^^

The protoype for the  netopen function is::

    int netopen(const char *pathname, int flags);

The netopen function take a string that identifies the file to be opend, 
and a integer that is used to designate any flags required for the open call.
The supported flags are O_RDONLY, O_WRONLY, or O_RDWR and O_CREAT. O_CREAT is
specfically used to create files on the server. If O_CREAT is specified, the 
file on the server will be created with 777 permissions (subject to umask).

This function takes the provided flags, and path, and packages them into an
opr_fname struct. It then sends the opr_fname struct to the server via a 
write operation on the socket file descriptor which was setup in the 
connection phase.

The open call expects to reieve an opr_ret from the server which will indicate
the sucess or failure of the call. It will pass the function retrun value back
as its return value. It also sets the errno global to the value of the errono
reported by the server side. 

close
^^^^^

The protoype for netclose is::

    int netclose(uint16_t fd);

It's operation is alomost identical to netopen with the exception of sending
an opr_fd.

read
^^^^

The prototype for netread is::

    ssize_t netread(int fildes, void *buf, size_t nbyte);

netread preforms a similar command setup and send operation to open. After
the command is sent, netread expects the server to send back an opr_ret. The
value of the opr_ret is the number of bytes that was read from the disk and 
thus should be aviable for reading from the socket. If the value is -1 the 
read operation failed, and netread will return a -1. It will also set
the value of errno to the value of errno reported by the server in the opr_ret.

If the opr_ret value is > -1, netread will read the report number of bytes
into the provided buffer from the socket. If the socket operation fails, 
the errno is set and the number of bytes sucessfully retrieved is returned.

write
^^^^^

The prototype for netwrite is::

    ssize_t netwrite(int fildes, void *buf, size_t nbyte);

netwrite preforms the same command setup as all the other functions. After
the command is sent, it immedately follows the command with the data to be 
written. It expects the server side to return an opr_ret with the sucess
or failure of the disk write operation. It will then return the bytes 
written to the server disk as it return value and set the errno if needed.

Server
------

read message
^^^^^^^^^^^^

The read_message function acts as a dispatch function. It calls the read_opr
function and then determines the operation based on the value returned (which
was read from the operation member of the opr struct). For a given operation
it will cast the allocated buffer to the correct struct type and then 
pass a pointer to that struct to one of the server analogue functions
for handling the commands. Each function will also the value of the
socket file descriptor from which the command came. This will be used for
data as well as returning value structs. Read message is meant to 
be called as a thread and this has a prototype::

    void * read_message(void * arg)

The arg is expected to be a pointer to an int which represents the socket
file descriptor that was generated from the accept function. read_message 
is responsible for closing the socket file descriptor once the dispatched
function returns.

open
^^^^

The protoype for server_net_open is::

    void server_net_open(struct opr_fname *message, int sock_fd);

This function extracts the filename and flags from the message that was
sent. It then uses those values to call the open on the servers file system. 
It then takes the file descriptor, which is simply an int and sends it to the 
client via the send_response function. If the disk operation failed, the errno 
will be set and the return value of pen will be -1. In this case, both those 
values are captured and sent back to the client. The client can then
immediately return those values to the user code.

close
^^^^^

The prototype for server_net_close function is::

    void server_net_close(struct opr_fd *message, int sock_fd);

This function is the logical opposite of open, and operates in almost exactly 
the same way.

read
^^^^

The prototype for server_net_read function is::

    void server_net_read(struct opr_size *message, int sock_fd)

The read function first tries to read the request number of bytes from the 
file. It tracks how many bytes were successfully read and any errnos that 
may have occurred. It then sends a response with that information.

We send the response first to indicate to the client how many bytes to read 
out of the socket, and we will only send as many bytes as we successfully read
from the file on disk. Once the response is sent, we then write bytes that
were in the buffer to the socket file descriptor.

If the disk read operation failed the opr_ret that is sent will indicate
-1 bytes read and have an errno value. The client side will not attempt to 
read from the socket. Instead it will set errno and return -1.

If the disk read succeeds but the socket write fails, the file descriptor will
be rewound to the last known successfully sent byte. On the client side, the 
return value of the netread operation will be indicate the number of bytes
successfully received.  The user code has to  handle the case where the 
amount of bytes received is less that the requested amount. 



write
^^^^^

The prototype for server_net_write function is::

    void server_net_write(struct opr_size *message, int sock_fd)

The write function works like the opposite end of the read function on the 
client side. It tries to read the target size of bytes from the 
socket file descriptor and then write those bytes to disk. It then sends back
a response to the client which indicates the number of bytes successfully
processed.

If either operation fails, the client will receive a response indicating 
failure and an errno which corresponds to the type of failure (disk or socket).

Conveninence Functions
----------------------

Relible read / write
^^^^^^^^^^^^^^^^^^^^

The reliable versions of the read and write function wrap the read / write 
functions into while loops that track how much progress the write/read 
operation has made. This progress is indicated by the return value. If the
return of the write/read is less that the requested amount, the function
increments the pointers upto the number of successfully processed bytes and
then calls read/write again. The prototypes for these functions are::

    ssize_t reliable_read(int sock_fd, void *buff, size_t size);
    ssize_t reliable_write(int sock_fd, void *buff, size_t size);

Send Response
^^^^^^^^^^^^^

Since each server side net\* operation has to package up the return value
and send it back to the client, this function wraps up the process of create
the opr_ret data structure and sending it back. The prototype is::

   ssize_t send_response(int sock_fd, int operation, uint64_t value, uint8_t err_value);



Read opr
^^^^^^^
The procedure for getting the operation of the socket is also common to both
client and server. The operation is to first read sizeof(opr) from the socket 
file descriptor into a buffer large enough to hold the largest possible struct.
It then casts the buffer to a (opr \*) and uses the struct member structure 
to read the respective member fields. It the uses the length field to 
determine how much additional data to read from the socket into the buffer.
If that read succeededs, it returns the value of the operation member. The
protoype for read_opr is::

    int read_opr(int sock_fd, char *buff, size_t buffsize);

Synchronization and TCP
-----------------------

We make the assumption that each read/write operation can act on at most 
BUFFSIZE bytes. If the netread/netwrite functions are called with a requested
size larger than the BUFFSIZE the operation will be truncated to BUFFSIZE, and
the number of bytes that successfully processed will be returned. ALl 
read/write operations can process at most min(BUFFSIZE, requested size) bytes
per session. Each call to netread/netwrite will open a new socket session. 

To process more than BUFFSIZE bytes, usercode has to call the netread/netwrite
functions multiple times.

Known Bugs
----------

Newlines appear on some writes, this may be related to reading EOF from files.


