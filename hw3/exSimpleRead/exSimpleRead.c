
//MAKE this
#define ARG_FAILURE   -100
#define SEEK_FAILURE  -101
#define READ_FAILURE  -102
#define WRITE_FAILURE -103
#define NET_PORT_READ  2000 //Pick correct number
#define NET_PORT_WRITE  2001 //Pick correct number
#define METABUFFSIZE 1024
#define MAXTHREADS 10
#define PORTSTART 8023

//MAKE this
struct opr_port{
    int opr_type;
    int length;
    int req_off;
    int req_size;
    int disk_fd;
}


//Port status struct, theres should be one per Thread
struct port_stat{
    int port;
    int status;
}


struct port_stat ports[MAXTHREADS] = {0};  // global port status array


//Initalize the port status array
int i = 0;
for(i =0; i < MAXTHREADS; i++){
    ports[i].port = PORTSTART + i;
    ports[i].status = 1; // Will get set to zero right before the accept
}

//MAKE this
ssize_t send_response(int sock_fd, int operation, uint64_t value, uint8_t err_value);

//Read Write thread
void * read_write_thread(void *arg){
    /* This thread gets created on startup and handles read requests (one
        at a time) for ever. It's arg is an INT that is the port number
        it will bind to. It retuns nothing. 
    */

    //Entry invariant
    check_mem(arg);

    //socket setupvars
    struct sockaddr_in cli_addr = { 0 };
    socklen_t clilen = sizeof(cli_addr);

    int my_port_index  = *(int *)arg; // worker thread arugment
    struct port_stat *my_port_stat = &ports[my_port_index];
    int sock_fd = 0; // Holds the current sock_fd
    int disk_fd = 0; // Holds the current disk_fd
    int opr_type = 0; // Type read from socket
    char metabuff[METABUFF]; //Where the meta oper goes
    char databuff[BUFFSIZE];//Where the data to respond will go
    ssize_t disk_read_bytes = 0;
    ssize_t bytes_sent = 0;
    off_t new_offset;
    struct opr_port *message = NULL; //Should point to buff when we read

    //Bind to port number my_arg.port
    int listen_fd = make_socket(my_port_stat->port);
    check( 0 < listen_fd, "Failed to create socket for port %d",my_port_stat->port);
    debug("read_write_thread: Create listen_socket %d",listen_fd);

    while (1){
        //Ready for connection
        my_port_stat->status = 0; // status 0 inidicates ready for connection
        sock_fd = accept(listen_fd,  //Block until connection
                        (struct sockaddr *) &cli_addr, 
                        &clilen);
        my_port_stat->status = 1; // status 1 inidicates working
        if(0 >= sock_fd){
            debug("main: Failed to accept on socket");
            continue; //Go back to waiting
        }


        /*
           Act on socket request
        */

        //get operation and check type
        messsage = NULL; // Just to be neat I null this before this call to prevent previous loop iterations from carrying over
        opr_type = read_opr(sock_fd, metabuff, sizeof(metabuff));
        if((NET_PORT_READ != opr_type) || (NET_PORT_WRITE != opr_type)){
            //This port only accept read/write requests 
            send_response(sock_fd, ARG_FAILURE, -1, EINVAL);
            close(sock_fd);
            continue; // Skip this iteration (saves on allocations) and block accept again
        }
        /*
          Operations common to both read and write
        */

        //Format message
        message = (struc opr_port *) metabuff;

        //seek fd to correct position
        errno = 0;
        disk_fd = get_dup_fd(message->disk_fd);
        if(-1 == disk_fd){
            send_response(sock_fd, ARG_FAILURE, -1, errno);
            close(sock_fd);
            continue; // Skip this iteration and block accept again
        }
        new_offset = lseek(disk_fd, message->req_off, SEEK_SET);
        if( 0 > new_offset){ //Seek Failure
            send_response(sock_fd, SEEK_FAILURE, -1, errno);
            close(sock_fd);
            safe_close(disk_fd);
            debug("read_write_thread: Failed to seek to %d", message->req_off);
            continue;
        }

        /*
            Work on read or write
        */


        if(NET_PORT_READ == opr_type){ // READ CASE
            //read disk
            bzero(databuff,BUFFSIZE);
            disk_read_bytes = read(disk_fd, databuff, message->req_size);
            if( 0 > disk_read_bytes){ //Read Failure
                send_response(sock_fd, READ_FAILURE, -1, errno);
                close(sock_fd);
                safe_close(disk_fd);
                debug("read_write_thread: Failed to read to %d from disk",
                        message->req_size);
                continue;
            }
            debug("read_write_thread: read disk %d", disk_read_bytes);
            send_response( sock_fd, NET_READ_RET, disk_read_bytes,errno);

            /*
            make one attempt on write, if fails the clinet will know and it's 
            the clients job (and ultimatley the users job) to seek again and 
            retry
            */

            //write socket
            bytes_sent = write(sock_fd, temp_buf, disk_read_bytes);
            debug("read_write_thread: sent %zd bytes", bytes_sent);

        } else if (NET_PORT_WRITE == opr_type){ //WRITE CASE
            //read socket
            bzero(databuff,BUFFSIZE);
            net_read_bytes = read(sock_fd, databuff, message->req_size);
            if( 0 > net_read_bytes){ //Read Failure
                close(sock_fd); //Can't report any thing cuz socket read failed
                safe_close(disk_fd);
                debug("read_write_thread: Failed to read %d from socket", 
                    message->req_size);
                continue;
            }
            debug("read_write_thread: read from socket %d bytes", net_read_bytes);

            /*
            This may be -1, but if it is we can't do any thing about it 
            besidies report it (which we're doing any way)
            */

            //write disk
            disk_write_bytes = write(disk_fd, temp_buf, net_read_bytes);
            debug("read_write_thread:  wrote %zd bytes to disk", disk_write_bytes);
            send_response(sock_fd, NET_WRITE_RET, disk_write_bytes,errno);

        } else { //Should not get here
            send_response(sock_fd, ARG_FAILURE, -1, errno);
            debug("read_write_thread: got a bad arg %d",opr_type);
        }

        close(sock_fd);
        safe_close(disk_fd); //Done with this FD 
    }

    //Should never get here.
    close(listen_fd);
    return NULL; 
}

