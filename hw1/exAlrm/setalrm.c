#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/time.h>
#include <string.h>
#include "setalrm.h"


void con() { 
    printf("starting I'm a constructor\n"); 
    do_sleep(500000); 
    printf("finishing I'm a constructor\n"); 
} 


void des() {
    printf("starting leaving!\n");
   // remove timer here
   do_sleep(0);
    printf("finishing leaving!\n");
}

void handle_sigalrm(int signal) {
    if(signal != SIGALRM){
       printf("got wrong signal %d\n", signal);
    }
    printf("Got sigalrm signal %d\n", signal);

}

void do_sleep( int ms) {
   struct itimerval timer;
   // set timer for ms milliseconds
   timer.it_value.tv_sec=0;
   timer.it_value.tv_usec = ms;
   printf("Sleeping for %d uS\n",ms);
   // disable recurrance
   timer.it_interval.tv_sec = 0;
   //timer.it_interval.tv_usec = 0;
   timer.it_interval.tv_usec = ms;

   struct sigaction sa;
   memset(&sa, 0, sizeof sa);
   sa.sa_handler = &handle_sigalrm;
   sigaction(SIGALRM, &sa, NULL);

//   sigset_t mask;
   //sa.sa_flags = SA_RESETHAND;
//   sigfillset(&sa.sa_mask);
//   sigprocmask(0,NULL, &mask);
//   sigdelset(&mask, SIGALRM);

   setitimer(ITIMER_REAL, &timer, NULL);
//   sigsuspend(&mask);
//   while(1){};
//   printf("sigsuspend() returned \n");
}
