#ifndef _SETALRM_H_
#define _SETALRM_H_

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/time.h>

static void con() __attribute__((constructor));
static void des() __attribute__((destructor));

void con();

void des();

void handle_sigalrm(int signal);

void do_sleep(int ms);

#endif
