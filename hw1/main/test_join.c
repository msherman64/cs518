#include <stdio.h>
#include <stdlib.h>
#include "my_pthread.h"
//#include <pthread.h>

#define numthreads 8192
// 7000 works, 8000 doesn't return to main reliably
// speed issue, some caching?
pthread_t thread_block[numthreads];

void * f1 (void * func_arg)
{
    pthread_t foo = *(pthread_t *) func_arg;

    pthread_join(foo,NULL); //join previous thread

    return NULL;
}

void * f2 (void * func_arg)
{

    printf("initial thread started\n"); 
    printf("initial thread finished wait\n"); 
    

    return NULL;
}


int main(int argc, char *argv[]) 
{
   
   
    pthread_create(&thread_block[0],NULL,*f2,NULL);

    int i=1;
    for( ;i<numthreads;i++){
        pthread_create(&thread_block[i],NULL,*f1,&thread_block[i-1]);
        printf("main: created thread id %p\n",thread_block[i]);
    }

    printf("main: joining thread id %p\n",thread_block[i-1]);
    pthread_join(thread_block[i-1],NULL);

    printf("main: finished all joins\n");
    return 0;
};
