#include <stdio.h>
#include <stdlib.h>
#include "my_pthread.h"
//#include <pthread.h>

pthread_t threads[100]; 
int ids[100];


void * short_func (void * func_arg)
{
    int id = *(int *) func_arg;

    printf("short_func: started %d\n",id);
    for( int i=0; i<5; i++){
    }
    printf("short_func: ended %d\n",id);
    return NULL;
}

void * long_func (void * func_arg)
{
    int id = *(int *) func_arg;

    printf("long_func: started %d\n",id);
    for( int i=0; i<5; i++){
        waitFor(1);
    }
    printf("long_func: ended %d\n",id);
    return NULL;
}


int main(int argc, char *argv[]) {

    int i = 1;
    for(i = 1; i < 20; i++){
        ids[i] = i;
        pthread_create(&threads[i],NULL,short_func,&ids[i]);
    }
    for(;i < 30; i++){
        ids[i] = i;
        pthread_create(&threads[i],NULL,long_func,&ids[i]);
    }
    for(; i < 50; i++){
        ids[i] = i;
        pthread_create(&threads[i],NULL,short_func,&ids[i]);
    }

    for(; i < 70; i++){
        ids[i] = i;
        pthread_create(&threads[i],NULL,short_func,&ids[i]);
    }

    for(; i < 100; i++){
        ids[i] = i;
        pthread_create(&threads[i],NULL,short_func,&ids[i]);
    }

    printf("main: made threads\n");
    for (int j = 1; j < 100; j++){
        pthread_join(threads[j],NULL);
    }
    printf("main: finished\n");

    return 0;

};
