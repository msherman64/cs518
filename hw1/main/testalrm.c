#include <stdio.h>
#include <stdlib.h>
#include "my_pthread.h"


void * f1 (void * func_arg)
{
    debug("f1: started\n");
    for( int i=0; i<5; i++){
        waitFor(1);
        debug("f1: count is %d\n",i);
    }
    int *ptr = malloc(sizeof(int));
    *ptr = 12;

//    my_pthread_exit((void *)ptr);
    return ptr;
}

void * f1j (void * func_arg)
{
    debug("f1j: start join\n");
    my_pthread_t temp_thread;

    int tid = *(int*) func_arg;
    temp_thread.thread_id = tid;
   
    void *ret = NULL; 
    my_pthread_join(temp_thread, &ret);

    debug("f1j: came back from join, returned %d\n",  *(int*)ret);
    free(ret);
    return NULL;

}

/*
void * f2 (void * func_arg)
{
    debug("f2: started\n");
    int *returnval = malloc(sizeof(int));
    *returnval=88;
    my_pthread_exit(returnval);
    debug("f2: should never get here\n");
    return NULL;
}
*/

void * f3 (void * func_arg)
{
    debug("f3: arg is %d\n", *(int*) func_arg);

    return NULL;
}

int main(int argc, char *argv[]) {

    my_pthread_t thread1;
    my_pthread_create(&thread1,NULL, f1,NULL);
    int foo = 3;
    my_pthread_t thread3;
    my_pthread_create(&thread3, NULL, f3, &foo); 
    int join_id = 1;

    my_pthread_t thread2;
    my_pthread_create(&thread2,NULL,f1j,&join_id);
    info("main: made thread \n");
    debug("main: waiting \n");
//    my_pthread_join(thread1,NULL);
    waitFor(10);    
    debug("main: came back from join\n");
    info("main: finished \n");

    return 0;

};
