#include <stdio.h>
#include <stdlib.h>
#include "my_pthread.h"
//#include <pthread.h>

#define numthreads 8192 //number of threads to spawn
pthread_t threads[numthreads];
long counter = 0; //init counter
long loops = 10000; //loops per thread
int lock = 1; //if 1, enables mutex

void * f1 (void * func_arg)
{
    for(int i=0; i<loops; i++){
        if(lock){
            pthread_mutex_lock(func_arg);
        }

        counter++;

        if(lock){
            pthread_mutex_unlock(func_arg);
        }
    }

    return NULL;
}

int main(int argc, char *argv[]) 
{
    printf("main: max threads is %d\n", numthreads);
    pthread_mutex_t mutex1;
    pthread_mutex_init(&mutex1,NULL);



    printf("main: final value should be %ld\n",loops*numthreads);
    for(int i=0; i<numthreads; i++){
        pthread_create(&threads[i],NULL, *f1, &mutex1);
    } 
    for(int j=0; j<numthreads; j++){
        pthread_join(threads[j],NULL);
    }

    pthread_mutex_destroy(&mutex1);

    printf("main: current value is: %ld\n",counter);

    return 0;

};
