#ifndef _MY_PTHREAD_H_
#define _MY_PTHREAD_H_

#include <sys/queue.h>      //TAILQ_*
#include <ucontext.h>       // ucontext_t

/* debug macros*/

//should this be write instead of printf?
#ifndef NDEBUG
#define debug(M, ...)
#else
#define debug(M, ...) printf("DEBUG %s:%d: " M "\n",\
        __FILE__, __LINE__, ##__VA_ARGS__)
#endif

#define warn(M, ...) printf("WARN %s:%d: " M "\n",\
        __FILE__, __LINE__, ##__VA_ARGS__)

#define info(M, ...) printf("INFO %s:%d: " M "\n",\
        __FILE__, __LINE__, ##__VA_ARGS__)

//A is what we want to assert is true.
#define check(A, M, ...) if(!(A)) {\
    debug(M, ##__VA_ARGS__); setcontext(&error_context); }

//If pointer is null, then !pointer is True
#define check_mem(A) check((A), "Pointer was null")

//Constructor for creating error context
static void con() __attribute__((constructor)); 
void con(void);
void error(void);


/* Internal constants and structures */
#define EDEADLK -2        // deadlock
#define EINVAL -3         // thread is not joinable or already joined by another
#define ESRCH -4          // could not find thread

#define STACKSIZE 8192    //default stack size
#define SCHED_QUANTA 50   //How long a thread is scheduled for
#define MAXQUANTA 1000    //Longest a thread can be scheduled for
#define MAXTHREADS 8192   // Limit on the number of threads you can schedule
#define PRIORITYLEVELS 10 //Number of queues in the calendar queueu
#define MAX_PRI_TICKS 5   // numer of ticks before we bump priority

#define pthread_t my_pthread_t
typedef struct _my_pthread_t
{
    int thread_id;        //Numeric thread identifer (just counts up)
    int priority;         // level at whcich we are reinserted into ready_queue
    int ticks;            // counter for number of ticks at current priority
    int finished;         // Tells us when to no longer schedule this thread
    struct _my_pthread_t *joined_by;   //who is joining this thread
    struct _my_pthread_t *waiting_for; //thread this is wating for
    ucontext_t context;   //Uses for calls to get/set context

    //Part of the callers memory that we will store the retrun value in
    void *func_arg;
    void *retval;

    //Next and Prev Pointers for ready or waiting queue
    TAILQ_ENTRY(_my_pthread_t) tailq_struct; 
} my_pthread_t;          //Type of the elements in the TAILQ

typedef TAILQ_HEAD(tailhead, _my_pthread_t) head_t;    //Type of the TAILQ head

// A list entry for our mutex list. Contains a pointer to a thread
typedef struct _mutex_list_t
{
    my_pthread_t *thread_p; //pointer to thread locking mutex
    TAILQ_ENTRY(_mutex_list_t) m_tailq_struct; //List structure
} mutex_list_t; //type of element in the queue

//Head for a mutex list (as opposed to a pthread list)
typedef TAILQ_HEAD(m_tailhead, _mutex_list_t) m_head_t;

//this is passed to mutex functions by reference as arg.
#define pthread_mutex_t my_pthread_mutex_t
typedef struct _my_pthread_mutex_t
{
    m_head_t *lock_q; //pointer to fifo queue of locking threads
} my_pthread_mutex_t; //type of element in the queue

/*
    We're never going to take one of these it's just here to prevent complier
    complaints 
*/
typedef struct {
} my_pthread_attr_t; 


/* exposed API */
#define pthread_yield my_pthread_yield
void my_pthread_yield();
#define pthread_exit my_pthread_yield
void my_pthread_exit(void *value_ptr);
#define pthread_join my_pthread_join
int my_pthread_join(my_pthread_t thread, void **value_ptr);
#define pthread_create my_pthread_create
int my_pthread_create( my_pthread_t *pt,
                       my_pthread_attr_t *attr,
                       void *(*function)(void*),
                       void * arg);

/* mutex API */

//attributes are ignored
#define pthread_mutex_init my_pthread_mutex_init
int my_pthread_mutex_init(my_pthread_mutex_t *mutex,
                          const pthread_mutexattr_t *mutexattr);

#define pthread_mutex_lock my_pthread_mutex_lock
int my_pthread_mutex_lock(my_pthread_mutex_t *mutex);
#define pthread_mutex_unlock my_pthread_mutex_unlock
int my_pthread_mutex_unlock(my_pthread_mutex_t *mutex);

//mutex should be unlocked before destroy
#define pthread_mutex_destroy my_pthread_mutex_destroy
int my_pthread_mutex_destroy(my_pthread_mutex_t *mutex);

/* Support functions*/
void init_timer(int ms);
void start_timer (int ms);
void run_thread(void *(*function)(void *));
void handle_sigalrm(int signal);

/* Convience functions*/
void waitFor (unsigned int secs);
void block_sigalrm();
void unblock_sigalrm();
int ready_insert(my_pthread_t *ins, int pri);
my_pthread_t * get_next(void);

#endif
