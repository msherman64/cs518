#include <stdio.h>                 //printf
#include <stdlib.h>                //malloc
#include <signal.h>                //sigaction
#include <sys/time.h>              // setitimer
#include <sys/queue.h>             //TAILQ_*
#include <string.h>                //memset
#include <ucontext.h>              // *context()
#include <time.h>                  //for time() function
#include "my_pthread.h"            //Our library
#include "valgrind.h"              //valgrind hooks for debugging

int cur_id = 0;                    // last thread id created
int cur_pri = 0;                   // Current priority que we are working on

my_pthread_t *cur_thread = NULL;   //pointer to active thread
head_t wait_head;                  // queue head
head_t ready_head[PRIORITYLEVELS]; // ready queue head
my_pthread_t * all_threads[MAXTHREADS] = {NULL}; // all threads

ucontext_t signal_context;          /* the interrupt context */
void *signal_stack;                 /* global interrupt stack */

ucontext_t error_context;           /* the error context context */

int was_init = 0;                   //flag to tell if we've run setup or not
sigset_t signal_set;                // signal set for blocking sigalrm

/* 
    setup signal handler, queue allocate memory for the current thread pointer
*/
void setup() {
    debug("setup: start function");

    struct sigaction sa; 
    //setting fuction pointer callback
    sa.sa_handler = handle_sigalrm; 

    //restart interrupted system calls (deffer till after protected regions)
    sa.sa_flags = SA_RESTART; 

    //block all signals during handler except sigkill and sigstop
    sigfillset(&sa.sa_mask);

    //register the mask and call back
    check(sigaction(SIGVTALRM, &sa, NULL) == 0, "Sigaction set failed");

    //Allocate the first stack
    cur_thread = calloc(sizeof(my_pthread_t),1);
    check_mem(cur_thread);
    debug("setup: malloced cur_thread %p",cur_thread);
    cur_thread->thread_id=0;                       //This is mains my_pthread_t
    cur_thread->priority=1;                  //Every ones initial prioirty is 1
    cur_thread->thread_id=0;
    signal_stack = calloc(STACKSIZE,1);
    check_mem(signal_stack);
    debug("setup: malloced signal_stack %p",signal_stack);

    // Initilaize all the queues
    TAILQ_INIT(&wait_head);
        // Can't check return value on these for some reason (macro badness)

    check(cur_id == 0,"Setup called call after threads were made?");
    all_threads[cur_id] = cur_thread; 
        // cur_id should be zero when setup is called

    for(int i = 0; i < PRIORITYLEVELS; i++){
        TAILQ_INIT(&ready_head[i]); 
        debug("setup: initialized pq %d",i);
    }
    check(sigfillset(&signal_set) == 0,
            "setup: failed to empty sig_set"); 
    debug("setup: end function");
}

/*
    This schedules an alarm to fire ms milliseconds form the termnation
    of this function. It's used to schedule a single alarm in the futre.
    We do not want a "regular fireing clock" instead we want a off set
    that gives us back control ms seconds after we hand it off
*/

void start_timer (int ms)
{
    check (ms < MAXQUANTA, "start_timer: quanta too large");

    //sets up timer struct values. We only need the usec field 
    struct itimerval timer;

    // set timer for ms milliseconds
    timer.it_value.tv_sec=0;
    timer.it_value.tv_usec = ms*1000;       //must be nonzero for timer to fire
    timer.it_interval.tv_sec = 0;
    timer.it_interval.tv_usec = 0;

    //this starts the timer
    check (setitimer(ITIMER_VIRTUAL, &timer, NULL) == 0, "timer set failed"); 
}

/*
    creates a thread. Allocates it's stack memory and sets all of it's 
    my_pthread_t paramters to starting defaults. This function retuns
    after create so the uc_link context should not be returned to.

    thead is added to the run que, and also tracked in the all_thread que.
    returns nonzero on failure.
*/

int my_pthread_create( my_pthread_t *pt, my_pthread_attr_t *attr, void *(*function)(void*), void *arg)
{
    debug("my_pthread_create: start function");

    //Check for null pointers in the mandatory args
    check_mem(pt);
    check_mem(function);

    if(!was_init){                          //Call setup if it was never called
        setup();
        was_init=1;
        start_timer(SCHED_QUANTA);
    }

    cur_id++;
    if (cur_id > MAXTHREADS){
        debug("make_pthread: can't make more threads");
        return -1;
    }

    // setting defaults in pt struct
    debug("my_pthread_create: begin Making thread %d",cur_id);
    all_threads[cur_id] = pt; //Index into the all threads array with thread id
    pt->thread_id = cur_id;          // make cur_id and thread_counter the same

    pt->priority=1;
    pt->finished=0;
    pt->ticks=0;
    pt->waiting_for=NULL;
    pt->joined_by=NULL;
    pt->func_arg=arg;

    //Create a context for the new thread
    void *stack=calloc(STACKSIZE,1);
    check_mem(stack);
    debug("my_pthread_create: malloced %p",stack);

    ucontext_t *uc = &pt->context;
    getcontext(uc);
    uc->uc_stack.ss_sp = stack;
    uc->uc_stack.ss_size = STACKSIZE;
    uc->uc_stack.ss_flags = 0;
    sigemptyset(&uc->uc_sigmask);
    int ret = VALGRIND_STACK_REGISTER(stack, stack + STACKSIZE);
    check(ret == 0, "my_pthread_create: VALGRIND_STACK_REGISTER Failed");
    makecontext(uc, (void (*)(void))run_thread, 1, function);


    //schedule with highest prioirty
    ready_insert(pt, 1);

    debug("my_pthread_create: finished, made thread id=%d",pt->thread_id);
    debug("my_pthread_create: end function");
    return 0;
}

/*
    All yield does is call SIGALARM, the sighandler / scheduler will then take
    over and schedule what ever is next in the queue
*/
void my_pthread_yield()
{
    raise(SIGVTALRM);
}

/*
    Exit saves the value that will be pathched into join and then sets the
    finsihed flag for this thread pointer
*/
void my_pthread_exit(void *value_ptr)
{
    debug("my_pthread_exit: start function for thread %d",
            cur_thread->thread_id);
    cur_thread->retval = value_ptr;                               //May be null
    cur_thread->finished=1;

    if(cur_thread->joined_by != NULL){
       // block_sigalrm(); //start protected region

        //make joined thread runnable
        my_pthread_t *np = cur_thread->joined_by;      //who was waiting for me
        np->waiting_for = NULL;                             //No longer waiting
        TAILQ_REMOVE(&wait_head, np, tailq_struct); 
        ready_insert(np, 1);

        debug("my_pthread_exit: thread %d moved to ready queue",
            np->thread_id);

        //unblock_sigalrm(); //end protected region
    }

 

    debug("my_pthread_exit: end function for thead  %d",
            cur_thread->thread_id);
    my_pthread_yield();
}

/*
    Join function set the waiting_for and joined_by hints to scheduer and
    then yeilds. Join returns 0 when the joined thread completes or a 
    negative number on error. If value_ptr is specified, will be used to 
    retrieve joined threads retval
*/

int my_pthread_join(my_pthread_t search_thread, void **value_ptr)
{
    my_pthread_t *np = all_threads[search_thread.thread_id];

    // Error conditions
    if(np == NULL){
        debug("my_pthread_join: not found in search");
        return ESRCH;
    } else if(np->joined_by != NULL){
        debug("my_pthread_join: thread already joined");
        return EINVAL;
    /*
     np->waiting_for may be null. The first case is to prevent joining some
     a thread that is wating on the current thread (deadlock). The second
     case is to prevent self joins (also deadlock).
    */
    } else if((np->waiting_for == cur_thread) ||
              (np == cur_thread)){
        debug("my_pthread_join: deadlock");
        return EDEADLK;
    } else if(np->finished == 1){
        debug("my_pthread_join: already finished");
        if (value_ptr != NULL){
            *value_ptr = np->retval;
        }
        return 0;
    }

    debug("my_pthread_join: found thread %d",np->thread_id);

    //block_sigalrm();  // begin protected region
    // set scheduler hints for joining and joined threads 
    np->joined_by=cur_thread;
    cur_thread->waiting_for=np;
    //unblock_sigalrm();  // end protected region

    debug("my_pthread_join: not already done, set pointers");
    my_pthread_yield();

    // set value_pointer's value to the address of np's return value pointer. 
    if (value_ptr != NULL){
        *value_ptr = np->retval;
    }

    return 0;
}


int my_pthread_mutex_init(my_pthread_mutex_t *mutex,
                          const pthread_mutexattr_t *mutexattr)
                                                       //attributes are ignored
{
    check_mem(mutex);
    mutex->lock_q = calloc(sizeof(m_head_t),1);        //malloc list head pointer
    check_mem(mutex->lock_q);
    debug("my_pthread_mutex_init: malloced list head %p",mutex->lock_q);
    m_head_t *lockp = mutex->lock_q;
    TAILQ_INIT(lockp);

    return 0;
}
/* this function should attempt to lock a mutex by placing itself on the 
   mutex's queue. if successful, it returns immediately otherwise, it blocks 
   until lock is obtained. The front of the list owns the lock, every one
   else on the list should be wating (on the wait queue).
 */
int my_pthread_mutex_lock(my_pthread_mutex_t *mutex)
{
    //debug and protection
    check_mem(mutex);
    //create list item, add reference to current thread, and insert 
    m_head_t *lockp = mutex->lock_q;       //get convenient handle to list head
    mutex_list_t *mtp = calloc(sizeof(mutex_list_t),1);
    check_mem(mtp);
    mtp->thread_p = cur_thread;               //point list object to cur_thread

    //actual tail insertion for FIFO ordering
    TAILQ_INSERT_TAIL(lockp, mtp, m_tailq_struct); 
    debug("my_pthead_mutex_lock: added thread %d to mutex queue\n",
          cur_thread->thread_id);

    //when we are not at the head of queue, we want to move to the wait queue
    check_mem(lockp->tqh_first);    //should  be not null, verify just inserted
    my_pthread_t *front = lockp->tqh_first->thread_p; 

    //we are the only item on the queue, and so are at the front
    if(front == cur_thread){ 
        debug("my_pthread_mutex_lock: thead %d got lock immediately",
              cur_thread->thread_id);
        return 0;                               //don't need to wait, succeeded
    } else {                          //case where we are waiting, not at front
       /*
        We set waiting for and allow the scheduler to move this thread to the
        wait queue
       */
        debug("my_pthread_mutex_lock: thead %d waiting for lock",
              cur_thread->thread_id);
        cur_thread->waiting_for=front; 
        my_pthread_yield();                              //give up control asap
        return 0;                            //return only when we get the lock
    }
}

/*
  Unlock mutex by popping current thread off of queue, if current thread is 
   in front free list item if removal successful
*/
int my_pthread_mutex_unlock(my_pthread_mutex_t *mutex)
{
    check_mem(mutex);
    //setup error cases
    m_head_t *lockp = mutex->lock_q;                 //pointer to head of queue
    check_mem(lockp);                                 //check mutex initialized

    //pointer to first item of queue
    mutex_list_t *front = lockp->tqh_first;

    //Front is null if list is empty, trying to unlock empty mutex
    if(front == NULL){
        debug("my_pthread_mutex_unlock: thead %d tried to unlock initialized"
              ", but empty mutex", cur_thread->thread_id);
        return 0;
    }
    if(front->thread_p != cur_thread){
        debug("my_pthread_mutex_unlock: thread %d can't unlock, not in front",
               cur_thread->thread_id);
        return -1;
    }

    //normal case where we have the lock
    TAILQ_REMOVE(lockp, front, m_tailq_struct); 
    debug("my_pthread_mutex_unlock: ran remove\n"); 
    free(front); //free list item

    //pop next item off of wait queue, so it can be scheduled
    front = lockp->tqh_first;            //old front is freed, we can now reuse
    if(front == NULL){
        return 0;                  //no more items, don't need to schedule next
    } else {
        //reschedule the head of the mutex queue
        front->thread_p->waiting_for = NULL;                //No longer waiting
        TAILQ_REMOVE(&wait_head, front->thread_p, tailq_struct); 
        ready_insert(front->thread_p, 1);

        debug("my_pthread_mutex_unlock: thread %d moved to ready queue",
            front->thread_p->thread_id);
        return 0;
    }
}

int my_pthread_mutex_destroy(my_pthread_mutex_t *mutex)
//mutex should be unlocked before destroy
{
    debug("my_pthread_mutex_destroy: start function for thread  %d",
        cur_thread->thread_id);
    check_mem(mutex);

    //check if mutex is locked.
    m_head_t *lockp = mutex->lock_q;
    check_mem(lockp);
    mutex_list_t *front = lockp->tqh_first;
    if(front != NULL){
        debug("my_pthread_mutex_destroy: cannot destroy, mutex not unlocked");
        return -1;
    }
    free(lockp);
    debug("my_pthread_mutex_destroy: end function for  %d",
        cur_thread->thread_id);
    return 0;
}

/*
    Wrapper for the running thread. passes the return value to
    my_pthread exit.
*/

void run_thread(void *(*function)(void *))
{
    void * dummy_retval = function(cur_thread->func_arg);
    my_pthread_exit(dummy_retval); 
}

/*
    This is a calendar que sheduler. Every process starts  
    my_pthread_t->prioirty 1 when created. They will be rescheduled at that 
    priority for MAX_PRIORITY_TICKS after which their prioity will be bumped 
    up (Higher means scheduled further into the future). When
    my_pthread_t-> prioirty reaches PRIORITYLEVELS-1, it gets reset to zero.
    This giving the old slow process more cycles for a while to see if we can 
    get it to complete.
*/

void scheduler(void)
{
    check_mem(cur_thread);        // If this happens we lost a context somwhere
    debug("Scheduler: thread %d completed %d ticks at priority %d",
            cur_thread->thread_id, cur_thread->ticks, cur_thread->priority);

    if(cur_thread->ticks >= MAX_PRI_TICKS){
        cur_thread->ticks = 0;                             //roll over priority
        cur_thread->priority++;
    }
    else {
        cur_thread->ticks++;                   // move the tick counter forward
    }
    if (cur_thread->priority >= PRIORITYLEVELS){
       debug("scheduler: priority of thread %d reset from %d",
            cur_thread->thread_id, cur_thread->priority);
       cur_thread->priority = 1;                      //Been stale for too long
    }

    if(cur_thread->waiting_for!=NULL){

        // this thread join another, put on wait queue
        TAILQ_INSERT_TAIL(&wait_head, cur_thread, tailq_struct);
        debug("scheduler: thread  %d moved to wait queue",cur_thread->thread_id);
    } else if(cur_thread->finished==0){

        //Thread not finished
        ready_insert(cur_thread, cur_thread->priority);
        debug("scheduler: thread  %d  rescheduled",cur_thread->thread_id);
    }

    my_pthread_t *dummy = cur_thread;
    cur_thread=get_next();

    if(dummy->finished == 1){
        //Free the stack we malloced in create, the rest belongs to the user
        ucontext_t uc = dummy->context; 
        free(uc.uc_stack.ss_sp);
    }

    start_timer(SCHED_QUANTA);
    check(setcontext(&cur_thread->context) == 0,
        "scheduler: failed to switch contexts");
}

/* 
    The sigalrm handler is tasked with storing the current context,
    and preparing a new context for the scehduler, then swapping to it. 
*/
//void handle_sigalrm(int signal, siginfo_t *info, void *old_context) 
void handle_sigalrm(int signal) 
{
    debug("handle_sigalrm: start function for thread %d",
        cur_thread->thread_id);
    if(signal != SIGVTALRM){
        debug("handle_sigalarm: got wrong signal %d", signal);
        return;
    }
    start_timer(0);                                     //disables timer firing
    debug("Tick");

    /* Create new scheduler context */
    getcontext(&signal_context);

    //This is getting overwritten on each call
    signal_context.uc_stack.ss_sp = signal_stack; 
    signal_context.uc_stack.ss_size = STACKSIZE;
    int ret = VALGRIND_STACK_REGISTER(signal_stack, signal_stack + STACKSIZE);
    check(ret == 0, "handle_siglarm: VALGRIND_STACK_REGISTER Failed");
    signal_context.uc_stack.ss_flags = 0;
    sigemptyset(&signal_context.uc_sigmask);
    makecontext(&signal_context, scheduler, 0);

    /* save running thread, jump to scheduler */
    check(swapcontext(&cur_thread->context,&signal_context) == 0,
        "Stack overflow");
    debug("handle_sigalrm: end function for thread %d",
        cur_thread->thread_id);
}

/*
    my_pthread lib safe sleep that uses timers to sleep instead of sigalarm.
*/

void waitFor (unsigned int secs)
{
    debug("waitFor: started function");
    unsigned int retTime = time(0) + secs;                // Get finishing time
    while (time(0) < retTime){
    // Loop until it arrives.
    };
    debug("waitFor: ended function");
}

/*
  The call to sigprocmask sets the block for sig alarm process wide
  it does not matter what context this is called in.
*/

void block_sigalrm()
{
    check(sigprocmask(SIG_BLOCK, &signal_set, NULL) == 0,
            "unblock_sigalrm: failed to set signal block"); 
}

/*
    undoes the block
*/

void unblock_sigalrm()
{
    check(sigprocmask(SIG_UNBLOCK, &signal_set, NULL) == 0,
            "unblock_sigalrm: failed to unset signal block"); 
}

/*
   This constructor runs when the library is included. It ensures that the 
   error_context is populated before we ever try to jump to it.
*/

void con(void)
{
    getcontext(&error_context);
    error_context.uc_stack.ss_sp = calloc(STACKSIZE,1);
    debug("con: malloced error stack %p", error_context.uc_stack.ss_sp);
    error_context.uc_stack.ss_size = STACKSIZE;
    error_context.uc_stack.ss_flags = 0;
    sigemptyset(&error_context.uc_sigmask);
    makecontext(&error_context, (void (*)(void))error, 0);
}

/*
   For now the error contexts just exits. It could potentially do some form of
   clean up
*/
void error(void)
{
    exit(1);
}

/*
    Insert into calendar que, priority can be in range [1,PRIORITYLEVELS]
*/
int ready_insert(my_pthread_t *ins, int pri)
{
    //arg checks
    check_mem(ins); 
    check( 0 < pri && PRIORITYLEVELS > pri,
        "ready_insert: Prority index out of range :  %d < %d < %d", 
        1, pri, PRIORITYLEVELS)

    //relative offset with out mod

    int ind = cur_pri + pri;
    if (ind >= PRIORITYLEVELS){
        ind = ind - PRIORITYLEVELS;
    }

    TAILQ_INSERT_TAIL(&ready_head[ind], ins, tailq_struct);
    debug("ready_insert: inserted thread %d into que %d", ins->thread_id, ind);
    return 0;
}

/* 
    Gets the next item from the calendar que. Moves cur_pri forward if needed
*/

my_pthread_t * get_next(void)
{
    //find next non-empty queue
    int around = 0;
    my_pthread_t *np = ready_head[cur_pri].tqh_first;
    while (np == NULL && around < PRIORITYLEVELS){
        cur_pri = cur_pri + 1;
        if (cur_pri >= PRIORITYLEVELS){                          //fast modulus
            cur_pri = PRIORITYLEVELS - cur_pri;
        }
        around ++;                             //Don't go around more than once
        np = ready_head[cur_pri].tqh_first;
    }
    check(np != NULL, "All ready queues empty");

    TAILQ_REMOVE(&ready_head[cur_pri], ready_head[cur_pri].tqh_first,
                  tailq_struct);
    debug("get_next: exited at cur_pri %d",cur_pri);
    return np;
}
