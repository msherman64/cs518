\select@language {english}
\contentsline {chapter}{\numberline {1}Homework 1}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Introduction}{2}{section.1.1}
\contentsline {chapter}{\numberline {2}Design}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}scheduler}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}get\_next}{3}{section.2.2}
\contentsline {section}{\numberline {2.3}ready\_insert}{3}{section.2.3}
\contentsline {section}{\numberline {2.4}my\_pthread\_t}{3}{section.2.4}
\contentsline {section}{\numberline {2.5}my\_pthread\_create}{4}{section.2.5}
\contentsline {section}{\numberline {2.6}setup}{4}{section.2.6}
\contentsline {section}{\numberline {2.7}run\_thread}{4}{section.2.7}
\contentsline {section}{\numberline {2.8}my\_pthread\_exit}{4}{section.2.8}
\contentsline {section}{\numberline {2.9}my\_pthread\_yield}{5}{section.2.9}
\contentsline {section}{\numberline {2.10}my\_pthread\_join}{5}{section.2.10}
\contentsline {section}{\numberline {2.11}mutex\_functions}{5}{section.2.11}
\contentsline {subsection}{\numberline {2.11.1}init}{5}{subsection.2.11.1}
\contentsline {subsection}{\numberline {2.11.2}lock}{6}{subsection.2.11.2}
\contentsline {subsection}{\numberline {2.11.3}unlock}{6}{subsection.2.11.3}
\contentsline {subsection}{\numberline {2.11.4}destroy}{6}{subsection.2.11.4}
\contentsline {section}{\numberline {2.12}start\_timer}{6}{section.2.12}
\contentsline {section}{\numberline {2.13}waitFor}{6}{section.2.13}
\contentsline {chapter}{\numberline {3}The Calendar Queue}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}List of queues}{7}{section.3.1}
\contentsline {section}{\numberline {3.2}Prioirty and ticks}{8}{section.3.2}
\contentsline {section}{\numberline {3.3}soft premption}{9}{section.3.3}
\contentsline {section}{\numberline {3.4}Tuneing parameters}{9}{section.3.4}
\contentsline {chapter}{\numberline {4}Background infromation on the tools we used}{10}{chapter.4}
\contentsline {section}{\numberline {4.1}ucontext}{10}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}ucontext\_t struct}{10}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}making a context}{10}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}ucontext references}{11}{subsection.4.1.3}
\contentsline {section}{\numberline {4.2}Signal Handling and Timer}{11}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}signal handler references}{11}{subsection.4.2.1}
\contentsline {section}{\numberline {4.3}sys/Queue.h}{11}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Types}{11}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Insertion}{12}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Removal}{12}{subsection.4.3.3}
\contentsline {section}{\numberline {4.4}OS References}{12}{section.4.4}
\contentsline {section}{\numberline {4.5}Other References}{12}{section.4.5}
\contentsline {chapter}{\numberline {5}FLow of execution of a thread}{13}{chapter.5}
\contentsline {section}{\numberline {5.1}Interaction between run and create}{13}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}my\_pthread\_create}{13}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}run\_thread}{13}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}handle\_sigalrm}{13}{subsection.5.1.3}
\contentsline {chapter}{\numberline {6}Error Handling}{15}{chapter.6}
\contentsline {section}{\numberline {6.1}debug}{15}{section.6.1}
\contentsline {section}{\numberline {6.2}info and warn}{15}{section.6.2}
\contentsline {section}{\numberline {6.3}check and check\_mem}{15}{section.6.3}
\contentsline {chapter}{\numberline {7}Test cases}{17}{chapter.7}
\contentsline {section}{\numberline {7.1}Priority Tests}{17}{section.7.1}
\contentsline {section}{\numberline {7.2}Join Tests}{17}{section.7.2}
\contentsline {section}{\numberline {7.3}Mutex Tests}{17}{section.7.3}
\contentsline {chapter}{\numberline {8}Bugs}{18}{chapter.8}
\contentsline {section}{\numberline {8.1}Fixed bugs}{18}{section.8.1}
\contentsline {subsection}{\numberline {8.1.1}invalid free}{18}{subsection.8.1.1}
\contentsline {subsection}{\numberline {8.1.2}queue.h portability}{18}{subsection.8.1.2}
\contentsline {subsection}{\numberline {8.1.3}Join Failures}{18}{subsection.8.1.3}
\contentsline {chapter}{\numberline {9}Performance and Profiling}{19}{chapter.9}
\contentsline {section}{\numberline {9.1}Tools}{19}{section.9.1}
\contentsline {section}{\numberline {9.2}valgrind}{19}{section.9.2}
\contentsline {subsection}{\numberline {9.2.1}references}{19}{subsection.9.2.1}
