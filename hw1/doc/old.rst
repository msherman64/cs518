Old Notes
=========
Observations
------------

* Scheduling is done via a calander que
* function calls have to be wraped via our lib function. We should only
  ever jump between contexts that we have written. Clinet code from 
  the caller should be 1 stack in.





Old notes
---------

The goal of the project is to implement the linux pthread library in userland

we need to implement the following functions

State Diagram
-------------

.. graphviz::

    digraph foo{
        "main" -> "pthread_create";
        "pthread_create" -> "yeild";
        "yeild" -> "swap_context(&cur_context, &schedule)" [color="red"];
        "swap_context(&cur_context, &schedule)" -> "scheduler"[color="red"];
        "scheduler" -> "run_thread(1)" [color="red"];
        "run_thread(1)" -> "func1";
        "scheduler" -> "main_stored" [color="red"];
        "main_stored"  -> "main" [color="green"];
    }

First
-----


ready queue + running state
scheduler/sighandler
register timer

* pthread_create

    * make new stack

* pthread_yield

    * move from running to ready(fire sigalarm)

* pthread_destroy

    * move from runing to returned/destroyed
    * fire sigalarm / call scheduler

* pthread_join

    * put behind *thread waiting on
    * if null, schedule
    * hash table? look up waited on.

Second
------

* mutex_create
* mutex_lock
* mutex_unlock
* mutex_destroy

* timer_syscall

* timer_handler
	scheduler runs inside handler
	
scheduler
	running_state
		scheduled to run
	ready queue
		able to be scheduled, not blocked
	waiting queue
		blocked on io or mutex
	maintainence cycle
		manages priorities
		moves threads between queues
	on create
		thead added to ready queue
	on destroy
		thread destroyed only from running queue (swapped in to clean up)?
	on yeild
		move to ready
	on join
		waiting?

mutexes
	each mutex has a queue of threads waiting for lock
	lock
		added to fifo queue, only returns when popped?
	unlock
		removes from queue
third
-----
multi level ready queue
priority handling
deal with priority inversion

