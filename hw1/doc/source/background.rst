Background infromation on the tools we used
===========================================

ucontext
---------

This was out primary tool for moving between code blocks. There were several
facilities that we needed to employ to make a working system. 

ucontext_t struct
^^^^^^^^^^^^^^^^^

The ucontext_t struct is the datatype that is used to store all of the 
information related to a specific context. There is no proper definition for 
the ucontext_t struct but a loose representation looks like::

    struct ucontext_t
    {
        ucontext_t *uc_link;
        sigset_t uc_sigmask;
        stack_t uc_stack;
        mcontext_t uc_mcontext;
    };

The sigmask is used to identify what signals to ignore in this context and
the uc_link is the context to return to once this one returns. This value
cannot be set directly (i.e. you can just assign it to a context you want to
hop to). It has to be set by a call to getcontext and will be set to what
drop to what ever context spawned the current one. 

The uc_stack is a structure that represents a stack and has to be allocated
before the ucontext can be used. 

making a context
^^^^^^^^^^^^^^^^

A code snippet for making a context is as follows::

    //Create a context for the new thread
    ucontext_t *uc = ....
    getcontext(uc);
    void *stack=calloc(STACKSIZE,1);
    uc->uc_stack.ss_sp = stack;
    uc->uc_stack.ss_size = STACKSIZE;
    uc->uc_stack.ss_flags = 0;
    sigemptyset(&uc->uc_sigmask);
    makecontext(uc, (void (*)(void))run_thread, 1, function);


In this code we given a ucontext_t pointer and we call getcontext on it. This
prepares some of the members of the structure (e.g. uc_link) to be like the
current context. It's similar to a call to fork() for process, because at the
point of call to get context, the current context and the stored contexts
are essentially the same. Once ucontext_t is prepared with getcontext, 
we allocate it a new stack, and set reset the signal masks. Finally the 
call to makecontext(...) replaces the executable portion of the context with 
code from the function that is pointed to by the second arg. In this case
we pass the function that will be made into a context a single argument 
which happens to be another function pointer. 

ucontext references
^^^^^^^^^^^^^^^^^^^

* https://www.gnu.org/software/libc/manual/html_node/System-V-contexts.html
* http://www.unix.com/programming/90783-signal-handling-context-switches-2.html
* http://www.oracle.com/technetwork/articles/servers-storage-dev/signal-handlers-studio-2282526.html
* https://nitish712.blogspot.com/2012/10/thread-library-using-context-switching.html
* http://pubs.opengroup.org/onlinepubs/009695399/functions/makecontext.html
* https://nitish712.blogspot.com/2012/10/

Signal Handling and Timer
----------------

To implement the scheduler quanta, we use the setitimer function. This specifies a timer class to use, as well as a struct of timer values.
Instead of an auto-recurring timer, we set it on each scheduler run, to ensure that critical functions are not interrupted.

Initialy, we used the SIGALRM timer, but changed to SIGVTALRM. While SIGALRM measures global cpu time, SIGVTALRM measures time spent in userspace in our
process. To ensure fairness to scheduled threads, we do not want to penalize them for time spent with the process swapped out.

The signal handler captures SIGVTALRM, and sets the SA_RESTART flag. This ensures that any system calls interrupted by the handler are resumed once it has finished. Calls to sigprocmask are used to enable and disable the receipt of alarms, to protect critical sections.

signal handler references
^^^^^^^^^^^^^^^^^^^^^^^^^
* https://www.gnu.org/software/libc/manual/html_node/Signal-Stack.html#Signal-Stack
* https://gist.github.com/aspyct/3462238
* https://www.gnu.org/software/libc/manual/html_node/Setting-an-Alarm.html
* https://thisissecurity.net/2015/01/03/playing-with-signals-an-overview-on-sigreturn-oriented-programming/

sys/Queue.h
-----------

We use the Queue.h library for making TAILQs. After a little setup we are 
able to traverse the TAILQs with simple for loops.

Types
^^^^^

We need to modify our storage type to have a TAILQ_ENTRY member inside the 
struct. This is another struct that stores forward and reverse pointers. We 
also need to declare a type for the head of the list. Note that in the type
def of the list entry we use _name as the struct name so that the 
TAILQ_MACRO has a struct to refer to. When we declare an entry of this list 
we will use the proper name at the end of the typedef. ::

    typedef struct _my_pthread_t
    {
        ...
        TAILQ_ENTRY(_my_pthread_t) tailq_struct; //Next and Prev Pointers for ready or waiting queue
    } my_pthread_t; //Type of the elements in the TAILQ

    typedef TAILQ_HEAD(tailhead, _my_pthread_t) head_t; //Type of the TAILQ head

Insertion
^^^^^^^^^

To insert into a tail queue we use the following macro::

    TAILQ_INSERT_TAIL(&wait_head, cur_thread, tailq_struct);

The args are :
 * wait_head - Pointer to the head of the list I want to insert into
 * cur_thread - Pointer to the entry I want to insert
 * tailq_struct - name of the struct inside my list entry that stores the 
    linked list elements

This action will insert at the tail of the queue. There is also a
TAILQ_INSERT_HEAD.

Removal
^^^^^^^

Remove can be called on any entry in the list via::

    TAILQ_REMOVE(&wait_head, np, tailq_struct);

If we want to "pop" off the head we can use ::

    TAILQ_REMOVE(&wait_head, wait_head.tqh_first, tailq_struct);

Which will remove the first entry.

OS References
-------------

* http://www.dirac.org/linux/gdb/02a-Memory_Layout_And_The_Stack.php
* http://duartes.org/gustavo/blog/post/journey-to-the-stack/
* https://computing.llnl.gov/tutorials/pthreads/
* https://stackoverflow.com/questions/9759880/automatically-executed-functions-when-loading-shared-libraries

Other References
----------------
* https://stackoverflow.com/questions/3930363/implement-time-delay-in-c
