Homework 1
==========

Authors:

 * **Michael Sherman**
 * **Shridatt Sugrim**

Built on:
**adapter.cs.rutgers.edu**

Introduction
------------

The goal of this exercise was to implement user threads using ucontext. We 
also had to implement mutual exculsions (mutexes). The api we had to conform
was::


    /* exposed API */
    void my_pthread_yield();
    void my_pthread_exit(void *value_ptr);
    int my_pthread_join(my_pthread_t thread, void **value_ptr);
    int my_pthread_create( my_pthread_t *pt,
                           my_pthread_attr_t *attr,
                           void *(*function)(void*),
                           void * arg);

    /* mutex API */
    //attributes are ignored
    int my_pthread_mutex_init(my_pthread_mutex_t *mutex,
                              const pthread_mutexattr_t *mutexattr);
    int my_pthread_mutex_lock(my_pthread_mutex_t *mutex);
    int my_pthread_mutex_unlock(my_pthread_mutex_t *mutex);
    //mutex should be unlocked before destroy
    int my_pthread_mutex_destroy(my_pthread_mutex_t *mutex);

We also had to define the types *my_pthread_mutex_t* and *my_pthread_t*.
