Bugs
=====

Fixed bugs
----------

invalid free
^^^^^^^^^^^^
This is the valgrind command we used to find the memory leaks, and segfault locations. 
GDB was not useful as it could not access memory location in question, and did not provide a backtrace.
    ::
    
    valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes -v ./test_join
We calloc a STACKSIZE region in pthread_create, and assign its pointer to ucontext->uc_stack.ss_sp.
Calloc is used to ensure the region is initialized.
When a thread exits, this must be freed, to prevent leaks. We were freeing it at the end of my_pthread_exit.
After exiting, yield is called, and we access cur_thread's context.

To fix it, the check for finished and subsequent free was moved into the scheduler, after we have used cur_thread's 
context for the last time.

queue.h portability
^^^^^^^^^^^^^^^^^^^
different versions of queue.h between ilab and 16.04 Ubuntu VM
The ilab version of TAILQ in queue.h is singly linked, equivalent to 16.04's STAILQ

In ubuntu 16.04, TAILQ is a doubly linked list, equivalent to CIRCLE_QUEUE in the ilab version.

We did not encounter a bug, as we only attempted forward traversal.

Join Failures
^^^^^^^^^^^^^

There still seems to be some case where calling join deschedules the main
thread. It seems to happen if we have a lot of short lives threads calling
join very aggressively. This is definitely some kind of timing issue because
putting a long sleep in main seems to make the problem go away.

The isse was that we were not properly testing for the case where a thread
tries to join it self. We had not covered the case where np == cur_thread.
Fixing this prevent a thread fromy trying to join it self and resolved the
segfault.
