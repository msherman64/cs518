The Calendar Queue
==================

We use a BSD Style calendar queue because it has a very simple implementation
and is tolerant of burst of short threads. We have a coarse grained 
scheduling strategy that guaruntees that aged threads will not sit at 
the bottom level of a queue forever.

List of queues
--------------

The calendar queue structure is a circular list of queues. We maintain 
current queue index variable that identifies the queue we are currently 
pulling from. Each time a new thread is to be scheduled, it is taken from 
the  head of this queue (and popped off). No insertions are made into the 
currently active queue (so that it must eventually empty). When a thread 
needs to be rescheduled it can be added to any of the non-active lists. The 
active index only increments in one direction (increasing numeric order wrap 
around). Thus higher priority process are put in the next immediately queue (
eg. (current index + 1) % MAXINDEX). The lowest priority process added to the 
farthest queue from the current one (e.g. (current index + MAXINDEX -1) % 
MAXINDEX). 

.. image:: ./images/CQ.png

A key observation is that the numeric value of the priority member of
my_pthread_t is interpreted as as how far away from the active queue the will
this thread be reinserted into the calendar queue.  This the reverse of what 
you might expect. The higher the value, the lower the priority. 


Prioirty and ticks
------------------

First note that we used a fixed size quanta for all queues. The use of a 
calender queue allows us to have a very simple ageing scheme, it penalizes 
excessive run time but prevents starvation. The scheme is as follows

 * All new threads start at the highest priority level (1).
 * Each time a thread is scheduled at a given priority  level, a counter is
   incremented.
 * When the counter crosses some predefined threshold, the thread prioirty is
   lowered (the number is bumped up by 1), and the counter resets.
 * This count and lower procedure will continue until the thread reaches the
   lowest priority (MAXINDEX - 1), and which point the threads priority will
   be reset to the highest (1).
 * This cycle continues until the thread completes.

One  advantage of this strategy is that each thread will run eventually because
it has to be put some somewhere in the calendar queue and because the active 
queue can't be inserted into it will get some run time.

In a regular multi-level queue there are long run queues and short run queues
and scheduling between them usually has some duty cycle where the long slow
process eventually get a large quanta slot so they can finish. This is done
using variable quanta. Our scheme approaches this because when the priority 
number wraps around the thread will get run often (because it getting 
inserted to the calendar closer to now). It may even be the case that the 
long running thread can get several quanta back to back (thus approximating
a larger quanta albeit with a context switch in between). 

While the context switch in the middle is a draw back, the scheduler has
a very simple job to update the priority (just a few simple numeric 
operations). Thus running the scheduler should be very lightweight and the 
interruptions in the slots should be very short.


soft premption
--------------

One draw back to this strategy is that there is not hard premption. Forcibly
putting a thread on the current active queue breaks the fundmental assumption.

One could implement a system that allows this but it would have to also check
to make sure a thread does "dominate" by consistently keeping the active queue
full. A simple fix to this might be to count the number of times that the 
current queue was popped and if it's greater than some meaningful threshold, 
simply forcibly increment the active index. 

Tuneing parameters
------------------

The two major tuning parameters in our strategy are the number of queues in
the calendar (MAXINDEX) and the number of insertions at the current
priority that the current thread is allowed before it's priority is decreased.
(TICKS).

We currently run with MAXINDEX = 10, and TICKS = 5 and this seems to get good
fairness for several large numbers of threads. One could build an adaptive 
system where these parameters are raised and lowered in response to decreasing
fairness amongst threads. 
