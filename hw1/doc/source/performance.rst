Performance and Profiling
=========================

Tools
-----

The time command was used to get the runtime of each test case

The perf command was used to profile time spent in each function for each test case

* Running perf on testPriority showed 67% of the time spent in waitFor, and time checking. This is expected, as it is busy waiting by checking the time. 
* Running perf on testMutex showed a large amount of time in sigprocmask, as overhead. The cost of setting the signal mask in each lock and unlock was
   too high, so we removed those calls. The results are below, showing that the user function f1 is now getting roughly a 5x speedup.


.. csv-table:: test_mutex performance
   :header: "Function Call", "With block_sigalrm()", "Without block_sigalrm()"

   "f1()", "0.75%", "4.84%"
   "my_pthread_mutex_lock", "4.05%", "27.59%" 
   "my_pthread_mutex_unlock", "3.78%", "23.43%" 
   "__sigprocmask", "9.79%", "0.01%" 
   "scheduler()", "0.00%", "not captured by profiler" 



valgrind
--------

To track memory usage, we use the tool valgrind. To let it handle our stack changes, the header 
valgrind.h is included.
We add two calls to::

    ret = VALGRIND_STACK_REGISTER(stack, stack + STACK_SIZE);

These calls tell valgrind the extent of each thread's stack. One is triggered during setup, 
and one on thread creation. We are then able to verify that our test cases are not loosing memory, withe command ::

    valgrind ./testname

references
^^^^^^^^^^
* http://valgrind.10908.n7.nabble.com/Programs-using-makecontext-swapcontext-td34348.html
* https://github.com/lu-zero/valgrind/blob/master/memcheck/tests/linux/stack_changes.c
* http://valgrind.org/downloads/repository.html

