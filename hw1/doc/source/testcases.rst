Test cases
==========

Priority Tests
--------------

To test the prioritization we have a testPriority.c test case. In it we
have 2 functions, a short running function (counts to 5 with a second 
sleep in between each count), and a long running function (counts to 
25). We first spawn 20 short running threads and then 10 long running 
threads. We then wait for a few seconds and spawn another 20 short 
thread. We repeat the second phase spawning 3 times.

In our tests the long running threads were able to complete before the 
later phases of short running threads. Thus having an influx of short running
jobs did not completely starve our long running threads. If starvation was
occurring the long running threads would finish last. 

Join Tests
----------

The file test_join tests thread creation and join.

* It creates numthreads threads
* the first thread waits for 5, then exits.
* each subsequent thread joins on the one prior
* The IDs are passed in via function arg, and the thread creates a my_pthread_t with that id to join on.
* main waits on the last thread

Expected output: The first thread's messages, followed by join returning in order of increasing id.

Mutex Tests
-----------

The file test_mutex tests mutex creation, lock, unlock, and destroy.

* It creates a single mutex, that is then passed via arg to all threads.
* It creates 8192 threads, that each loop num_loops times
* Each loop, they each lock the mutex, increment a counter, and then unlock
* Main joins on all threads, to wait for completion.
* The expected value of the counter, num_loops * num_threads, is compared to the final value
  and printed.

This test has succeeded with num_loops = 10000, and num_threads = 8192.
The lock variable allows turning the use of mutexes on and off, to verify that concurrency
issues are present without the mediation of the mutex.
