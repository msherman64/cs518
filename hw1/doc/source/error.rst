Error Handling
===============

Following the example of Zed Shaw (Learn C the hard way), we used some 
variadic macros to make convenient debug-wrappers.

debug
-----

This is used for debug messages. It gives you the file and line number where 
the message was emitted from in the message text. If the NDEBUG flag is not set 
during compilation these messages will not print::

    #ifndef NDEBUG
    #define debug(M, ...)
    #else
    #define debug(M, ...) printf("DEBUG %s:%d: " M "\n",\
            __FILE__, __LINE__, ##__VA_ARGS__)
    #endif

info and warn
-------------

Same as debug bug these always print.::

    #define warn(M, ...) printf("WARN %s:%d: " M "\n",\
            __FILE__, __LINE__, ##__VA_ARGS__)

    #define info(M, ...) printf("INFO %s:%d: " M "\n",\
            __FILE__, __LINE__, ##__VA_ARGS__)

check and check_mem
-------------------

check and check_mem, check if a condition is true (similar to an assert). If
the condition is false, the output the error message (second argument), and 
then jump to the error context. We use the con facility to make sure the 
error context is setup at the start of the program (before any thing else
is run).::

    //A is what we want to assert is true.
    #define check(A, M, ...) if(!(A)) {\
        debug(M, ##__VA_ARGS__); setcontext(&error_context); }

    //If pointer is null, then !pointer is True
    #define check_mem(A) check((A), "Pointer was null")

This is the setup for making sure error_context is defined::

    static void con() __attribute__((constructor)); 
    void con(void);
    void error(void);


    void con(void)
    {
        debug("con: started function");
        getcontext(&error_context);
        error_context.uc_stack.ss_sp = malloc(STACKSIZE);
        debug("con: malloced error stack %p", error_context.uc_stack.ss_sp);
        error_context.uc_stack.ss_size = STACKSIZE;
        error_context.uc_stack.ss_flags = 0;
        sigemptyset(&error_context.uc_sigmask);
        makecontext(&error_context, (void (*)(void))error, 0);
        debug("con: ended function");
    }

    void error(void)
    {
        debug("error: started function");
        exit(1);
        debug("error: ended function");
    }

