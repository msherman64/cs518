FLow of execution of a thread
==================================

Interaction between run and create 
-----------------------------------

my_pthread_create
^^^^^^^^^^^^^^^^^

my_pthread_create is where a context is prepped to be run. It is not 
actually run there, however. Instead it is added to the list of things that 
will be scheduled via a call to ready_insert().

One important point of this create is that it the bottom of the stack that 
gets created is the run_thread function. The uc_link value that is set at the 
call to getcontext() becomes stale after the this function exits because main 
has moved forward so what that uc_link was pointing to is no longer valid. If 
we let this context run to completion the entire program will exit because 
of the stale uc_link.

Thus run_thread function never completes, it yields right before it would
return to that stale context. 

.. _runthreaddiscuss:

run_thread
^^^^^^^^^^

We wrap the function we're supposed to be calling inside our own function
When the function we're calling is done, control is returned to us
and we can preform cleanup as needed.  We set the finished flag to 1 so 
that it will not be scheduled again.

It is expected that cur_thread is pointing to correct my_pthread_t so that
the setting of flags is done to the correct threading object. This is
enforced by design because this function won't be scheduled if 
cur_thread is not point to it's correspond my_pthread_t.

handle_sigalrm
^^^^^^^^^^^^^^

This is really the most innovative part of this setup. The scheduler
context that is created here is ephemeral. It's really just a place holder
to go to the intermediate stage where we run the scheduler. It needs to be 
recreated every time because we need to jump to the top of the scheduler 
function. The scheduler never reaches the end of the scheduler context because
of the setcontext call at the bottom of the scheduler. 

The major innovation is the last call to swapcontext and how we capture 
the state of the main thread. All other threads have their contexts made
via Make context, however for the main thread this is not the case. After a
call to make context, execution falls back to main. At some later point
the alarm that was set when the first call to my_pthread_create fires,
then this signal handler fires.

When swap context is called, the current context is stored in cur_thread
and we pass to the scheduler which will put the context just stored in
cur_thread into the back of the queue. What is key to note is that this
contexts next step is to return to main and continue execution in the main
thread. Since the next set of threads to run are not main, mains state is
frozen.

Each time another thread is run, this same procedure will store the current
state of the running thread, and pass control to the scheduler (which will
in turn put it at the back of the queue). 

Eventually we'll return to main, continue to run in the main thread until
the alarm fires again. Then the whole process repeats.
