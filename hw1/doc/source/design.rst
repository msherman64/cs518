Design
======

Our implementation uses a calendar queue which is an array of TAILQs. The 
TAILQs can have entities inserted into the back of the queue, and also have
entites poped off the front of the queue. The types and functions that are 
used are described below.

scheduler
---------

The scheduler function is used to make the scheduler context. This context is 
entered immediately after a sigalrm is fired. While in the scheduler, the 
ticks and priority are updated. Ticks is a counter that keeps track of how 
many times the current thread  was scheduled at the current priority level.

After the priority level is updated, the thread pointer is reinserted into the
ready queue at the priority just calculated, unless it is finished. If the
thread has joined another thread (indicated by a non-null waiting_for 
pointer) the thread is put on the waiting queue instead. Finally the next 
thread in the current state of the ready queue is swap in.

get_next
--------

Get next returns a pointer to the next argument in the current queue from the 
array of queues that make up the calendar queue. If the current queue is 
empty, get next will hop to the next non-empty queue, and begin emptying it. 

If get next loops around more than once, we have entered an error condition.

ready_insert
------------

Ready insert takes two args:

 * Pointer to a my_pthread_t to be inserted into the ready queue
 * priority to insert at.

The priority is a number between 1 and PRIORITYLEVELS - 1. The higher this
number is the further into the future this thread will be scheduled. Thus
an insertion priority of 1 is the "highest priority" and an insertion 
priority of PRIORITYLEVELS - 1 is the lowest priority. 

my_pthread_t
------------

This type is exposed to the user. We use it as the central data structure
for keeping track of the state of a thread, and for storing the hints
used to schedule (or deschedule) a thread.::

    typedef struct _my_pthread_t
    {
        int thread_id;                     //Numeric thread identifer (just counts up)
        int priority;                      //level at whcich we are reinserted into ready_queue
        int ticks;                         //counter for number of ticks at current priority
        int finished;                      //Tells us when to no longer schedule this thread
        struct _my_pthread_t *joined_by;   //who is joining this thread
        struct _my_pthread_t *waiting_for; //thread this is wating for
        ucontext_t context;                //Uses for calls to get/set context

        //Part of the callers memory that we will store the retrun value in
        void *func_arg;
        void *retval;

        //Next and Prev Pointers for ready or waiting queue
        TAILQ_ENTRY(_my_pthread_t) tailq_struct;
    } my_pthread_t;                        //Type of the elements in the TAILQ


my_pthread_create
-----------------

Arguments:

 * my_pthread_t \*pt - pointer to my_pthread_t. Used for "rendezvousing" with
    client code internally tracking what thread contexts we are keeping track of.
 * my_pthread_attr_t \*attr - Ignored
 * void \*(\*function)(void\*) - pointer to function. Function takes an void \*
     and returns a void \*. This is the "body" of the thread to be created
 * void \*arg - pointer to function arg, will be passed to function when
    called. It is up to the client code to know what the type is and how to
    interpret it.

Create is actually the work horse of the library. It sets up thread and 
stores contexts for future scheduling. Create adds  every new thread created 
to the global thread list which is later used to find the thread pointer
to join. If setup was never called the first call to create will call setup. 

setup
-----

Creates the context for the main thread, and initializes queue heads.

run_thread
----------

The run_thread function is a thin wrapper around the function we're 
supposed to run in the thread.  See :ref:`runthreaddiscuss`

Once a function completes we call exit with the return value of the function
as the argument. 


my_pthread_exit
---------------

The exit function can be entered in two ways, either an explicit call via
a running thread, or reaching the bottom of the run_thread functions
execution. When inside the exit function, we set the "finished" flags which
will be used to prevent future scheduling of this thread. 

If this thread was joined by some other thread, we pull the joining thread
out of the wait queue and add it to the ready queue with the highest priority
so that it can resume. We also store the value_ptr passed into the function
in the my_pthread_t. It may be NULL.

my_pthread_yield
----------------

Yield is the simplest function in the batch. It simply raises sigalrm.

my_pthread_join
---------------

Join is one of the more complex functions in the library. It takes a 
my_pthread_t type as the arugment. It then uses the thread_id member to 
index into our global list of threads, and get a pointer to the existing 
thread object (which has been populated with proper ucontext pointers, etc..).

Once we have a consistent pointer, we test collection of error cases , 
including deadlock and joining threads that are done. Assuing no errors 
have occured, we set the waiting_for member of the current my_pthread_t to 
point to the thread we want to wait forr. We then use the retrievied pointer 
to set the joined_by member of the thread we will be waiting on.

Finally we call yeild to relinquish control. At this point the scheduler will
note that the wiating_for member of the current thread is not NULL and 
put the current thread on the wait queue.

mutex_functions
---------------

The goal of the mutex functions is to enable functions to order access to a 
protected region, and to prevent collisions.  To prevent collisions, they 
provide hints to the scheduler via the ready and wait queues.  To order 
actions, each mutex maintains a queue of threads attempting to lock it.

We implement this with four mutex functions:

* my_pthread_mutex_init
* my_pthread_mutex_lock
* my_pthread_mutex_unlock
* my_pthread_mutex_destroy

As well as 3 typedefs to maintain the lists, however only one, the
my_pthread_mutex_t is exposed to the user:

* my_pthread_mutex_t
* mutex_list_t
* mutex_list_head

init
^^^^
The mutex init function takes a pointer to a mutex type. It then initializes 
the mutex's list of mutex_list objects, so that a head object exists. This 
head object is placed on the heap, in case the creating thread returns.

lock
^^^^
The mutex lock function takes a pointer to a mutex type. It needs to add the 
current thread to the end of the mutex's queue. If it is the only thread 
locking this mutex, it will be at the front queue, and this function 
exits immediately, with the thread having the lock. This allows 
the thread to immediately return to the ready queue upon scheduling.

If another thread has the lock, the list insertion will not be at the front of
the queue. The waiting_for value of the current thread is set to the 
thread at the head of the queue. This signals the scheduler to place it on 
the wait queue.  It will not be scheduled again until all threads ahead of it 
in the mutex queue have called unlock.

In both cases, the function exits quickly, by returning if it has lock, and 
by yielding if not.

unlock
^^^^^^
The mutex unlock function takes a pointer to a mutex type. It checks to see 
if it is currently at the head of the mutex's queue. If it is not, it returns 
-1, to prevent unlocking erroneously.

If it is at the head of the mutex's queue, then it currently has the lock. 
This function then pops the thread off of the queue, to unlock it. The list 
entry is freed, and the next thread on the mutex queue is moved from waiting 
to ready.

destroy
^^^^^^^
This function cleans up the mutex object. If the list is non-empty, it 
returns an error condition, as you cannot destroy a locked mutex.
If the list is empty, it frees the list_head object.

start_timer
-----------

We fire a fixed timer on each scheduler run, instead of an interval timer and
signal mask. This is to ensure that each thread gets a full time slice, 
independent of scheduler runtime. In addition, the virtual timer is used, so as
not to lose thread time while the process is swapped out.This also ensures 
that a timer will not fire while we are in the scheduler.

waitFor
-------

An implementation of a sleep function that sleeps by spinning a while loop 
until a real time clock second has passed. We need an alternative method 
of introducing delays that does not depend on sigalrm.
