#include <ucontext.h>
#include <sys/types.h>
#include <sys/time.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <poll.h>

#include "my_pthread.h"

void
thread2(void)
{
   printf("in thread2\n");
}

int
main()
{

    fprintf(stderr,"Process Id: %d\n", (int)getpid());

    /* make all our contexts */
    mkcontext(thread2);


    /* initialize the signal handlers */
    setup_signals();

    raise(SIGALRM);

    return 0; /* make gcc happy */
}
