#include <ucontext.h>
#include <sys/types.h>
#include <sys/time.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <poll.h>

#include "my_pthread.h"

ucontext_t contexts[NUMCONTEXTS];   /* store our context info */
int used_contexts = 0;

void
timer_interrupt(int j, siginfo_t *si, void *old_context)
{
    setcontext(&contexts[0]);
}

/* Set up SIGALRM signal handler */
void
setup_signals(void)
{
    struct sigaction act;

    act.sa_sigaction = timer_interrupt;
    sigemptyset(&act.sa_mask);
    act.sa_flags = SA_RESTART | SA_SIGINFO;

    if(sigaction(SIGALRM, &act, NULL) != 0) {
        perror("Signal handler");
    }
}

int
mkcontext ( void (*func)(void) )
{
    void * stack;

    ucontext_t *uc = &contexts[used_contexts];

    getcontext(uc);

    stack = malloc(STACKSIZE);
    if (stack == NULL) {
        perror("malloc");
        exit(1);
    }

    /* we need to initialize the ucontext structure, give it a stack,
        flags, and a sigmask */
    uc->uc_stack.ss_sp = stack;
    uc->uc_stack.ss_size = STACKSIZE;
    uc->uc_stack.ss_flags = 0;
    sigemptyset(&uc->uc_sigmask);

    /* setup the function we're going to, and n-1 arguments. */
    makecontext(uc, func, 0);

    printf("context is %x\n",(unsigned int)uc);

    used_contexts++;
    return 0;

}
