#ifndef __my_pthread_h__
#define __my_pthread_h__


#define NUMCONTEXTS 10              /* how many contexts to make */
#define STACKSIZE 4096              /* stack size */


void
timer_interrupt(int j, siginfo_t *si, void *old_context);

void
setup_signals(void);

int
mkcontext ( void (*func)(void) );

#endif
